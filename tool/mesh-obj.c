#include "file/mesh.h"

#include <stdio.h>
#include <stdlib.h>

static void dump_verts(FILE *f, uint32_t n, struct file_mesh_vert *v)
{
	for (uint32_t i = 0; i < n; i++) {
		fprintf(f, "# vertex %u\n", (unsigned) i);
		fprintf(f, "v %f %f %f\n", v[i].pos[0], v[i].pos[1], v[i].pos[2]);
		fprintf(f, "vn %f %f %f\n", v[i].normal[0], v[i].normal[1], v[i].normal[2]);
		fprintf(f, "vt %f %f\n\n", v[i].uv[0], v[i].uv[1]);
	}
}

static void dump_tris16(FILE *f, uint32_t n, uint16_t *v)
{
	for (uint32_t i = 0; i < n/3; i++) {
		fprintf(f, "f %u/%u/%u %u/%u/%u %u/%u/%u\n",
				(unsigned) v[3*i] + 1, (unsigned) v[3*i] + 1, (unsigned) v[3*i] + 1,
				(unsigned) v[3*i + 1] + 1, (unsigned) v[3*i + 1] + 1, (unsigned) v[3*i + 1] + 1,
				(unsigned) v[3*i + 2] + 1, (unsigned) v[3*i + 2] + 1, (unsigned) v[3*i + 2] + 1);
	}
}

static void dump_tris32(FILE *f, uint32_t n, uint32_t *v)
{
	for (uint32_t i = 0; i < n/3; i++) {
		fprintf(f, "f %u/%u/%u %u/%u/%u %u/%u/%u\n",
				(unsigned) v[3*i] + 1, (unsigned) v[3*i] + 1, (unsigned) v[3*i] + 1,
				(unsigned) v[3*i + 1] + 1, (unsigned) v[3*i + 1] + 1, (unsigned) v[3*i + 1] + 1,
				(unsigned) v[3*i + 2] + 1, (unsigned) v[3*i + 2] + 1, (unsigned) v[3*i + 2] + 1);
	}
}

int main(int argc, char **argv)
{
	FILE *in = argc <= 1 ? stdin : fopen(argv[1], "rb");
	FILE *out = argc <= 2 ? stdout : fopen(argv[2], "w");
	struct file_mesh_header hdr;
	fread(&hdr, sizeof(struct file_mesh_header), 1, in);
	if (hdr.vert_count <= UINT16_MAX) {
		uint16_t *v = malloc(sizeof(uint16_t) * hdr.vi_count);
		fread(v, sizeof(uint16_t), hdr.vi_count, in);
		dump_tris16(out, hdr.vi_count, v);
		free(v);
	} else {
		uint32_t *v = malloc(sizeof(uint32_t) * hdr.vi_count);
		fread(v, sizeof(uint32_t), hdr.vi_count, in);
		dump_tris32(out, hdr.vi_count, v);
		free(v);
	}
	struct file_mesh_vert *verts = malloc(sizeof(struct file_mesh_vert) * hdr.vert_count);
	fread(verts, sizeof(struct file_mesh_vert), hdr.vert_count, in);
	fclose(in);
	dump_verts(out, hdr.vert_count, verts);
	fclose(out);
	return 0;
}
