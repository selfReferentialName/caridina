.MAIN: tool/respack

tool/respack: tool/respack.c
	$(CC) $(CFLAGS) -o tool/respack tool/respack.c

tool/mesh-obj: tool/mesh-obj.c
	$(CC) $(CFLAGS) -o tool/mesh-obj tool/mesh-obj.c

tool/meshify: tool/meshify.cpp
	$(CXX) $(CFLAGS) -o tool/meshify tool/meshify.cpp $(LFLAGS) -lassimp

.include "tool/test/rules.mk"
