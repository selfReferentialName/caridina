= Caridina Engine

Caridina is a little toy game engine written in C11.

== Description

Caridina is a small C11 game engine based on the Flecs entity component system
which renders using Vulkan.

.Current Features
 * Nothing
 * There isn't actually any proper code yet

.Planned Features
 * Foreward+ rendering
 * Global illumination
 * 3d physics
 * Lisp scripting language
 ** Compiled as needed on the target machine
 * Postprocessing effects
 ** Screen Space Ambient Occlusion
 ** Screen Space Reflection
 ** Bloom and auto-exposure
 ** Depth of field

.Features that will not be implemented, at least for a while
 * Rendering to a texture rather than the window

== Installation

First, install the dependencies

 * bmake (on BSD, this is the already included make)
 * glfw3
 * flecs
 * cglm
 * TODO: add links (esp to flecs and cglm)

Then, installation is as simple as running

----
bmake config # optional, interactively set configuration options
bmake -j<your number of cores>
sudo bmake install
----

If you want to build the documentation and install the manpages,
you also need

 * doxygen with latex support
 * graphviz

== Usage

Here's a basic example which just draws a basic triangle.

----
#include <caridina/caridina.h>

vec3 pos[] = {
	{-1, -1, 0},
	{1, -1, 0},
	{0, 1, 0}
};

vec3 normal[] = {
	{0, 0, -1},
	{0, 0, -1},
	{0, 0, -1}
};

vec3 col[] = {
	{1, 0, 0},
	{0, 1, 0},
	{0, 0, 1}
};

crd_vert_index_t idx[] = {
	0, 1, 2
};

int main(int argc, char **argv)
{
	crd_init(argc, argv);
	crd_mesh_t mesh = crd_mesh_static_vertcol(3, idx, pos, normal, col);
	crd_entity_t tri = crd_new_entity();
	crd_add_pos(tri, (vec3) {0, 0, 1});
	crd_add_mesh(tri, mesh);
	crd_entity_t cam = crd_new_entity();
	crd_add_pos(cam, (vec3) {0, 0, 0});
	crd_add_xform(cam, (mat3) {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
	crd_add_frustum(cam, 0.5, 1.5, GLM_PI_4 * 0.9, -1);
	crd_set_camera(cam);
	return crd_game_loop();
}
----

More examples can be found under the examples directory.

== Tools

 * `respack` -- Create resource packs.

== Support

None, sorry.

== Contributing

Need more code to write.

== Authors and acknowledgment

Currently just me.

== License

TODO: pick one. Probably MIT.

== Roadmap

 . Implement everything to render a static cube.
 . Foreward+ lighting.
 . Screen space reflection (will affect renderpass and shader stuff, hence why it's so early).
 . Load static obj files.
 . Load static GLTF files.
 . Bony animations and animated GLTF files.
 . Basic physics.
 . Lisp scripting language.
