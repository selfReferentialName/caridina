#!/bin/sh

set -e

tool/respack tool/test/respack/foo.respack tool/test/respack/*.res tool/test/respack/*.png
diff tool/test/respack/foo.respack tool/test/respack/foo.expect
