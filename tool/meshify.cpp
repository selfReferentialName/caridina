#include "file/mesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <vector>
#include <unordered_map>

#include <cstdio>

struct loaded_mesh {
	std::vector<file_mesh_vert> verts;
	std::vector<uint32_t> idxs;
};

static void load_mesh(loaded_mesh *r, mat4 xform, const aiMesh *mesh, const aiScene *scene)
{
	uint32_t mesh_start = r->verts.size();
	printf("Loading mesh %s with %u verts and %u tris\n", mesh->mName.data, mesh->mNumVertices, mesh->mNumFaces);
	for (unsigned i = 0; i < mesh->mNumVertices; i++) {
		file_mesh_vert vert;
		vec3 raw_pos = {mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z};
		glm_mat4_mulv3(xform, raw_pos, 1, vert.pos);
//		vert.pos[1] *= -1;
		// TODO: correct skew from scaling
		vec3 raw_nml = {mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z};
		glm_mat4_mulv3(xform, raw_nml, 0, vert.normal);
		if (mesh->mTextureCoords[0]) {
			vert.uv[0] = mesh->mTextureCoords[0][i].x;
			vert.uv[1] = mesh->mTextureCoords[0][i].y;
		} else {
			glm_vec2_zero(vert.uv);
		}
		r->verts.push_back(vert);
	}
	for (unsigned i = 0; i < mesh->mNumFaces; i++) {
		aiFace *face = &mesh->mFaces[i];
		for (unsigned j = 0; j < face->mNumIndices; j++) {
			r->idxs.push_back(face->mIndices[j] + mesh_start);
		}
	}
}

static void load_node(loaded_mesh *r, aiNode *node, const aiScene *scene)
{
	printf("Loading node %s\n", node->mName.data);
	for (unsigned i = 0; i < node->mNumMeshes; i++) {
		mat4 xform;
		memcpy(xform[0], node->mTransformation[0], sizeof(vec4));
		memcpy(xform[1], node->mTransformation[1], sizeof(vec4));
		memcpy(xform[2], node->mTransformation[2], sizeof(vec4));
		memcpy(xform[3], node->mTransformation[3], sizeof(vec4));
		glm_mat4_transpose(xform);
		load_mesh(r, xform, scene->mMeshes[node->mMeshes[i]], scene);
	}
	for (unsigned i = 0; i < node->mNumChildren; i++) {
		load_node(r, node->mChildren[i], scene);
	}
}

static struct loaded_mesh load_mesh(const aiScene *scene)
{
	struct loaded_mesh r = {};
	load_node(&r, scene->mRootNode, scene);
	return r;
}

static void write_mesh(FILE *f, struct loaded_mesh *mesh)
{
	file_mesh_header hdr = {};
	hdr.magic = 0x6873656D;
	hdr.topology = FILE_MESH_LIST;
	hdr.vi_count = mesh->idxs.size();
	hdr.vert_count = mesh->verts.size();
	hdr.checksum = hdr.magic + hdr.topology + hdr.vi_count + hdr.vert_count;
	fwrite(&hdr, sizeof(file_mesh_header), 1, f);
	if (hdr.vi_count <= UINT16_MAX) {
		for (uint32_t vi32 : mesh->idxs) {
			uint16_t vi16 = vi32;
			fwrite(&vi16, sizeof(uint16_t), 1, f);
		}
	} else {
		fwrite(mesh->idxs.data(), sizeof(uint32_t), mesh->idxs.size(), f);
	}
	fwrite(mesh->verts.data(), sizeof(file_mesh_vert), mesh->verts.size(), f);
}

int main(int argc, char **argv)
{
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(argv[2], aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_OptimizeMeshes);
	struct loaded_mesh mesh = load_mesh(scene);
	FILE *f = fopen(argv[1], "wb");
	write_mesh(f, &mesh);
	fclose(f);
	return 0;
}
