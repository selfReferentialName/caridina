#undef NDEBUG

#include "server/resource/file.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#define MIN(x, y) ((x < y) ? (x) : (y))

void perrno(void)
{
	perror("");
}

void xwrite(int fd, void *data, size_t size)
{
	size_t actual;
	if ((actual = write(fd, data, size)) != size) {
		printf("Only wrote %d bytes.\n", (int) actual);
		perror("Didn't write correctly.");
		abort();
	}
}

uint32_t get_file_size(char *file)
{
	struct stat stats;
	if (stat(file, &stats) != 0) {
		printf("Tried to get size of %s.\n", file);
		perror("Couldn't get size of file");
		abort();
	}
	return stats.st_size;
}

int main(int argc, char **argv)
{
	atexit(perrno);

	assert(argc >= 2);
	// TODO: maybe support files not from arguments
	uint32_t file_count = argc - 2;

	int fd = creat(argv[1], 0644);
	assert(fd != -1);

	struct res_pack_header hdr = {
		.magic = {
			.s = "crd0"
		},
		.file_count = file_count,
		.chunk_count = file_count
	};
	hdr.checksum = hdr.magic.i + hdr.file_count + hdr.chunk_count;
	xwrite(fd, &hdr, sizeof(struct res_pack_header));

	for (uint32_t i = 0; i < file_count; i++) {
		struct res_pack_file_base base = {
			.size = get_file_size(argv[i + 2]),
			.chunk_count = 1
		};
		strcpy(base.name, argv[i + 2]);
		xwrite(fd, &base, sizeof(struct res_pack_file_base));
//		uint32_t id = 1; // this line of code is not commented because it may be useful, but because it needs to be shamed
		xwrite(fd, &i, sizeof(uint32_t));
	}

	for (uint32_t i = 0; i < file_count; i++) {
		uint32_t size = get_file_size(argv[i + 2]);
		xwrite(fd, &size, sizeof(uint32_t));
		int fdi = open(argv[i + 2], O_RDONLY);
		assert(fdi != -1);
		char *data = malloc(size);
		assert(read(fdi, data, size) == size);
		close(fdi);
		xwrite(fd, data, size);
		free(data);
	}

	close(fd);
	return 0;
}
