/** @file file/mesh.h
 * @brief Triangle mesh file format.
 *
 * Meshes are formatted like the following struct:
 *
 * ```
 * struct file_mesh {
 *     struct file_mesh_header hdr;
 *     union {
 *         uint16_t vi16[hdr.vi_count]; // if hdr.vert_count <= UINT16_MAX
 *         uint32_t vi32[hdr.vi_count]; // if hdr.vert_count > UINT16_MAX
 *     };
 *     struct file_mesh_vert vert[hdr.vert_count];
 * };
 * ```
 */
#ifndef FILE_MESH_H
#define FILE_MESH_H

#include <cglm/cglm.h>

/// Topology of the mesh.
enum file_mesh_topology {
	FILE_MESH_LIST ///< Vertex indices are in triplets representing triangles.
};

/// Flags to mark special things about the mesh.
enum file_mesh_flags {
	FILE_MESH_FLAT = 0x1, ///< TODO: Don't interpolate normals.
};

/// All supported flags ored together
#define FILE_MESH_ALL_FLAGS 0

/// The header of a mesh file.
struct file_mesh_header {
	uint32_t magic; ///< "mesh" == 0x6873656D
	uint32_t topology; ///< The topology (a value of `enum file_mesh_topology`)
	uint32_t vi_count; ///< Number of verts in the topology (i.e. vi16 or vi32)
	uint32_t vert_count; ///< Number of actual vertices (i.e. one more than maximum vertex index)
	uint32_t flags; ///< Flags, or of `enum file_mesh_flags`.
	uint32_t res0, res1; ///< Reserved padding; must be 0.
	uint32_t checksum; ///< `magic + topology + vi_count + vert_count + flags + res0 + res1`
};

/// A vertex with default flags
struct file_mesh_vert {
	vec3 pos; ///< Vertex position
	vec3 normal; ///< Normal vector as unit vector
	vec2 uv; ///< Texture coordinate in range [0,1]x[0,1]
};

#endif
