#include <caridina/caridina.h>

#include <GLFW/glfw3.h>
#include <cglm/cglm.h>

static crd_entity_t teapot;

static void pressed_up(void)
{
	vec3 pos;
	crd_get_pos(teapot, pos);
	pos[1] -= 0.2;
	crd_add_pos(teapot, pos);
}

static void pressed_down(void)
{
	vec3 pos;
	crd_get_pos(teapot, pos);
	pos[1] += 0.2;
	crd_add_pos(teapot, pos);
}

static void pressed_left(void)
{
	vec3 pos;
	crd_get_pos(teapot, pos);
	pos[0] -= 0.2;
	crd_add_pos(teapot, pos);
}

static void pressed_right(void)
{
	vec3 pos;
	crd_get_pos(teapot, pos);
	pos[0] += 0.2;
	crd_add_pos(teapot, pos);
}

static void pressed_in(void)
{
	vec3 pos;
	crd_get_pos(teapot, pos);
	pos[2] += 0.2;
	crd_add_pos(teapot, pos);
}

static void pressed_out(void)
{
	vec3 pos;
	crd_get_pos(teapot, pos);
	pos[2] -= 0.2;
	crd_add_pos(teapot, pos);
}

static void pressed_rol(void)
{
	mat3 lform;
	crd_get_xform(teapot, lform);
	mat4 xform;
	glm_mat4_identity(xform);
	glm_mat4_ins3(lform, xform);
	glm_rotate_y(xform, GLM_PI/30, xform);
	glm_rotate_x(xform, GLM_PI/30, xform);
	glm_mat4_pick3(xform, lform);
	crd_add_xform(teapot, lform);
}

static void pressed_ror(void)
{
	mat3 lform;
	crd_get_xform(teapot, lform);
	mat4 xform;
	glm_mat4_identity(xform);
	glm_mat4_ins3(lform, xform);
	glm_rotate_y(xform, -GLM_PI/30, xform);
	glm_rotate_x(xform, -GLM_PI/30, xform);
	glm_mat4_pick3(xform, lform);
	crd_add_xform(teapot, lform);
}

int main(int argc, char **argv)
{
	crd_action_t up = crd_register_action("up", GLFW_KEY_UP);
	crd_action_t down = crd_register_action("down", GLFW_KEY_DOWN);
	crd_action_t left = crd_register_action("left", GLFW_KEY_LEFT);
	crd_action_t right = crd_register_action("right", GLFW_KEY_RIGHT);
	crd_action_t in = crd_register_action("in", GLFW_KEY_PAGE_UP);
	crd_action_t out = crd_register_action("out", GLFW_KEY_PAGE_DOWN);
	crd_action_t rol = crd_register_action("rol", GLFW_KEY_Q);
	crd_action_t ror = crd_register_action("ror", GLFW_KEY_E);

	crd_init(argc, argv);

	crd_on_action_released(up, &pressed_up);
	crd_on_action_released(down, &pressed_down);
	crd_on_action_released(left, &pressed_left);
	crd_on_action_released(right, &pressed_right);
	crd_on_action_released(in, &pressed_in);
	crd_on_action_released(out, &pressed_out);
	crd_on_action_released(rol, &pressed_rol);
	crd_on_action_released(ror, &pressed_ror);

	teapot = crd_new_entity("Teapot");
	vec3 teapot_pos = {0, 0, 0};
	crd_add_pos(teapot, teapot_pos);
	crd_mesh_t teapot_mesh = crd_mesh_resource("crd/meshes/teapot.mesh");
	crd_add_mesh(teapot, teapot_mesh);

	crd_entity_t cam = crd_new_entity("Camera");
	vec3 cam_pos = {0, 1.44, -25.49};
	crd_add_pos(cam, cam_pos);
	crd_add_frustum(cam, 0.1, 100, 1.29, -1);
	crd_set_camera(cam);

	return crd_game_loop();
}
