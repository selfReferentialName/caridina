MESHES_OBJ!=ls meshes/*.obj

crd/meshes:
	mkdir -p crd/meshes

.for mesh in $(MESHES_OBJ)
crd/$(mesh:S/.obj/.mesh/:S/.bin/.mesh/): crd/meshes tool/meshify $(mesh)
	tool/meshify crd/$(mesh:S/.obj/.mesh/:S/.bin/.mesh/) $(mesh)
RESOURCES+=crd/$(mesh:S/.obj/.mesh/:S/.bin/.mesh/)
.endfor
