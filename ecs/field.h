/** @file ecs/field.h
 * @brief Information about ECS fields.
 */
#ifndef ECS_FIELD_H
#define ECS_FIELD_H

#include "ecs/internal.h"
#include "util/hash.h"

#include <pthread.h>
#include <stdatomic.h>

void ecs_field_init(void); ///< Initialisation fo field stuff

/// Maximum number of fields to recycle (otherwise shuffle stuff to reduce num).
/// Works out to 4 on amd64 linux glibc.
#define ECS_FIELD_RECYCLE_NUM ((64 - sizeof(pthread_mutex_t) - 2*sizeof(ecs_entity_t)) / sizeof(ecs_entity_t))

/// Double buffered field ID allocation information.
struct ecs_field_ids {
	pthread_mutex_t lock; ///< Acquired when IDs can't change.
	ecs_entity_t back; ///< Maximum  number of ids without resizing.
	ecs_entity_t num; ///< Maximum allocated id plus one.
	ecs_entity_t recycle[ECS_FIELD_RECYCLE_NUM]; ///< Array with IDs to recycle and padding (ecs_entity_t) -1 after the last ID.
};

extern struct ecs_field_ids ecs_field_ids[(1<<8) * sizeof(ecs_field_t)]; ///< Double buffered field ID information.

/// Double buffered field information other than ID allocation.
struct ecs_field_info {
	// First cache line
	struct hash_table entity_to_id; ///< Hash table from entity ID to ID in backing array.
	ecs_entity_t *id_to_entity; ///< Malloced array from backing array ID to entity ID.
	const char *name; ///< Name of this field.
	// Second cache line
	union {
		size_t comp_size; ///< (For components) The size of the data.
		struct {
			ecs_field_t arch_link_count; ///< (For archetypes) Number of linked components.
			ecs_field_t arch_excl_count; ///< (For archetypes) Number of excluded components.
		};
	};
	union {
		char *comp_data; ///< (For components) The malloced archetype free data backing.
		char **arch_data; ///< (For archetypes) The malloced data backing arrays.
	};
	union {
		ecs_field_t *arch_link; ///< (For archetypes) Array of linked components. Sorted.
	};
	union {
		ecs_field_t *arch_excl; ///< (For archetypes) Array of excluded components.
	};
	ecs_field_t *supers; ///< Array of fields which are supersets of this.
	ecs_field_t super_count; ///< Number of supersets of this.
	ecs_field_t super_back; ///< Backing array size of supers.
	ecs_field_t pad0; ///< Pad to align.
	bool is_comp; ///< True for components, false for archetypes.
	bool pad1; ///< Pad to align.
	void *pad2[3]; ///< Pad to get to cache line size.
};

extern struct ecs_field_info ecs_fields[(1<<8) * sizeof(ecs_field_t)]; ///< Field information.
extern pthread_mutex_t ecs_field_supers_lock[(1<<8) * sizeof(ecs_field_t)]; ///< Locks out the field supers.

extern struct hash_table ecs_field_by_name; ///< Hash table to lookup a field by its name.
extern ecs_field_t ecs_field_next; ///< Next field ID to allocate.
extern struct hash_table_vlk ecs_field_by_comps; ///< Hash table to lookup a field by its sorted components.
extern pthread_mutex_t ecs_field_by_name_lock; ///< Locks out ecs_field_by_name, ecs_field_by_comps and ecs_field_next.

#endif
