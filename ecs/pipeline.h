/** @file ecs/pipeline.h
 * @brief Internal declarations related to pipelines.
 */
#ifndef ECS_PIPELINE_H
#define ECS_PIPELINE_H

#include "ecs/api.h"

void ecs_pipeline_init(void); ///< Called by ecs_init to get pipelines ready to run.

#endif
