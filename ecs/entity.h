/** @file ecs/entity.h
 * @brief ECS internal entity stuff.
 */
#ifndef ECS_ENTITY_H
#define ECS_ENTITY_H

#include "ecs/internal.h"

#include <pthread.h>

void ecs_entity_init(void); ///< Allocate entity tables.

extern pthread_mutex_t ecs_entity_new_lock; ///< Lock out stuff related to acquiring and freeing entity handles.
extern ecs_entity_t ecs_entity_back; ///< Backing size of the entity tables.
extern ecs_entity_t ecs_entity_num; ///< Maximum allocated entity ID.
extern ecs_entity_t *ecs_entity_recycle; ///< Array of entity IDs which can be freed terminated with 0.
extern ecs_entity_t ecs_entity_recycle_num; ///< Number of elements in ecs_entity_recycle
extern ecs_entity_t ecs_entity_recycle_back; ///< Maximum number of elements in ecs_entity_recycle.

extern const char **ecs_entity_name; ///< Array of entity names.
extern ecs_field_t *ecs_entity_fields; ///< Array of what field an entity has.

#endif
