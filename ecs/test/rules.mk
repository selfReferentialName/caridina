ecs/test/a: ecs/test/a.o $(OBJECTS)
	$(LD) ecs/test/a.o $(OBJECTS) $(LFLAGS) -o ecs/test/a

check-ecs-a: ecs/test/a
	valgrind ecs/test/a

TESTS+=check-ecs-a
