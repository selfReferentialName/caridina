#include "ecs/api.h"
#include "util/log.h"
#include "attributes.h"

#include <stdio.h>
#include <math.h>
#include <string.h>

static ecs_field_t ca, cb, pos, vel;
static ecs_field_t cab, posvel;

#define n_a 19
#define n_b 13
#define n_ab 1
#define n_p 10
#define n_pv 2

static ecs_entity_t a[n_a];
static ecs_entity_t b[n_b];
static ecs_entity_t ab[n_ab];
static ecs_entity_t p[n_p];
static ecs_entity_t pv[n_pv];

bool gpu_demo_mesh;

static void check_ab(ecs_entity_t num, ecs_entity_t *ids, UNUSED void **data)
{
	for (ecs_entity_t i = 0; i < num; i++) {
		bool in = false;
		for (int j = 0; j < n_ab; j++) {
			in = in || ab[j] == ids[j];
		}
		log_assert(in);
	}
}

static void run_move(ecs_entity_t num, UNUSED ecs_entity_t *ids, void **data)
{
	logs("Running move", CRD_LOG_ALWAYS);
	double *p = data[pos];
	double *v = data[vel];
	for (ecs_entity_t i = 0; i < num; i++) {
		log_f(CRD_LOG_ALWAYS, "i = %u p = %f v = %f", i, p[i], v[i]);
		p[i] += v[i];
	}
}

static void check_zero_pos(ecs_entity_t num, ecs_entity_t *ids, void **data)
{
	for (ecs_entity_t i = 0; i < num; i++) {
		if (ids[i] == 0) continue;
		log_f(CRD_LOG_ALWAYS, "Made it here with i == %u", i);
		bool found_in_pv = false;
		for (int j = 0; j < n_pv; j++) {
			if (pv[j] == ids[i]) found_in_pv = true;
		}
		if (!found_in_pv) log_f(CRD_LOG_ALWAYS, "Entity %u isn't in pv", ids[i]);
		log_assert(found_in_pv);
		double p = ((double *) data[pos])[i];
		if (fabs(p) > 0.01) log_f(CRD_LOG_ALWAYS, "entity %d pos = %f vel = %f", (int) ids[i], p, ((double *) data[vel])[i]);
		log_assert(fabs(p) < 0.1);
	}
}

int main(int argc, char **argv)
{
	ecs_init();

	struct ecs_component_info pos_info = {
		.name = "position",
		.size = sizeof(double)
	};
	pos = ecs_create_component(&pos_info);
	struct ecs_component_info vel_info = {
		.name = "velocity",
		.size = sizeof(double)
	};
	vel = ecs_create_component(&vel_info);
	struct ecs_component_info a_info = {
		.name = "a",
		.size = 0
	};
	ca = ecs_create_component(&a_info);
	struct ecs_component_info b_info = {
		.name = "b",
		.size = 0
	};
	cb = ecs_create_component(&b_info);

	log_assert(cb == ecs_create_component(&b_info));

	ecs_field_t ab_comps[3] = {ca, cb, ca};
	struct ecs_archetype_info ab_info = {
		.name = "ab",
		.comp_count = 3,
		.comps = ab_comps
	};
	cab = ecs_create_archetype(&ab_info);

	log_assert(cab == ecs_create_archetype(&ab_info));

	ecs_field_t pv_comps[2] = {pos, vel};
	struct ecs_archetype_info pv_info = {
		.name = "position+velocity",
		.comp_count = 2,
		.comps = pv_comps
	};
	posvel = ecs_create_archetype(&pv_info);

	int ra = n_a;
	int rb = n_b;
	int rab = n_ab;
	int rp = n_p;
	int rpv = n_pv;
	memset(a, -1, sizeof(a));
	memset(b, -1, sizeof(b));
	memset(ab, -1, sizeof(ab));
	memset(p, -1, sizeof(p));
	memset(pv, -1, sizeof(pv));
	while (ra + rb + rab + rp + rpv) {
		int i = rand() % (ra + rb + rab + rp + rpv);
		if (i < ra) {
			ra--;
			a[ra] = ecs_new_entity("a");
			ecs_add_comps(a[ra], 1, &ca, NULL);
			log_f(CRD_LOG_ALWAYS, "a[%d] = %d", (int) ra, (int) a[ra]);
		} else if (i < ra + rb) {
			rb--;
			b[rb] = ecs_new_entity("b");
			ecs_add_comps(b[rb], 1, &cb, NULL);
			log_f(CRD_LOG_ALWAYS, "b[%d] = %d", (int) rb, (int) b[rb]);
		} else if (i < ra + rb + rab) {
			rab--;
			ab[rab] = ecs_new_entity("ab");
			const void *data[2] = { NULL, NULL };
			ecs_field_t fields[2] = { ca, cb };
			ecs_add_comps(ab[rab], 2, fields, data);
			log_f(CRD_LOG_ALWAYS, "ab[%d] = %d", (int) rab, (int) ab[rab]);
		} else if (i < ra + rb + rab + rp) {
			rp--;
			p[rp] = ecs_new_entity("p");
			double ps = rand();
			const void *data = &ps;
			ecs_add_comps(p[rp], 1, &pos, &data);
			log_f(CRD_LOG_ALWAYS, "p[%d] = %d", (int) rp, (int) p[rp]);
		} else {
			rpv--;
			pv[rpv] = ecs_new_entity("pv");
			double ps = 4;
			double vl = -1;
			const void *data[2] = {&vl, &ps};
			ecs_add_comps(pv[rpv], 1, &vel, data);
			ecs_add_comps(pv[rpv], 1, &pos, &data[1]);
			log_f(CRD_LOG_ALWAYS, "pv[%d] = %d", (int) rpv, (int) pv[rpv]);
		}
	}

	struct ecs_system check_ab_sys = {
		.run = &check_ab,
		.field = cab,
		.name = "Check AB",
		.want_ids = true
	};
	struct ecs_system check_zero_pos_sys = {
		.run = &check_zero_pos,
		.field = posvel,
		.name = "Check Zeroed Position",
		.want_ids = true
	};
	struct ecs_system move_sys = {
		.run = &run_move,
		.field = posvel,
		.name = "Move",
		.writes = {
			pos
		}
	};

	ecs_run_system(check_ab_sys);
	for (int i = 0; i < 4; i++) {
		ecs_run_system(move_sys);
	}
	ecs_run_system(check_zero_pos_sys);

	for (int i = 0; i < n_pv; i++) {
		double ps = rand() % 1000000;
		double vl = -ps / 4.;
		ecs_set_data(pv[i], pos, &ps);
		ecs_set_data(pv[i], vel, &vl);
	}

	struct ecs_pipeline stage_move_2 = {
		.system = move_sys
	};
	struct ecs_pipeline stage_check_ab = {
		.system = check_ab_sys,
		.next = &stage_move_2
	};
	struct ecs_pipeline stage_move_1 = {
		.system = move_sys,
		.next = &stage_check_ab
	};

	ecs_run_pipeline(&stage_move_1);

	ecs_drop_comps(ab[0], 1, &ca);
	ab[0] = 0;
	ecs_delete_entity(ab[1]);
	ab[1] = 0;

	ecs_run_pipeline(&stage_move_1);

	ecs_run_system(check_zero_pos_sys);

	return 0;
}

