#include "ecs/entity.h"
#include "ecs/pipeline.h"
#include "ecs/field.h"

void ecs_init(void)
{
	ecs_entity_init();
	ecs_pipeline_init();
	ecs_field_init();
}
