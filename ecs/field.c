#include "ecs/field.h"
#include "util/log.h"
#include "util/mem/immortal.h"
#include "util/xmalloc.h"

#include <string.h>
#include <alloca.h>
#include <stdlib.h>

#define NULL_OR(x, y) ((x) ? x : y)

struct ecs_field_ids ecs_field_ids[(1<<8) * sizeof(ecs_field_t)];

struct ecs_field_info ecs_fields[(1<<8) * sizeof(ecs_field_t)];

struct hash_table ecs_field_by_name = {
	.keysize = ECS_MAX_NAME_LEN,
	.valsize = sizeof(ecs_field_t),
	.count = 2048
};
ecs_field_t ecs_field_next = 1;
struct hash_table_vlk ecs_field_by_comps = {
	.valsize = sizeof(ecs_field_t),
	.count = 2048
};
pthread_mutex_t ecs_field_by_name_lock = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t ecs_field_supers_lock[(1<<8) * sizeof(ecs_field_t)];

ecs_field_t ecs_field_next;
struct hash_table_vlk ecs_field_by_comps;

// Valgrind weirdness
void ecs_field_init(void)
{
#ifdef NDEBUG
	memset(ecs_field_ids, 0, sizeof(ecs_field_ids));
	memset(ecs_fields, 0, sizeof(ecs_fields));
#endif
}

#define ID_BACK0 128

// requires supers lock
static bool is_super(ecs_field_t sub, ecs_field_t sup)
{
	if (ecs_fields[sub].is_comp) {
		// the component is linked
		for (ecs_field_t i = 0; i < ecs_fields[sup].arch_link_count; i++) {
			if (ecs_fields[sup].arch_link[i] == sub) return true;
		}
		return false;
	} else {
		// everything in the sub is in the super
		for (ecs_field_t i = 0; i < ecs_fields[sub].arch_link_count; i++) {
			bool found = false;
			for (ecs_field_t j = 0; j < ecs_fields[sup].arch_link_count; j++) {
				if (ecs_fields[sub].arch_link[i] == ecs_fields[sup].arch_link[j]) found = true;
			}
			if (!found) {
				return false;
			}
		}
		// and everything *not* in the sub isn't in the super
		for (ecs_field_t i = 0; i < ecs_fields[sub].arch_excl_count; i++) {
			for (ecs_field_t j = 0; j < ecs_fields[sup].arch_link_count; j++) {
				if (ecs_fields[sub].arch_excl[i] == ecs_fields[sup].arch_link[j]) {
					return false;
				}
			}
		}
		return true;
	}
}

// locks supers
static void add_super(ecs_field_t sub, ecs_field_t super)
{
	if (sub == super) return;
	pthread_mutex_lock(&ecs_field_supers_lock[sub]);
	for (ecs_field_t i = 0; i < ecs_fields[sub].super_count; i++) {
		if (ecs_fields[sub].supers[i] == super) {
			pthread_mutex_unlock(&ecs_field_supers_lock[sub]);
			return;
		}
	}
	ecs_field_t idx = ecs_fields[sub].super_count;
	if (idx >= ecs_fields[sub].super_back) {
		ecs_fields[sub].super_back *= 2;
		if (idx >= ecs_fields[sub].super_back) ecs_fields[sub].super_back = idx + 1;
		ecs_fields[sub].supers = realloc(ecs_fields[sub].supers, ecs_fields[sub].super_back * sizeof(ecs_field_t));
	}
	ecs_fields[sub].supers[idx] = super;
	ecs_fields[sub].super_count++;
	pthread_mutex_unlock(&ecs_field_supers_lock[sub]);
	log_f(CRD_LOG_VERBOSE, "Field %s (%u) is a super of %s (%u)",
			NULL_OR(ecs_fields[super].name, "auto"), (unsigned) super,
			NULL_OR(ecs_fields[sub].name, "auto"), (unsigned) sub); // TODO: make CRD_LOG_VERY_VERBOSE
}

// called when a new archetype is made to set its children's supers
// needs supers to be locked
static void recalc_field_supers(ecs_field_t f)
{
	log_assert(!ecs_fields[f].is_comp);
	for (ecs_field_t i = 0; i < ecs_fields[f].arch_link_count; i++) {
		ecs_field_t c = ecs_fields[f].arch_link[i];
		add_super(c, f); // f is a super of all of its components
		pthread_mutex_lock(&ecs_field_supers_lock[c]);
		for (ecs_field_t j = 0; j < ecs_fields[c].super_count; j++) {
			if (is_super(f, ecs_fields[c].supers[j])) {
				add_super(f, ecs_fields[c].supers[j]); // any of f's components' supers could be a super of f
			}
			if (is_super(ecs_fields[c].supers[j], f)) {
				add_super(ecs_fields[c].supers[j], f); // and vice versa; f could be a super of any of its components' supers
			}
		}
		pthread_mutex_unlock(&ecs_field_supers_lock[c]);
	}
}

static bool name_field(const char *in_name, ecs_field_t *r)
{
	char name[ECS_MAX_NAME_LEN];
	memset(name, 0, ECS_MAX_NAME_LEN);
	if (in_name) {
		log_assert(strlen(in_name) < ECS_MAX_NAME_LEN);
		strcpy(name, in_name);
		pthread_mutex_lock(&ecs_field_by_name_lock);
		if (hash_table_get(ecs_field_by_name, name, r)) {
			pthread_mutex_unlock(&ecs_field_by_name_lock);
			return true;
		}
	}
	*r = ecs_field_next;
	ecs_field_next++;
	log_assert(ecs_field_next);
	hash_table_set(&ecs_field_by_name, name, r);
	pthread_mutex_unlock(&ecs_field_by_name_lock);
	return false;
}

ecs_field_t ecs_create_component(struct ecs_component_info *info)
{
	ecs_field_t r;
	if (name_field(info->name, &r)) return r;

	pthread_mutex_lock(&ecs_field_by_name_lock);
	hash_table_vlk_set(&ecs_field_by_comps, &r, sizeof(ecs_field_t), &r);
	pthread_mutex_unlock(&ecs_field_by_name_lock);

	ecs_fields[r] = (struct ecs_field_info) {
		.entity_to_id = {
			.keysize = sizeof(ecs_entity_t),
			.valsize = sizeof(ecs_entity_t),
			.count = 128
		},
		.id_to_entity = xmalloc(sizeof(ecs_entity_t)*ID_BACK0),
		.name = info->name,
		.comp_size = info->size,
		.comp_data = xmalloc(info->size*ID_BACK0),
		.is_comp = true
	};
	pthread_mutex_init(&ecs_field_supers_lock[r], NULL);

	pthread_mutex_init(&ecs_field_ids[r].lock, NULL);
	ecs_field_ids[r].back = ID_BACK0;
	memset(ecs_field_ids[r].recycle, -1, sizeof(ecs_field_ids[r].recycle));

	pthread_mutex_lock(&ecs_field_by_name_lock);
	hash_table_vlk_set(&ecs_field_by_comps, &r, sizeof(ecs_field_t), &r);
	pthread_mutex_unlock(&ecs_field_by_name_lock);

	log_f(CRD_LOG_VERBOSE, "Created component %s (%u)", info->name, r);

	return r;
}

ecs_field_t ecs_create_archetype(struct ecs_archetype_info *info)
{
	ecs_field_t r;

	// sort and deduplicate components so order can't vary
	for (ecs_field_t i = 0; i < info->comp_count; i++) {
		for (ecs_field_t j = i; j > 0; j--) {
			if (info->comps[j] > info->comps[j - 1]) {
				break;
			} else if (info->comps[j] == info->comps[j - 1]) {
				info->comp_count--;
				info->comps[j] = info->comps[info->comp_count];
				i = -1; j = 1; continue; // restart outer loop
			}
			ecs_field_t t = info->comps[j];
			info->comps[j] = info->comps[j - 1];
			info->comps[j - 1] = t;
		}
	}

	if (info->comp_count == 0) return 0;
	if (info->comp_count == 1 && info->ex_comp_count == 0) {
		return info->comps[0];
	}

	if (info->ex_comp_count == 0) { // don't add or remove spurious exclusions
		pthread_mutex_lock(&ecs_field_by_name_lock);
		if (hash_table_vlk_get(&ecs_field_by_comps, info->comps, sizeof(ecs_field_t)*info->comp_count, &r)) {
			log_f(CRD_LOG_VERBOSE, "%p[%u].name == %p", ecs_fields, (unsigned) r, ecs_fields[r].name);
			if (ecs_fields[r].name == NULL) {
				ecs_fields[r].name = info->name;
			}
			pthread_mutex_unlock(&ecs_field_by_name_lock);
			return r;
		}
		pthread_mutex_unlock(&ecs_field_by_name_lock);
	}

	if (name_field(info->name, &r)) return r;
	pthread_mutex_lock(&ecs_field_by_name_lock);
	hash_table_vlk_set(&ecs_field_by_comps, info->comps, sizeof(ecs_field_t)*info->comp_count, &r);
	pthread_mutex_unlock(&ecs_field_by_name_lock);

	ecs_fields[r] = (struct ecs_field_info) {
		.entity_to_id = {
			.keysize = sizeof(ecs_entity_t),
			.valsize = sizeof(ecs_entity_t),
			.count = 128
		},
		.id_to_entity = xmalloc(sizeof(ecs_entity_t)*ID_BACK0),
		.name = info->name,
		.arch_link_count = info->comp_count,
		.arch_excl_count = info->ex_comp_count,
		.arch_data = mem_imrtl_malloc(sizeof(void *)*info->comp_count),
		.arch_link = mem_imrtl_malloc(sizeof(ecs_entity_t)*info->comp_count),
		.arch_excl = mem_imrtl_malloc(sizeof(ecs_entity_t)*info->ex_comp_count),
		.is_comp = false
	};
	pthread_mutex_init(&ecs_field_supers_lock[r], NULL);
	for (ecs_field_t i = 0; i < info->comp_count; i++) {
		ecs_fields[r].arch_data[i] = xmalloc(ecs_fields[info->comps[i]].comp_size * ID_BACK0);
		ecs_fields[r].arch_link[i] = info->comps[i]; // TODO: make sure these are components
	}
	for (ecs_field_t i = 0; i < info->ex_comp_count; i++) {
		ecs_fields[r].arch_excl[i] = info->ex_comps[i]; // TODO: make sure these are components
	}

	pthread_mutex_init(&ecs_field_ids[r].lock, NULL);
	ecs_field_ids[r].back = ID_BACK0;
	memset(ecs_field_ids[r].recycle, -1, sizeof(ecs_field_ids[r].recycle));

	recalc_field_supers(r);

	log_f(CRD_LOG_VERBOSE, "Created archetype %u (%s)", (unsigned) r, NULL_OR(info->name, "auto"));

	return r;
}

ecs_field_t ecs_get_field(const char *in_name)
{
	char name[ECS_MAX_NAME_LEN];
	memset(name, 0, ECS_MAX_NAME_LEN);
	log_assert(strlen(in_name) < ECS_MAX_NAME_LEN);
	strcpy(name, in_name);
	ecs_field_t r;
	pthread_mutex_lock(&ecs_field_by_name_lock);
	if (!hash_table_get(ecs_field_by_name, name, &r)) {
		r = 0;
	}
	pthread_mutex_unlock(&ecs_field_by_name_lock);
	return r;
}

void ecs_get_comps(ecs_field_t field, ecs_field_t *num, ecs_field_t *out)
{
	if (ecs_fields[field].is_comp) {
		*num = 1;
		if (out) {
			*out = field;
		}
	} else {
		*num = ecs_fields[field].arch_link_count;
		if (out) {
			for (ecs_field_t i = 0; i < *num; i++) {
				out[i] = ecs_fields[field].arch_link[i];
			}
		}
	}
}

size_t ecs_get_size(ecs_field_t field)
{
	if (ecs_fields[field].is_comp) {
		return ecs_fields[field].comp_size;
	} else {
		size_t r = 0;
		for (ecs_field_t i = 0; i < ecs_fields[field].arch_link_count; i++) {
			r += ecs_fields[ecs_fields[field].arch_link[i]].comp_size;
		}
		return r;
	}
}

const char *ecs_get_field_name(ecs_field_t field)
{
	return ecs_fields[field].name;
}
