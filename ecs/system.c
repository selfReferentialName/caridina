#include "ecs/field.h"
#include "util/mem/pool.h"
#include "util/log.h"

#include <alloca.h>

static pthread_mutex_t data_index_lock = PTHREAD_MUTEX_INITIALIZER; // lock out data_index_pool
static struct mem_pool *data_index_pool; // a pool of arrays from component to data

static void run_on(struct ecs_system sys, ecs_field_t f)
{
	pthread_mutex_lock(&data_index_lock);
	void **data = mem_pool_alloc(data_index_pool);
	pthread_mutex_unlock(&data_index_lock);

	pthread_mutex_lock(&ecs_field_ids[f].lock);
	ecs_entity_t *ids = NULL;
	if (sys.want_ids) {
		ids = alloca(sizeof(ecs_entity_t) * ecs_field_ids[f].num);
		for (ecs_entity_t i = 0; i < ecs_field_ids[f].num; i++) {
			ids[i] = ecs_fields[f].id_to_entity[i];
		}
	}
	if (ecs_fields[f].is_comp) {
		data[f] = ecs_fields[f].comp_data;
	} else {
		for (ecs_field_t i = 0; i < ecs_fields[f].arch_link_count; i++) { // TODO: writes more than necessary
			data[ecs_fields[f].arch_link[i]] = ecs_fields[f].arch_data[i];
		}
	}
	sys.run(ecs_field_ids[f].num, ids, data);
	pthread_mutex_unlock(&ecs_field_ids[f].lock);
}

#define NULL_OR(x, y) ((x) ? x : (y))

void ecs_run_system(struct ecs_system system)
{
	pthread_mutex_lock(&data_index_lock);
	if (!data_index_pool) {
		data_index_pool = mem_pool_new(sizeof(void *) * (1 << 8 << sizeof(ecs_field_t)));
	}
	pthread_mutex_unlock(&data_index_lock);

	ecs_field_t f = system.field;
	pthread_mutex_lock(&ecs_field_supers_lock[f]);
	for (ecs_field_t i = 0; i < ecs_fields[f].super_count; i++) {
		log_f(CRD_LOG_VERY_VERBOSE, "Running system %s on field %s (%u)",
				system.name,
				NULL_OR(ecs_fields[ecs_fields[f].supers[i]].name, "auto"),
				(unsigned) ecs_fields[f].supers[i]);
		run_on(system, ecs_fields[f].supers[i]);
	}
	pthread_mutex_unlock(&ecs_field_supers_lock[f]);
	log_f(CRD_LOG_VERY_VERBOSE, "Running system %s on field %s (%u)", system.name, NULL_OR(ecs_fields[f].name, "auto"), (unsigned) f);
	run_on(system, f);
}
