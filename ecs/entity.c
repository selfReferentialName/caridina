#include "ecs/entity.h"
#include "ecs/field.h"
#include "util/log.h"
#include "util/xmalloc.h"

#include <stdlib.h>
#include <string.h>

pthread_mutex_t ecs_entity_new_lock = PTHREAD_MUTEX_INITIALIZER;
ecs_entity_t ecs_entity_back;
ecs_entity_t ecs_entity_num;
ecs_entity_t *ecs_entity_recycle;
ecs_entity_t ecs_entity_recycle_num;
ecs_entity_t ecs_entity_recycle_back;
const char **ecs_entity_name;
ecs_field_t *ecs_entity_fields;

void ecs_entity_init(void)
{
	ecs_entity_back = 512;
	ecs_entity_recycle_back = 1024;
	ecs_entity_recycle = xmalloc(sizeof(ecs_entity_t) * ecs_entity_recycle_back);
	ecs_entity_name = xmalloc(sizeof(const char *) * ecs_entity_back);
	ecs_entity_fields = xmalloc(sizeof(ecs_field_t) * ecs_entity_back);
}

ecs_entity_t ecs_new_entity(const char *name)
{
	ecs_entity_t r;
	pthread_mutex_lock(&ecs_entity_new_lock);
	if (ecs_entity_recycle_num == 0) { // nothing to recycle
		ecs_entity_num++;
		r = ecs_entity_num;
		if (r >= ecs_entity_back) { // expand the backing
			ecs_entity_back *= 2;
			ecs_entity_name = realloc(ecs_entity_name, sizeof(char *)*ecs_entity_back);
			ecs_entity_fields = realloc(ecs_entity_fields, sizeof(ecs_field_t)*ecs_entity_back);
		}
	} else { // get to recycle
		r = ecs_entity_recycle[0];
		ecs_entity_recycle[0] = ecs_entity_recycle[ecs_entity_recycle_num - 1];
		ecs_entity_recycle_num--;
	}
	ecs_entity_name[r] = name;
	ecs_entity_fields[r] = 0;
	pthread_mutex_unlock(&ecs_entity_new_lock);
	return r;
}

// swap all recycles with IDs at the end drop the number of ids
// needs the ids already locked
static void empty_recycles(ecs_field_t f, struct ecs_field_ids *ids)
{
	for (int i = 0; i < ECS_FIELD_RECYCLE_NUM; i++) {
		// TODO: we don't need to do this much if id_rec == id_num - 1
		ecs_entity_t id_rec = ids->recycle[i];
		ecs_entity_t id_liv = ids->num - 1;
		ecs_entity_t entity = ecs_fields[f].id_to_entity[id_rec];
		log_f(CRD_LOG_VERY_VERBOSE, "setting ecs_fields[%u].entity_to_id[%u] = %u", (unsigned) f, entity, (unsigned) id_liv);
		hash_table_set(&ecs_fields[f].entity_to_id, &entity, &id_liv);
		ecs_fields[f].id_to_entity[id_liv] = entity;
		ids->num--;
	}
}

// acquires ecs_field_ids[f].lock
static void remove_id(ecs_entity_t e, ecs_field_t f)
{
	if (f == 0) return;

	pthread_mutex_lock(&ecs_field_ids[f].lock);
	ecs_entity_t id;
	log_assert(hash_table_get(ecs_fields[f].entity_to_id, &e, &id));
	if (id == ecs_field_ids[f].num - 1) { // get to drop last
		ecs_field_ids[f].num--;
	} else { // need to recycle
		for (int i = 0; i < ECS_FIELD_RECYCLE_NUM; i++) {
			if (ecs_field_ids[f].recycle[i] == (ecs_entity_t) -1) {
				ecs_field_ids[f].recycle[i] = id;
				goto recycled;
			}
		}
		empty_recycles(f, &ecs_field_ids[f]);
		ecs_field_ids[f].recycle[0] = id;
recycled:
	}
	hash_table_remove(&ecs_fields[f].entity_to_id, &e);
	ecs_fields[f].id_to_entity[id] = 0;
	pthread_mutex_unlock(&ecs_field_ids[f].lock);
}

// acquires field ids lock
static void remove_field(ecs_entity_t e)
{
	ecs_field_t f = ecs_entity_fields[e];
	remove_id(e, f);
}

void ecs_delete_entity(ecs_entity_t entity)
{
	remove_field(entity);

	pthread_mutex_lock(&ecs_entity_new_lock);
	if (ecs_entity_recycle_num >= ecs_entity_recycle_back) {
		ecs_entity_recycle_back *= 2;
		ecs_entity_recycle = realloc(ecs_entity_recycle, sizeof(ecs_entity_t)*ecs_entity_recycle_back);
	}
	ecs_entity_recycle[ecs_entity_recycle_num] = entity;
	ecs_entity_recycle_num++;
	pthread_mutex_unlock(&ecs_entity_new_lock);
}

static void ensure_field_id(ecs_entity_t e, ecs_field_t f)
{
	if (f == 0) return;

	pthread_mutex_lock(&ecs_field_ids[f].lock);
	ecs_entity_t id;
	if (hash_table_get(ecs_fields[f].entity_to_id, &e, &id)) { // field already made
		pthread_mutex_unlock(&ecs_field_ids[f].lock);
		return;
	}
	for (int i = ECS_FIELD_RECYCLE_NUM - 1; i >= 0; i--) { // can we recycle?
		if (ecs_field_ids[f].recycle[i] != (ecs_entity_t) -1) { // yes
			id = ecs_field_ids[f].recycle[i];
			ecs_field_ids[f].recycle[i] = (ecs_entity_t) -1;
			goto recycled;
		}
	}
	// we can't recycle
	id = ecs_field_ids[f].num;
	ecs_field_ids[f].num++;
	if (ecs_field_ids[f].num >= ecs_field_ids[f].back) {
		ecs_field_ids[f].back *= 2;
		if (ecs_fields[f].is_comp) {
			ecs_fields[f].comp_data = realloc(ecs_fields[f].comp_data,
					ecs_fields[f].comp_size*ecs_field_ids[f].back);
		} else {
			for (ecs_field_t i = 0; i < ecs_fields[f].arch_link_count; i++) {
				ecs_fields[f].arch_data[i] = realloc(ecs_fields[f].arch_data[i],
						ecs_fields[ecs_fields[f].arch_link[i]].comp_size*ecs_field_ids[f].back);
			}
		}
		ecs_fields[f].id_to_entity = realloc(ecs_fields[f].id_to_entity, sizeof(ecs_entity_t)*ecs_field_ids[f].back);
	}
recycled:
	log_f(CRD_LOG_VERY_VERBOSE, "ecs_fields[%u].id_to_entity[%u] = %u; ecs_fields[f].id_to_entity = %p", f, e, id, ecs_fields[f].id_to_entity);
	ecs_fields[f].id_to_entity[id] = e;
	hash_table_set(&ecs_fields[f].entity_to_id, &e, &id);
	pthread_mutex_unlock(&ecs_field_ids[f].lock);
}

static void *get_data_loc(ecs_field_t f, ecs_field_t c, ecs_entity_t id)
{
	size_t size = ecs_fields[c].comp_size;
	if (ecs_fields[f].is_comp) {
		return ecs_fields[f].comp_data + size*id;
	} else {
		for (ecs_field_t i = 0; i < ecs_fields[f].arch_link_count; i++) {
			if (ecs_fields[f].arch_link[i] == c) {
				return ecs_fields[f].arch_data[i] + size*id;
			}
		}
	}
	log_unreachable();
}

static void copy_data(ecs_entity_t e, ecs_field_t f0, ecs_field_t f1)
{
	if (f0 == 0) return;
	if (f0 == f1) return;

	pthread_mutex_lock(&ecs_field_ids[f0].lock);

	ecs_field_t id0; // initial id
	log_assert(hash_table_get(ecs_fields[f0].entity_to_id, &e, &id0)); // get the initial id

	pthread_mutex_lock(&ecs_field_ids[f1].lock);

	ecs_field_t id1; // updated id
	log_assert(hash_table_get(ecs_fields[f1].entity_to_id, &e, &id1));

	if (ecs_fields[f0].is_comp) { // set the old data
		memcpy(get_data_loc(f1, f0, id1), get_data_loc(f0, f0, id0), ecs_fields[f0].comp_size);
	} else if (ecs_fields[f1].is_comp) {
		// nothing to do, there can be no old data
	} else { // f0 has at least 2 components so type is an archetype
		for (ecs_field_t i = 0; i < ecs_fields[f0].arch_link_count; i++) {
			ecs_field_t c = ecs_fields[f1].arch_link[i];
			size_t size = ecs_fields[c].comp_size;
			memcpy(ecs_fields[f1].arch_data[i] + size*id1,
					get_data_loc(f0, c, id0),
					size);
		}
	}

	pthread_mutex_unlock(&ecs_field_ids[f1].lock);
	pthread_mutex_unlock(&ecs_field_ids[f0].lock);
}


void ecs_add_comps(ecs_entity_t entity, ecs_field_t num, const ecs_field_t *comps, const void *const *data)
{
	if (num == 0) return; // don't do no change

	ecs_field_t nn = num; // new number of components
	ecs_field_t f0 = ecs_entity_fields[entity]; // initial field
	if (!ecs_fields[f0].is_comp) { // add existing components
		nn = num + ecs_fields[f0].arch_link_count;
	} else if (f0 != 0) {
		nn++;
	}
	ecs_field_t comps_sorted[nn]; // sorted components passed into ecs_create_archetype
	for (int i = 0; i < num; i++) { // add passed in components
		comps_sorted[i] = comps[i];
	}
	if (ecs_fields[f0].is_comp) { // add existing componencts
		comps_sorted[num] = f0;
	} else {
		for (int i = num; i < nn; i++) {
			comps_sorted[i] = ecs_fields[f0].arch_link[i - num];
		}
	}
	struct ecs_archetype_info create = {
		.name = NULL,
		.comp_count = nn,
		.comps = comps_sorted,
		.ex_comp_count = 0
	};
	ecs_field_t type = ecs_create_archetype(&create); // make new archetype

	if (create.comp_count != 0) {
		pthread_mutex_lock(&ecs_entity_new_lock);
		ecs_entity_fields[entity] = type; // set field
		pthread_mutex_unlock(&ecs_entity_new_lock);
		ensure_field_id(entity, type); // make sure the data is backed
		copy_data(entity, f0, type);
		remove_id(entity, f0);
	}

	for (ecs_field_t i = 0; i < num; i++) {
		if (data && data[i]) ecs_set_data(entity, comps[i], data[i]); // set the new data
	}
}

void ecs_drop_comps(ecs_entity_t entity, ecs_field_t num, const ecs_field_t *comps)
{
	// TODO: may be able to shrink where this is locked
	pthread_mutex_lock(&ecs_entity_new_lock);

	ecs_field_t f0 = ecs_entity_fields[entity];
	ecs_field_t n0;
	ecs_get_comps(f0, &n0, NULL);
	ecs_field_t c0[n0];
	ecs_get_comps(f0, &n0, c0);

	ecs_field_t c1[n0];
	ecs_field_t n1 = 0;
	for (ecs_field_t i = 0; i < n0; i++) {
		bool dropped = false;
		for (ecs_field_t j = 0; j < num; j++) {
			if (comps[j]) dropped = true;
		}
		if (!dropped) {
			c1[n1] = c0[i];
			n1++;
		}
	}

	struct ecs_archetype_info create = {
		.name = NULL,
		.comp_count = n1,
		.comps = c1,
		.ex_comp_count = 0
	};
	ecs_field_t f1 = ecs_create_archetype(&create);

	if (f1 != 0) {
		ensure_field_id(entity, f1);
		copy_data(entity, f0, f1);
	}

	remove_field(entity);
	ecs_entity_fields[entity] = f1;

	pthread_mutex_unlock(&ecs_entity_new_lock);
}

bool ecs_has_comp(ecs_entity_t entity, ecs_field_t comp)
{
	ecs_field_t f = ecs_entity_fields[entity];
	if (f == comp) {
		return true;
	} else if (f == 0) {
		return false;
	} else if (ecs_fields[f].is_comp) {
		return false;
	} else {
		for (ecs_field_t i = 0; i < ecs_fields[f].arch_link_count; i++) {
			if (ecs_fields[f].arch_link[i] == comp) {
				return true;
			}
		}
		return false;
	}
}

void ecs_get_data(ecs_entity_t entity, ecs_field_t comp, void *data)
{
	ecs_field_t f = ecs_entity_fields[entity];
	pthread_mutex_lock(&ecs_field_ids[f].lock);
	ecs_entity_t id;
	log_assert(hash_table_get(ecs_fields[f].entity_to_id, &entity, &id));
	size_t size = ecs_fields[comp].comp_size;
	memcpy(data, get_data_loc(f, comp, id), size);
	pthread_mutex_unlock(&ecs_field_ids[f].lock);
}

void ecs_set_data(ecs_entity_t entity, ecs_field_t comp, const void *data)
{
	ecs_field_t f = ecs_entity_fields[entity];
	pthread_mutex_lock(&ecs_field_ids[f].lock);
	ecs_entity_t id;
	log_assert(hash_table_get(ecs_fields[f].entity_to_id, &entity, &id));
	size_t size = ecs_fields[comp].comp_size;
	memcpy(get_data_loc(f, comp, id), data, size);
	pthread_mutex_unlock(&ecs_field_ids[f].lock);
}
