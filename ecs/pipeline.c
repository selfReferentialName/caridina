#include "ecs/pipeline.h"
#include "util/log.h"

void ecs_pipeline_init(void) {}

static void run_stage(struct ecs_pipeline *s)
{
	ecs_run_system(s->system);
}

void ecs_run_pipeline(struct ecs_pipeline *start)
{
	logs("Running a pipeline", CRD_LOG_VERY_VERBOSE);
	for (struct ecs_pipeline *cur = start; cur; cur = cur->next) {
		run_stage(cur);
	}
}
