/** @file ecs/api.h
 * @brief Entity Component System Public API
 *
 * All functions which don't have `run`, `new`, or `delete` in the name are
 * idempotent aside from `ecs_finish`.
 *
 * All functions here are mostly thread safe.
 *
 * When a system is running, it acquires a lock on specific IDs, so attempting
 * to add or remove a component a system is running on or from an entity with
 * a component a system is running on from that system can cause deadlock.
 * Basically, don't add or remove components while running a system and you'll
 * be fine.
 *
 * `ecs_run_pipeline` is not reentrant and only one call can run at a time.
 * `ecs_add_comps`, `ecs_get_data`, and `ecs_set_data` are not atomic when it
 * comes to setting and getting the data and are therefore not thread safe for
 * the same entity and component.
 *
 * 0 acts as NULL for all handles.
 */
#ifndef ECS_API_H
#define ECS_API_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

/// No name field may exceed 1 less than this length.
#define ECS_MAX_NAME_LEN 64

typedef uint16_t ecs_field_t; ///< A handle for a field.
typedef uint32_t ecs_entity_t; ///< An entity (which is just a handle).

/// Call before any ecs functions.
void ecs_init(void);

//////////////////////// COMPONENTS ////////////////////////

/// Information for creating a component.
struct ecs_component_info {
	const char *name; ///< Unique, immortal string for the name of the component. Set to NULL for internal unique fields.
	size_t size; ///< The size of the component.
};
/// Create a new component. Returns already created field with same name if present.
ecs_field_t ecs_create_component(struct ecs_component_info *info);

/// Information for creating an archetype. Returns already created field with same name if present.
struct ecs_archetype_info {
	const char *name; ///< Unique, immortal string for the name of the archetype. Set to NULL for internal unique fields.
	size_t comp_count; ///< The number of linked components.
	ecs_field_t *comps; ///< The linked components. This gets sorted for you with insertion sort. Duplicates are removed.
	size_t ex_comp_count; ///< The number of excluded components.
	ecs_field_t *ex_comps; ///< The excluded components.
};
/// Create a new archetype. Returns already created field with same name or same included and excluded components if present.
ecs_field_t ecs_create_archetype(struct ecs_archetype_info *info);

/// Get the field with a given name. Returns zero on failure.
ecs_field_t ecs_get_field(const char *name);

/** Get all components linked to a given field.
 *
 * Returns the input for a component.
 *
 * If out isn't NULL, it must be big enough.
 *
 * @param field The field to get the components of.
 * @param num The returned number of components of the field.
 * @param out The array to return the components to or NULL.
 */
void ecs_get_comps(ecs_field_t field, ecs_field_t *num, ecs_field_t *out);

/// Get the size of a field. This is the sum of the sizes for an archetype.
size_t ecs_get_size(ecs_field_t);

/// Get the name of a field. Inverse of ecs_get_field.
const char *ecs_get_field_name(ecs_field_t field);

//////////////////////// ENTITIES ////////////////////////

/// Create a new entity with a given non-unique name which lives as long as the entity.
ecs_entity_t ecs_new_entity(const char *name);

/// Delete an entity.
void ecs_delete_entity(ecs_entity_t entity);

/** @brief Add components to an entity.
 *
 * Because of archetypes, a call to this function has a cost that doesn't
 * really scale much with the number of added fields. Try to minimise calls
 * to this function.
 *
 * This function automatically creates an archetype with all fields of this component
 * if one does not already exist.
 *
 * If data is NULL, or any given data[i] is NULL, don't initialise that field.
 *
 * @param entity The entity to add components to.
 * @param num The number of components to add.
 * @param comps The component handles to add.
 * @param data The component data to add.
 */
void ecs_add_comps(ecs_entity_t entity, ecs_field_t num, const ecs_field_t *comps, const void *const *data);

/// Remove components from an entity.
void ecs_drop_comps(ecs_entity_t entity, ecs_field_t num, const ecs_field_t *comps);

/// Return true if an entity has data for a component.
bool ecs_has_comp(ecs_entity_t entity, ecs_field_t comp);

/// Get the component data from an entity.
void ecs_get_data(ecs_entity_t entity, ecs_field_t comp, void *data);

/// Ste the component data on an entity.
void ecs_set_data(ecs_entity_t entity, ecs_field_t comp, const void *data);

//////////////////////// SYSTEMS ////////////////////////

/// The maximum number of components a single system can write to.
#define ECS_MAX_SYSTEM_WRITES 10

/// A system. If `.run == NULL` or `.field == 0`, running the system does nothing.
struct ecs_system {
	/** @brief Called to run the system.
	 *
	 * May be called multiple times per `ecs_run_system`.
	 *
	 * Some invalid entities may be in data. The data is stale, but generally it's OK for performance
	 * to iterate over it and pretend like it's live. Dead data has its ID passed as 0 in ids.
	 *
	 * Data is indexed by component.
	 *
	 * @param len The number of entities to run on.
	 * @param ids The identities of the affected entities. Shape is [`len`].
	 * @param data Pointers to the arrays for the component data. Shape is [number of components][`len`]
	 */
	void (*run)(ecs_entity_t len, ecs_entity_t *ids, void **data);
	ecs_field_t field; ///< The field to run on.
	const char *name; ///< The name of the system for debugging.
	ecs_field_t writes[ECS_MAX_SYSTEM_WRITES]; ///< Start left justified array 0 padded of components which are written to.
	bool want_ids; ///< If false, skip an expensive lookup and pass ids as NULL.
};

/// Run a system.
void ecs_run_system(struct ecs_system system);

//////////////////////// PIPELINES ////////////////////////

/// A pipeline as a linked list of stages.
/// Do not change while running.
struct ecs_pipeline {
	struct ecs_system system; ///< The underlying system.
	uint64_t mutex; ///< Two stages `a` and `b` can't run concurrently if `a.mutex & b.mutex` is nonzero.
	struct ecs_pipeline *next;
	bool single_threaded; ///< If true, only call `system.run` in one thread.
	bool fence_before; ///< If true, wait until all previous stages are done.
	bool fence_after; ///< If true, wait until this stage is done for subsequent stages.
};

/// Run a pipeline. Mutexes out all other attempts to run a pipeline until done.
void ecs_run_pipeline(struct ecs_pipeline *start);

#endif
