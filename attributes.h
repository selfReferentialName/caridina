/** @file attributes.h
 * @brief GCC attributes for visibility and stuff
 */
#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H

#define UNUSED __attribute__((unused))
#define HIDDEN __attribute__((visibility("hidden")))
#define PACKED __attribute__((packed))

#endif
