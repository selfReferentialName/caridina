/** @file system/api.h
 * @brief API to talk with the specific ECS stuff.
 */
#ifndef SYSTEM_API_H
#define SYSTEM_API_H

#include "ecs/api.h"

void sys_init(void); ///< Initialise the system stuff. Call after ecs_init.

/// The data stored in ct_frustum_info
struct sys_frustum_info {
	double near; ///< Distance to near clipping plane.
	double far; ///< Distance to far clipping plane.
	double fov; ///< Field of view in radians.
	double aspect; ///< Aspect ratio with negative to match window.
};

extern ecs_field_t ct_pos; ///< Position component ID.
extern ecs_field_t ct_xform; ///< Transformation component ID.
extern ecs_field_t ct_frustum; ///< Frustum component ID.
extern ecs_field_t ct_frustum_info; ///< Frustum settings component ID.
extern ecs_field_t ct_dynamic_frustum; ///< Dynamic frustum flag component ID.

extern ecs_field_t ct_mesh; ///< Mesh handle component ID.

extern struct ecs_pipeline *sys_predraw; ///< Pipeline to run before rendering.
extern struct ecs_pipeline *sys_draw; ///< Pipeline to run drawing.
extern struct ecs_pipeline *sys_update; ///< Pipeline to update stuff after drawing.

extern ecs_entity_t sys_camera; ///< The entity handle for the camera.

#endif
