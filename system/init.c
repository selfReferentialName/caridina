#include "system/api.h"
#include "system/i.h"

struct ecs_pipeline *sys_predraw;
struct ecs_pipeline *sys_draw;
struct ecs_pipeline *sys_update;

void sys_init(void)
{
	sys_init_spatial();
	sys_predraw = &sys_spatial_affine;
	sys_init_mesh();
	sys_init_draw();
}
