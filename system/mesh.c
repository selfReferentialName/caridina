#include "system/api.h"
#include "server/gpu/api.h"
#include "ecs/api.h"

ecs_field_t ct_mesh;

static struct ecs_component_info mesh_info = {
	.name = "mesh",
	.size = sizeof(gpu_mesh_t)
};

void sys_init_mesh(void)
{
	ct_mesh = ecs_create_component(&mesh_info);
}
