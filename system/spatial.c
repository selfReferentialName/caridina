#include "system/api.h"
#include "system/i.h"
#include "ecs/api.h"
#include "attributes.h"
#include "server/gpu/api.h"

#include <cglm/cglm.h>
#include <GLFW/glfw3.h>

ecs_field_t ct_pos;
ecs_field_t ct_xform;
ecs_field_t ct_frustum;
ecs_field_t ct_frustum_info;

// position stored as coordinate vector
static struct ecs_component_info pos_info = {
	.name = "position",
	.size = sizeof(vec3)
};

// transform stored as homogenous transformation matrix
static struct ecs_component_info xform_info = {
	.name = "xform",
	.size = sizeof(mat4)
};

// frustum stored as homogonous matrix transforming the frustum itself to the axis aligned unit cube from (-1,-1,0) to (1,1,1)
static struct ecs_component_info frustum_info = {
	.name = "frustum",
	.size = sizeof(mat4)
};

// frustum information necessary to re-run crd_add_frustum
static struct ecs_component_info frustum_info_info = {
	.name = "frustum settings",
	.size = sizeof(struct sys_frustum_info)
};

static void run_affine_offset(ecs_entity_t len, UNUSED ecs_entity_t *ids, void **data)
{
	vec3 *pos = data[ct_pos];
	mat4 *xform = data[ct_xform];
	for (ecs_entity_t i = 0; i < len; i++) {
		glm_vec3_copy(pos[i], (float *) xform[i][3]);
	}
}

// TODO: we don't need to run this on every frustum necessarily
static void run_set_frustum(ecs_entity_t len, UNUSED ecs_entity_t *ids, void **data)
{
	int width, height;
	glfwGetWindowSize(gpu_window, &width, &height);
	double win_aspect = width / (double) height;
	struct sys_frustum_info *infos = data[ct_frustum_info];
	mat4 *fm = data[ct_frustum];
	for (ecs_entity_t i = 0; i < len; i++) {
		struct sys_frustum_info fi = infos[i];
		double focal = 1 / tan(fi.fov / 2); // focal length
		double aspect = fi.aspect > 0 ? fi.aspect : win_aspect;
		mat4 m = {
			{ focal / aspect, 0, 0, 0 },
			{ 0, -focal, 0, 0 },
			{ 0, 0, fi.near / (fi.near-fi.far), fi.far*fi.near / (fi.far-fi.near) },
			{ 0, 0, 1, 0 }
		};
		glm_mat4_copy(m, fm[i]);
	}
}

static ecs_field_t set_frustum_field_comps[2];
static struct ecs_archetype_info set_frustum_field_info = {
	.comp_count = 2,
	.comps = set_frustum_field_comps
};
static struct ecs_pipeline set_frustum = {
	.system = {
		.run = &run_set_frustum,
		.name = "Recalculate frustum matrix",
		.want_ids = false
	}
};

static ecs_field_t affine_offset_field_comps[2];
static struct ecs_archetype_info affine_offset_field_info = {
	.comp_count = 2,
	.comps = affine_offset_field_comps
};
struct ecs_pipeline sys_spatial_affine = {
	.system = {
		.run = &run_affine_offset,
		.name = "Update affine transform to reflect position",
		.want_ids = false
	},
	.next = &set_frustum
};


void sys_init_spatial(void)
{
	ct_pos = ecs_create_component(&pos_info);
	ct_xform = ecs_create_component(&xform_info);
	ct_frustum = ecs_create_component(&frustum_info);
	ct_frustum_info = ecs_create_component(&frustum_info_info);

	sys_spatial_affine.system.writes[0] = ct_xform;
	affine_offset_field_comps[0] = ct_pos;
	affine_offset_field_comps[1] = ct_xform;
	sys_spatial_affine.system.field = ecs_create_archetype(&affine_offset_field_info);

	set_frustum.system.writes[0] = ct_frustum;
	set_frustum_field_comps[0] = ct_frustum;
	set_frustum_field_comps[1] = ct_frustum_info;
	set_frustum.system.field = ecs_create_archetype(&set_frustum_field_info);
}

