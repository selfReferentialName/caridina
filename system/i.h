/** @file system/i.h
 * @brief System stuff internal declarations.
 */
#ifndef SYSTEM_I_H
#define SYSTEM_I_H

#include "ecs/api.h"

void sys_init_spatial(void); ///< Setup spatial components.
void sys_init_mesh(void); ///< Setup mesh components.

void sys_init_draw(void); ///< Create the drawing pipeline.

extern struct ecs_pipeline sys_spatial_affine; ///< Call before rendering to sync xform stuff.
extern struct ecs_pipeline *sys_spatial_affine_last; ///< Pointer to the last stage to keep xform sane.

#endif
