#include "system/api.h"
#include "ecs/api.h"
#include "server/gpu/api.h"
#include "attributes.h"
#include "util/log.h"

static void run_send(ecs_entity_t num, UNUSED ecs_entity_t *ids, void **data)
{
//	log_f(CRD_LOG_ALWAYS, "Sending %u meshes", (unsigned) num);
	gpu_mesh_t *mesh = data[ct_mesh];
	mat4 *xform = data[ct_xform];
	for (ecs_entity_t i = 0; i < num; i++) {
		gpu_draw(mesh[i], xform[i]);
	}
}

static ecs_field_t send_field_comps[2];
static struct ecs_archetype_info send_field_info = {
	.comp_count = 2,
	.comps = send_field_comps
};
static struct ecs_pipeline send = {
	.system = {
		.run = run_send,
		.name = "send meshes to gpu server"
	}
};

void sys_init_draw(void)
{
	send_field_comps[0] = ct_xform;
	send_field_comps[1] = ct_mesh;
	send.system.field = ecs_create_archetype(&send_field_info);
	sys_draw = &send;
}
