/** @file config/general.h
 * @brief General configuration variables.
 */
#ifndef CONFIG_GENERAL_H
#define CONFIG_GENERAL_H

#include <caridina/_log.h>

#include <stddef.h>

#define CFG_OPTSTRING "v:"

extern enum crd_log_level cfg_log_level; ///< Maximum verbosity of logged messages.

// TODO: if argument parsing takes too long, consider only parsing the arguments once
extern int cfg_argc; ///< Command line argument count.
extern char **cfg_argv; ///< Command line argument array.

#define CFG_RES_MAX_MODS 1024
#define CFG_RES_MAX_GAME 128
#define CFG_RES_MAX_ENGINE 16
#define CFG_RES_MAX_PATH 256

extern char *(cfg_res_mods[CFG_RES_MAX_MODS]); ///< Ordered list of mod pack or folder names.
extern size_t cfg_res_mods_len; ///< Size of cfg_res_mods.
extern char *(cfg_res_game[CFG_RES_MAX_GAME]); ///< Ordered list of game resource pack names.
extern size_t cfg_res_game_len; ///< Size of cfg_res_game.
extern char *(cfg_res_engine[CFG_RES_MAX_ENGINE]); ///< Ordered list of engine resource pack names.
extern size_t cfg_res_engine_len; ///< Size of cfg_res_engine.
extern char *(cfg_res_path[CFG_RES_MAX_PATH]); ///< Ordered list for file search path.
extern size_t cfg_res_path_len; ///< Size of cfg_res_path.

#endif
