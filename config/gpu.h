/** @file config/gpu.h
 * @brief Configuration of how the renderer and window are set up.
 */
#ifndef CONFIG_GPU_H
#define CONFIG_GPU_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

extern bool cfg_gpu_prefer_discrete; ///< Perfer a discrete GPU over all others.
extern char *cfg_gpu_favorite; ///< If there's a physical device with this name, use it.

extern char *cfg_window_name; ///< The name of the window.
extern bool cfg_window_fullscreen; ///< Should the game be in fullscreen mode?
extern uint64_t cfg_window_width; ///< The default requested window width.
extern uint64_t cfg_window_height; ///< The default requested window height.

#endif
