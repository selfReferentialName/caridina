#include "config/file.h"
#include "util/log.h"
#include "config/general.h"
#include "config/gpu.h"
#include "util/fio.h"
#include "server/resource/api.h"
#include "util/cstr.h"
#include "util/mem/immortal.h"

#include <stdlib.h>
#include "threadlocal.h"
#include <string.h>
#include <ctype.h>
#include <unistd.h>

static uint64_t parse_uint(char *start, char *end, bool *err)
{
	uint64_t r = 0;
	for (char *c = start; c <= end; c++) {
		switch (*c) {
			case ' ':
			case ',':
			case '_':
			case '\t':
				break;
			default:
				if ('0' > *c || *c > '9') {
					log_f(CRD_LOG_ERROR, "Invalid character in integer %c", *c);
					*err = true;
					return 0;
				} else {
					r *= 10;
					r += *c - '0';
				}
		}
	}
	return r;
}

static void add_int_entry(struct config_entry *entry, char *start, char *end)
{
	int64_t value;
	bool err = false;
	if (start > end) {
		value = 0;
	} else if (start[0] == '-') {
		value = -(int64_t) parse_uint(start + 1, end, &err);
	} else if (start[0] == '+') {
		value = (int64_t) parse_uint(start + 1, end, &err);
	} else {
		value = (int64_t) parse_uint(start, end, &err);
	}
	if (err) return;

	if (entry->is_list) {
		entry->val.i[*entry->val_len] = value;
		(*entry->val_len)++;
	} else {
		*entry->val.i = value;
	}
}

static void add_uint_entry(struct config_entry *entry, char *start, char *end)
{
	uint64_t value;
	bool err = false;
	if (start > end) {
		value = 0;
	} else if (start[0] == '+') {
		value = parse_uint(start + 1, end, &err);
	} else {
		value = parse_uint(start, end, &err);
	}
	if (err) return;

	if (entry->is_list) {
		entry->val.u[*entry->val_len] = value;
		(*entry->val_len)++;
	} else {
		*entry->val.u = value;
	}
}

static void add_float_entry(struct config_entry *entry, char *start, char *end)
{
	double sign = 1;
	double mantissa = 0;
	double exponent = 1;
	double value;

	char *c = start;
entry:
	if (c > end) goto done;
	if (c == start) {
		switch (*c) {
			case '-':
				sign = -1;
				c++;
				goto entry;
			case '+':
				c++;
				goto entry;
			default:
				goto before_point;
		}
	}
	/* fall through */

before_point:
	if (c > end) goto done;
	switch (*c) {
		case 'e':
		case 'E':
			c++;
			goto start_exponent;
		case ',':
		case ' ':
		case '\t':
		case '_':
			c++;
			goto before_point;
		case '.':
			c++;
			goto after_point;
		default:
			if ('0' > *c || *c > '9') {
				log_f(CRD_LOG_ERROR, "Invalid character %c in float", *c);
				return;
			} else {
				mantissa *= 10;
				mantissa += *c - '0';
				c++;
				goto before_point;
			}
	}

after_point:
	if (c > end) goto done;
	switch (*c) {
		case 'e':
		case 'E':
			c++;
			goto start_exponent;
		case ',':
		case ' ':
		case '\t':
		case '_':
			c++;
			goto after_point;
		default:
			if ('0' > *c || *c > '9') {
				log_f(CRD_LOG_ERROR, "Invalid character %c in float", *c);
				return;
			} else {
				mantissa *= 10;
				mantissa += *c - '0';
				exponent /= 10;
				c++;
				goto after_point;
			}
	}

start_exponent:
	if (c > end) goto done;
	switch (*c) {
		case '+':
			c++;
			goto in_exponent;
		case '-':
			exponent = -exponent;
			c++;
			goto in_exponent;
		case ' ':
		case ',':
		case '\t':
		case '_':
			c++;
			goto start_exponent;
		default:
			goto in_exponent;
	}

in_exponent:
	if (c > end) goto done;
	switch (*c) {
		case ' ':
		case ',':
		case '\t':
		case '_':
			c++;
			goto in_exponent;
//		default:
			/* fall through */
	}
	if ('0' > *c || *c > '9') {
		log_f(CRD_LOG_ERROR, "Invalid character %c in float", *c);
		return;
	} else {
		exponent *= 10;
		if (exponent > 0) {
			exponent -= *c - '0';
		} else {
			exponent += *c - '0';
		}
		c++;
		goto in_exponent;
	}

done:
	value = sign * mantissa * exponent;

	if (entry->is_list) {
		entry->val.f[*entry->val_len] = value;
		(*entry->val_len)++;
	} else {
		*entry->val.f = value;
	}
}

static void add_string_entry(struct config_entry *entry, char *start, char *end, malloc_t *alloc)
{
	char *value = "";
	if (start <= end) {
		size_t size = (size_t) end - (size_t) start + 1;
		value = mem_malloc(alloc, size + 1);
		memcpy(value, start, size);
		value[size] = 0;
	}

	if (entry->is_list) {
		entry->val.s[*entry->val_len] = value;
		(*entry->val_len)++;
	} else {
		*entry->val.s = value;
	}
}

static void add_bool_entry(struct config_entry *entry, char *start, char *end)
{
	bool value;
	if (end < start) {
		value = true;
	} else {
		switch (*start) {
			case 't':
			case 'y':
			case 'T':
			case 'Y':
			case '1':
				value = true;
				break;
			case 'f':
			case 'n':
			case 'F':
			case 'N':
			case '0':
				value = false;
				break;
			case 'd':
			case 'D':
				return;
			default:
				{
					char *s = alloca((uintptr_t) end - (uintptr_t) start + 2);
					memcpy(s, start, (uintptr_t) end - (uintptr_t) start + 1);
					s[(uintptr_t) end - (uintptr_t) start + 1] = 0;
					log_f(CRD_LOG_ERROR, "Unknown string %s for bool.", s);
					return;
				}
		}
	}

	if (entry->is_list) {
		entry->val.b[*entry->val_len] = value;
		(*entry->val_len)++;
	} else {
		*entry->val.b = value;
	}
}

// start and end are inclusive, end < start means empty
static void add_entry(struct config_section section, char *name, char *start, char *end, malloc_t *alloc)
{
	for (int i = 0; i < section.entryc; i++) {
		if (strcmp(section.entryv[i].name, name) == 0) {
			if (section.entryv[i].is_list && *section.entryv[i].val_len >= section.entryv[i].max_len) {
				log_f(CRD_LOG_ERROR, "Too many entries in list: [%s] %s", section.name, name);
				return;
			}
			switch (section.entryv[i].type) {
				case CONFIG_INT:
					add_int_entry(&section.entryv[i], start, end);
					return;
				case CONFIG_UINT:
					add_uint_entry(&section.entryv[i], start, end);
					return;
				case CONFIG_FLOAT:
					add_float_entry(&section.entryv[i], start, end);
					return;
				case CONFIG_STRING:
					add_string_entry(&section.entryv[i], start, end, alloc);
					return;
				case CONFIG_BOOL:
					add_bool_entry(&section.entryv[i], start, end);
					return;
			}
		}
	}
	log_f(CRD_LOG_ERROR, "Entry not in section: [%s] %s", section.name, name);
}

static void add_quoted_entry(struct config_section section, ...)
{
	log_assert(!section.name);
}

static void parse(size_t size, char *data, struct config_format format, malloc_t *alloc)
{
	struct config_section section = {};
	for (int i = 0; i < format.sectionc; i++) {
		if (strcmp(format.sectionv[i].name, "") == 0) {
			section = format.sectionv[i];
		}
	}

	char section_name[SECTION_NAME_MAX_LEN];
	section_name[0] = 0;
	int section_name_len;
	char entry_name[ENTRY_NAME_MAX_LEN];
	entry_name[0] = 0;
	int entry_name_len;
	int entry_name_end;
	char *entry_start;
	char *entry_end;

entry:
	if (size <= 0) return;
	switch (*data) {
		case ' ':
		case '\n':
		case '\t':
		case '\r':
			data++; size--;
			goto entry;
		case '[':
			data++; size--;
			section_name_len = 0;
			goto section_name;
		case '#':
			data++; size--;
			goto trash_line;
		default:
			entry_name_len = 0;
			entry_name_end = 0;
			goto entry_name;
	}

expect_whitespace:
	if (size <= 0) return;
	switch (*data) {
		case '\n':
		case '\r':
			data++; size--;
			goto entry;
		case '\t':
		case ' ':
			data++; size--;
			goto expect_whitespace;
		default:
			log_f(CRD_LOG_ERROR, "Expected end of line, got %c", *data);
			goto trash_line;
	}

trash_line:
	if (size <= 0) return;
	switch (*data) {
		case '\n':
		case '\r':
			data++; size--;
			goto entry;
		default:
			data++; size--;
			goto trash_line;
	}

section_name:
	if (size <= 0) {
		section_name[section_name_len] = 0;
		log_f(CRD_LOG_ERROR, "Reached EOF before ending section name: %s", section_name);
		return;
	}
	switch (*data) {
		case ']':
			section_name[section_name_len] = 0;
			for (int i = 0; i < format.sectionc; i++) {
				if (strcmp(format.sectionv[i].name, section_name) == 0) {
					section = format.sectionv[i];
				}
			}
			data++; size--;
			goto expect_whitespace;
		case '\n':
		case '\r':
			section_name[section_name_len] = 0;
			log_f(CRD_LOG_ERROR, "Reached newline before ending section name: %s", section_name);
			data++; size--;
			goto entry;
		default:
			section_name[section_name_len] = tolower(*data);
			section_name_len++;
			data++; size--;
			if (section_name_len >= SECTION_NAME_MAX_LEN) {
				section_name[section_name_len - 1] = 0;
				log_f(CRD_LOG_ERROR, "Section name exceeds max length: %s", section_name);
				goto trash_line;
			}
			goto section_name;
	}

entry_name:
	if (size <= 0) {
		entry_name[entry_name_end] = 0;
		log_f(CRD_LOG_ERROR, "Entry has no value: [%s] %s", section_name, entry_name);
		return;
	}
	switch (*data) {
		case '\n':
		case '\r':
			entry_name[entry_name_end] = 0;
			log_f(CRD_LOG_ERROR, "Entry has no value: [%s] %s", section_name, entry_name);
			data++; size--;
			goto entry;
		case '=':
		case ':':
			entry_name[entry_name_end] = 0;
			data++; size--;
			goto before_value;
		default:
			entry_name_end++;
			/* fall through */
		case ' ':
		case '\t':
			entry_name[entry_name_len] = tolower(*data);
			entry_name_len++;
			data++; size--;
			if (entry_name_len >= ENTRY_NAME_MAX_LEN) {
				entry_name[entry_name_len - 1] = 0;
				log_f(CRD_LOG_ERROR, "Entry name exceeds max length: %s", entry_name);
				goto trash_line;
			}
			goto entry_name;
	}

before_value:
	if (size <= 0) {
		add_entry(section, entry_name, data, data - 1, alloc);
		return;
	}
	switch (*data) {
		case '\n':
		case '\r':
			add_entry(section, entry_name, data, data - 1, alloc);
			data++; size--;
			goto entry;
		case ' ':
		case '\t':
			data++; size--;
			goto before_value;
		case '"':
			entry_start = data + 1;
			data++; size--;
			goto quoted_value;
		default:
			entry_start = data;
			entry_end = data;
			data++; size--;
			goto basic_value;
	}

basic_value:
	if (size <= 0) {
		add_entry(section, entry_name, entry_start, entry_end, alloc);
		return;
	}
	switch (*data) {
		case '\n':
		case '\r':
			add_entry(section, entry_name, entry_start, entry_end, alloc);
			data++; size--;
			goto entry;
		default:
			entry_end = data;
		case ' ':
		case '\t':
			data++; size--;
			goto basic_value;
	}

quoted_value:
	if (size <= 0) {
		log_f(CRD_LOG_ERROR, "Quoted entry not terminated: [%s] %s", section_name, entry_name);
		return;
	}
	switch (*data) {
		case '\n':
		case '\r':
			log_f(CRD_LOG_ERROR, "Quoted entry not terminated: [%s] %s", section_name, entry_name);
			data++; size--;
			goto entry;
		case '"':
			add_quoted_entry(section, entry_start, data - 1);
			data++; size--;
			goto expect_whitespace;
		case '\\':
			data++; size--;
			if (size <= 0) {
				log_f(CRD_LOG_ERROR, "Quoted entry not terminated: [%s] %s", section_name, entry_name);
				return;
			}
			switch (*data) {
				case '"':
				case '\\':
					data++; size--;
					goto quoted_value;
				default:
					log_f(CRD_LOG_ERROR, "Invalid escape code \\%c in quoted entry: [%s] %s", section_name, entry_name);
					goto trash_line;
			}
		default:
			data++; size--;
			goto quoted_value;
	}
}

void config_read_res(const char *path, struct config_format format, malloc_t *alloc)
{
	struct res_promise *res = res_open(path, 0);
	res_await(res);
	parse(res->size, res->data, format, alloc);
	res_close(res);
}

void config_read_file(const char *path, struct config_format format, malloc_t *alloc)
{
	struct fio_file_data data = fio_read_file(path);
	parse(data.size, data.data, format, alloc);
	fio_free_file(data);
}

#define RES_ENTRIES 4
static struct config_entry res_entries[RES_ENTRIES] = {
	{
		.name = "mod",
		.val = {
			.s = cfg_res_mods
		},
		.val_len = &cfg_res_mods_len,
		.max_len = CFG_RES_MAX_MODS,
		.type = CONFIG_STRING,
		.is_list = true
	},
	{
		.name = "game",
		.val = {
			.s = cfg_res_game
		},
		.val_len = &cfg_res_game_len,
		.max_len = CFG_RES_MAX_GAME,
		.type = CONFIG_STRING,
		.is_list = true
	},
	{
		.name = "engine",
		.val = {
			.s = cfg_res_engine
		},
		.val_len = &cfg_res_engine_len,
		.max_len = CFG_RES_MAX_ENGINE,
		.type = CONFIG_STRING,
		.is_list = true
	},
	{
		.name = "path",
		.val = {
			.s = cfg_res_path
		},
		.val_len = &cfg_res_path_len,
		.max_len = CFG_RES_MAX_PATH,
		.type = CONFIG_STRING,
		.is_list = true
	}
};

#define LOG_ENTRIES 1
static char *cfg_log_level_name = "";
static struct config_entry log_entries[LOG_ENTRIES] = {
	{
		.name = "verbosity",
		.val = {
			.s = &cfg_log_level_name
		},
		.type = CONFIG_STRING,
		.is_list = false
	}
};
static void process_log_cfg(void)
{
	if (strlen(cfg_log_level_name) < 1) {
		return;
	}
	cstr_tolower_inplace(cfg_log_level_name);
	if (memcmp(cfg_log_level_name, "very", strlen("very")) == 0) {
		cfg_log_level = CRD_LOG_VERY_VERBOSE;
	} else if (memcmp(cfg_log_level_name, "event", strlen("event")) == 0) {
		cfg_log_level = CRD_LOG_EVENTS;
	} else {
		switch (*cfg_log_level_name) {
			case '!':
			case 'a':
				cfg_log_level = CRD_LOG_ALWAYS;
				return;
			case 'e':
				cfg_log_level = CRD_LOG_ERROR;
				return;
			case 'w':
				cfg_log_level = CRD_LOG_WARN;
				return;
			case '+':
			case 'd':
				cfg_log_level = CRD_LOG_DEFAULT;
				return;
			case '-':
				cfg_log_level = CRD_LOG_EVENTS;
				return;
			case 'v':
				cfg_log_level = CRD_LOG_VERBOSE;
				return;
			default:
				log_f(CRD_LOG_ERROR, "Invalid log verbosity: %s", cfg_log_level_name);
		}
	}
}

#define GPU_ENTRIES 2
static struct config_entry gpu_entries[GPU_ENTRIES] = {
	{
		.name = "discrete",
		.val = {
			.b = &cfg_gpu_prefer_discrete
		},
		.type = CONFIG_BOOL,
		.is_list = false
	},
	{
		.name = "favorite",
		.val = {
			.s = &cfg_gpu_favorite
		},
		.type = CONFIG_STRING,
		.is_list = false
	}
};

#define WINDOW_ENTRIES 4
struct config_entry window_entries[WINDOW_ENTRIES] = {
	{
		.name = "name",
		.val = {
			.s = &cfg_window_name
		},
		.type = CONFIG_STRING,
		.is_list = false
	},
	{
		.name = "fullscreen",
		.val = {
			.b = &cfg_window_fullscreen
		},
		.type = CONFIG_BOOL,
		.is_list = false
	},
	{
		.name = "width",
		.val = {
			.u = &cfg_window_width
		},
		.type = CONFIG_UINT,
		.is_list = false
	},
	{
		.name = "height",
		.val = {
			.u = &cfg_window_height
		}
	}
};

#define SECTION_COUNT 4
static struct config_section sections[SECTION_COUNT] = {
	{
		.name = "resource packs",
		.entryv = res_entries,
		.entryc = RES_ENTRIES
	},
	{
		.name = "log",
		.entryv = log_entries,
		.entryc = LOG_ENTRIES
	},
	{
		.name = "gpu",
		.entryv = gpu_entries,
		.entryc = GPU_ENTRIES
	},
	{
		.name = "window",
		.entryv = window_entries,
		.entryc = WINDOW_ENTRIES
	}
};

static struct config_format engine_config_format = {
	.sectionv = sections,
	.sectionc = SECTION_COUNT
};

char *cfg_engine_config = "caridina.ini";

void config_init(int argc, char **argv)
{
	cfg_argc = argc;
	cfg_argv = argv;

	optind = 1;
	setenv("POSIXLY_CORRECT", "please", 0);
	for (int opt = getopt(argc, argv, CFG_OPTSTRING); opt != -1; opt = getopt(argc, argv, CFG_OPTSTRING)) {
		if (opt == 'c') {
			cfg_engine_config = mem_imrtl_malloc(strlen(optarg) + 1);
			strcpy(cfg_engine_config, optarg);
		}
	}

	config_read_file(cfg_engine_config, engine_config_format, &mem_imrtl_allocator);

	process_log_cfg();
}
