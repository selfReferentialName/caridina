#include "config/camera.h"

#include <stdatomic.h>

#pragma GCC diagnostic ignored "-Wdiscarded-array-qualifiers"

volatile mat4 cam_vp;
atomic_uchar cam_gen;

void cam_mulv(vec4 in, vec4 out)
{
	vec4 tmp;
	unsigned char gen = cam_gen;
	while (gen != cam_gen) {
		gen = cam_gen;
		glm_mat4_mulv(cam_vp, in, tmp);
	}
	glm_vec4_copy(tmp, out);
}

void cam_set(mat4 vp)
{
	unsigned char gen = cam_gen;
	while (gen != cam_gen - 1) {
		gen = cam_gen;
		glm_mat4_copy(vp, cam_vp);
		cam_gen++;
	}
}
