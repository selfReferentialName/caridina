/** @file config/file.h
 * @brief A simple INI file reader and writer. Also read and write the engine config.
 *
 * BNF for the file format (excluding how errors are parsed)
 *
 * <line> ::= <section-name>
 *          | <entry>
 *          | <whitespace> <line>
 *          | <line> <whitespace>
 *          | <whitespace>
 *
 * <section-name> ::= "[" <any-char-but-square-brackets-and-newlines>* "]"
 *
 * <entry> ::= <entry-name> <whitespace> "=" <whitespace> <entry-value>
 *           | <entry-name> <whitespace> ":" <whitespace> <entry-value>
 *
 * <entry-name> ::= <any-char-but-square-brackets-and-newlines>*
 *
 * Whitespace on either side is removed
 * <entry-value> ::= "\"" <explicit-string-char>* "\""
 *                 | <any-char-but-double-quote-and-newlines> <any-char-but-newlines>*
 *                 | <nothing>
 *
 * <explicit-string-char> ::= <any-char-but-backslash-and-double-quote>
 *                          | "\\\""
 *                          | "\\\\"
 */
#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include "util/mem/fnptrs.h"

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

// parsing limitations
#define SECTION_NAME_MAX_LEN 256
#define ENTRY_NAME_MAX_LEN 256

/// A section in the config file.
struct config_section {
	char *name; ///< The name of the section. "" for the default section []
	struct config_entry *entryv; ///< The entries in the section.
	int entryc; ///< The number of entries in the section.
};

/// The type of an entry in a section.
enum config_entry_type {
	CONFIG_INT, ///< A signed integer (int64_t)
	CONFIG_UINT, ///< An unsigned integer (uint64_t)
	CONFIG_FLOAT, ///< A floating point number (double)
	CONFIG_STRING, ///< An arbitrary string (char *)
	CONFIG_BOOL, ///< A true or false flag (bool)
};

/// TODO: A size measured in bytes possibly with a prefix (uint64_t)
#define CONFIG_BYTE_SIZE CONFIG_UINT

/// An entry in the config file. A configurable variable.
struct config_entry {
	char *name; ///< The key of the entry.
	union {
		int64_t *i; ///< Where to read/write a value for int64_t
		uint64_t *u; ///< Where to read/write a value for uint64_t
		double *f; ///< Where to read/write a value for double
		char **s; ///< Where to read/write a value for char *
		bool *b; ///< Where to read/write a value for bool
	} val;
	size_t *val_len; ///< Where to put the length for lists.
	size_t max_len; ///< Backng of val_x for lists.
	enum config_entry_type type; ///< The type of this entry.
	bool is_list; ///< If true, put diplicate keys at the back of a list.
};

/// An INI config file format.
struct config_format {
	struct config_section *sectionv; ///< The array of sections.
	int sectionc; ///< The number of sections.
};

/** @brief Read an INI file which is a resource.
 *
 * Read an INI file with the given format from the given resource path.
 * On an error, it does nothing with the offending line and logs an error
 * before continuing to try to read the file.
 *
 * @param res The resource path.
 * @param format The config file format.
 * @param alloc The allocator to put strings in.
 *
 * @see config_read_file
 */
void config_read_res(const char *res, struct config_format format, malloc_t *alloc);

/** @brief Read an INI file which is a file.
 *
 * Read an INI file with the given format from the given filename.
 * On an error, it does nothing with the offending line and logs an error
 * before continuing to try to read the file.
 *
 * @param filename The filename.
 * @param format The config file format.
 * @param alloc The allocator to put strings in.
 *
 * @see config_read_res
 */
void config_read_file(const char *filename, struct config_format format, malloc_t *alloc);

/** @brief TODO: Write an INI file to a file.
 *
 * Write out an INI file with the given format.
 * Writes out all sections and entries regardless of need.
 * TODO: consider only writing what's changed.
 *
 * Intended to be run inside log_assert in most cases.
 *
 * @param filename The name of the file to write to.
 * @param format The config file format.
 *
 * @return true on success, false on error.
 *
 * @see log_assert
 */
bool config_write(const char *filename, struct config_format format);

/** @brief TODO: Reorganise an INI file to look nice.
 *
 * Reorders the lines of an INI file so sections and entries are in
 * alphabetical order and spaces stuff out nicely with blank lines.
 * Preserves comments assuming they affect the next non-blank line
 * if it's an entry or the previous non-blank line if it's a section,
 * breaking ties by whichever is closest.
 *
 * Logs an error and gives up doing nothing if it encounters a problem.
 *
 * @param filename The name of the file to format.
 */
void config_reformat(const char *filename);

extern char *cfg_engine_config; ///< Name of the engine config file.

/** @brief Read the engine configuration.
 *
 * Reads the engine configuration and initialisises various configuration
 * stuff. It is the first function called from crd_init because
 * it saves the argument list for use by later functions.
 *
 * @see config_read_file config_read_res config_save crd_init
 */
void config_init(int argc, char **argv);

/** Save the engine configuration.
 *
 * @see config_write config_init
 */
void config_save(void);

#endif
