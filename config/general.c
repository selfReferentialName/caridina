#include "config/general.h"

#ifndef NDEBUG
enum crd_log_level cfg_log_level = CRD_LOG_EVENTS;
#else
enum crd_log_level cfg_log_level = CRD_LOG_DEFAULT;
#endif

int cfg_argc;
char **cfg_argv;

char *(cfg_res_mods[CFG_RES_MAX_MODS]);
size_t cfg_res_mods_len;
char *(cfg_res_game[CFG_RES_MAX_GAME]);
size_t cfg_res_game_len;
char *(cfg_res_engine[CFG_RES_MAX_ENGINE]);
size_t cfg_res_engine_len;
char *(cfg_res_path[CFG_RES_MAX_PATH]);
size_t cfg_res_path_len;
