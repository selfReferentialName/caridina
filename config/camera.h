/** @file config/camera.h
 * @brief Camera state.
 */
#ifndef CONFIG_CAMERA_H
#define CONFIG_CAMERA_H

#include <cglm/cglm.h>

#include <stdatomic.h>

void cam_mulv(vec4 in, vec4 out); ///< Apply camera transform to a vector

void cam_set(mat4 in); ///< Set camera transform.

extern volatile mat4 cam_vp; ///< Matrix view*projection matrix.
extern atomic_uchar cam_gen; ///< Incremented before and after cam_vp is changed.

#endif
