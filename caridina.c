#include "util/mem/immortal.h"
#include "config/file.h"
#include "config/vulkan.h"
#include "config/camera.h"
#include "server/resource/api.h"
#include "server/draw/api.h"
#include "util/sync.h"

#include <caridina/mesh.h>

mat4 id4;

crd_vert_index_t idx[] = {2, 1, 0};

vec3 pos[] = {
	{-0.7, -0.7, 0.5},
	{ 0.7, -0.7, 0.5},
	{ 0.0,  0.7, 0.5}
};

vec3 col[] = {
	{1, 0, 0},
	{0, 1, 0},
	{0, 0, 1}
};

void thanks(void)
{
	puts("Thank you for playing Wing Commander!!!!!!");
}

int main(int argc, char **argv)
{
	atexit(thanks);
	mem_imrtl_init();
	config_init(argc, argv);
	res_init();
	vlk_init();
	glm_mat4_identity(id4);
	cam_set(id4);
	crd_mesh_t tri = crd_mesh_static_col(3, idx, pos, pos, col);
	while (true) {
		log_checkpoint(draw_submit(tri, 1, &id4), CRD_LOG_ALWAYS);
		log_checkpoint(draw_done(), CRD_LOG_ALWAYS);
		log_checkpoint(sync_frame_end(), CRD_LOG_ALWAYS);
		logs("Next frame", CRD_LOG_ALWAYS);
	}
}
