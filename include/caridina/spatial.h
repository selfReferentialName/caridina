/** @file caridina/spatial.h
 * @brief Functions to deal with entities in 3D space.
 */
#ifndef CARIDINA_SPATIAL_H
#define CARIDINA_SPATIAL_H

#include <caridina/base.h>

#pragma GCC visibility push(default)

/** @brief Add a position component.
 *
 * A position component stores where an entity is in 3D space.
 * A lot of systems rely on the position component being there.
 * If the entity already has a position component, update the position.
 *
 * @param entity The entity to add (or change) a position to.
 * @param pos0 The initial (or updated) position.
 */
void crd_add_pos(crd_entity_t entity, vec3 pos0);

/** @brief Get the value of a position component.
 *
 * @param entity The entity to get the position of.
 * @param pos Where to write the position.
 */
void crd_get_pos(crd_entity_t entity, vec3 pos);

/** @brief Add a transform component.
 *
 * A transform component stores a 3D to 3D linear transformation.
 * Basically, that means it stores a scale, rotation, skew, and
 * maybe flip all in one (because matrices are the simplest way
 * to do all of these). Stored as a 3 by 3 matrix.
 * If the entity already has a transform component, update the transform.
 *
 * @param entity The entity to add (or change) a transform to.
 * @param xform0 The inital (or updated) transform.
 */
void crd_add_xform(crd_entity_t entity, mat3 xform0);

/** @brief Get the value of a transform component.
 *
 * @param entity The entity to get the transform of.
 * @param xform Where to write the transform.
 */
void crd_get_xform(crd_entity_t entity, mat3 xform);

/** @brief Add a frustum component.
 *
 * A frustum is a truncated pyramid like shape used for projective
 * transformations in stuff like cameras. The frustum is dealt with
 * after the transform has already been applied, and always points
 * at positive z.
 *
 * @param entity The entity to add (or change) a frustum to.
 * @param near The distance to the small base (near clipping plane). Make as big as possible.
 * @param far The distance to the big base (far clipping plane). Make as small as possible.
 * @param fov The angle between the axes on the Y axis in radians.
 * @param aspect The aspect ratio (width/height). Set negative to use the screen aspect ratio.
 */
void crd_add_frustum(crd_entity_t entity, double near, double far, double fov, double aspect);

#pragma GCC visibility pop

#endif
