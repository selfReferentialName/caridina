/** @file caridina/base.h
 * @brief Base definitions included with every Caridina header file.
 */
#ifndef CARIDINA_BASE_H
#define CARIDINA_BASE_H

#include <stdint.h>
#include <cglm/types.h>
#include <stddef.h>
#include <stdatomic.h>

typedef int32_t crd_handle_t; ///< A generic handle used for most things Caridina deals with.

typedef crd_handle_t crd_entity_t; ///< An entity. Something which can have components added to it.

#endif
