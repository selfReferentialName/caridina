/** @file caridina/caridina.h
 * @brief Includes all Caridina headers for convienience.
 */
#ifndef CARIDINA_H
#define CARIDINA_H

#include <caridina/base.h>
#include <caridina/mesh.h>
#include <caridina/material.h>
#include <caridina/spatial.h>
#include <caridina/control.h>
#include <caridina/ecs.h>
#include <caridina/input.h>

#endif
