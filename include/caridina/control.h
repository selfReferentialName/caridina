/** @file caridina/control.h
 * @brief Basic engine control functions, mostly stuff to put in the main function.
 */
#ifndef CARIDINA_CONTROL_H
#define CARIDINA_CONTROL_H

#include <caridina/base.h>

#pragma GCC visibility push(default)

/** @brief Initialise Caridina.
 *
 * Initialise the engine. Call before any other functions.
 *
 * @param argc The argc passed to the main function. Pass 0 to not parse arguments.
 * @param argv The argv passed to the main function. Used iff argc != 0
 */
void crd_init(int argc, char **argv);

/** @brief Set the scene camera.
 *
 * Set the camera. The camera must have a position, transform, and
 * frustum component. Must be called before running crd_game_loop.
 *
 * @param cam The camera entity.
 *
 * @see crd_add_pos crd_add_xform crd_add_frustum
 */
void crd_set_camera(crd_entity_t cam);

/** @brief Run the game.
 *
 * Run the game loop. Run systems and render the world in a loop.
 * Intended to be used in the main function like
 *
 *     return crd_game_loop();
 *
 * Must be called after crd_init and crd_set_camera.
 *
 * @return The exit code of the game.
 *
 * @see crd_init crd_set_camera
 */
int crd_game_loop(void);

extern atomic_bool crd_exit; ///< Set true to quit.

#pragma GCC visibility pop

#endif
