/** @file caridina/_log.h
 * @brief User visble types for logging.
 */
#ifndef CARIDINA_LOG_H
#define CARIDINA_LOG_H

/// The verbosity of log messages.
enum crd_log_level {
	CRD_LOG_ALWAYS, ///< Messages which can't be turned off (e.g. crash info)
	CRD_LOG_ERROR, ///< Recoverable error mesages (e.g. texture not found)
	CRD_LOG_WARN, ///< Warnings
	CRD_LOG_DEFAULT, ///< Very major events which should usually be logged
	CRD_LOG_EVENTS, ///< Major events which don't always need to be logged
	CRD_LOG_VERBOSE, ///< Minor events which can be helpful when debugging
	CRD_LOG_VERY_VERBOSE ///< Extremely verbose logging. Disabled in release builds.
};

#endif
