/** @file caridina/mesh.h
 * @brief Mesh handling functions.
 */
#ifndef CARIDINA_MESH_H
#define CARIDINA_MESH_H

#include <caridina/base.h>

#pragma GCC visibility push(default)

typedef uint16_t crd_mesh_t; ///< A mesh which is ready for use.

typedef uint32_t crd_vert_index_t; ///< A vertex index in a mesh. Public type; not used internally.

/** @brief Create an unanimated mesh from indexed vertex positions, normals, and UV locations.
 *
 * The mesh is arranged into triangles and uses vertex indices because most vertices will
 * be used in multiple triangles. No explicit number of vertex indices is passed in; instead
 * the number of vertex indices is the maximum value in the indices array.
 *
 * You may need to wait a frame before the mesh is actually usable.
 *
 * @param vert_count The number of vertices; three times the number of triangles.
 * @param indices An array of vertex indices arranged into triplets representing triangles.
 * @param pos The positions of each vertex (indexed).
 * @param normal The normal vectors of each vertex (indexed).
 * @param uv The UV positions of each vertex (indexed).
 *
 * @return A handle to the mesh.
 *
 * @see crd_mesh_resource
 */
crd_mesh_t crd_mesh_static(size_t vert_count, crd_vert_index_t *indices,
		vec3 *pos, vec3 *normal, vec2 *uv);

/** @brief Load a converted mesh from a file.
 *
 * The mesh is first converted from an editor export format to Caridina's `.mesh`
 * format which can be loaded at runtime with the `meshify` tool. Then the mesh is
 * put in a resource pack
 *
 * You may need to wait a frame before the mesh is actually usable.
 *
 * @param path The resource path.
 *
 * @return A handle to the mesh.
 *
 * @see crd_mesh_static
 */
crd_mesh_t crd_mesh_resource(const char *path);

/** @brief Unload a mesh from the GPU.
 *
 * The GPU has a limited amount of memory. This limits the number of vertices (and triangles,
 * but vertices themselves are more costly) that can be loaded at any given time. At a budget
 * of 1GiB for meshes, you can handle about 64 million vertices. A budget of 256MiB means 8
 * million vertices across all meshes (not instances, which take up about 64 bytes per entity
 * with a linked mesh).
 *
 * This is enough to keep in GPU memory while looking at the same scene, but it may be less
 * than your total number of meshes across many scenes. As such, when you unload a scene,
 * you should unload the meshes it uses to be nice to the GPU.
 *
 * Unloading a mesh is fast, but in order to use it again, you need to make it again, which
 * takes time.
 *
 * After the call, the passed in mesh is invalid. Don't call while an entity is still linked
 * to the mesh.
 *
 * @param mesh The mesh to unload.
 */
void crd_mesh_unload(crd_mesh_t mesh);

/** @brief Attach a mesh to an entity.
 *
 * Attach a mesh to an entity. The entity must already have a position component and must
 * not have another mesh already linked to it. The mesh will move around with the entity
 * as its position and transform change.
 *
 * @param entity The entity to put the mesh on.
 * @param mesh The mesh to put on the entity.
 *
 * @see crd_add_pos crd_add_xform
 */
void crd_add_mesh(crd_entity_t entity, crd_mesh_t mesh);

#pragma GCC visibility pop

#endif
