/** @file caridina/material.h
 * @brief Caridina API for changing a visualised entity's material
 */
#ifndef CARIDINA_MATERIAL_H
#define CARIDINA_MATERIAL_H

#include <caridina/base.h>

#pragma GCC visibility push(default)

/// Whether a parameter to a material is a color or texture.
enum crd_material_format {
	CRD_MAT_DEFAULT = 0, ///< Leave at a defualt value.
	CRD_MAT_COLOR, ///< A single scalar color.
//	CRD_MAT_TEXTURE ///< An entire image. TODO
};

/// A parameter to a material. Not all parameters use all parts.
struct crd_material_param {
	union {
		vec4 c; ///< (For `CRD_MAT_COLOR`) the color of the parameter.
	};
	enum crd_material_format f; ///< How is the parameter represented.
};

/// A PBR based material.
struct crd_material {
	struct crd_material_param albedo; ///< (rgb) Diffuse color for non-metals and specular color for metals.
	struct crd_material_param roughness; ///< (r) Roughness. 0 is perfectly shiny while 1 is perfectly rough.
	struct crd_material_param metalness; ///< (r) Metalness. 0 for non-metals (dielectrics) and 1 for metals (conductors). Generally keep to 0 and 1.
	struct crd_material_param f0; ///< (r) Initial intensity of fresnel. Scaled by albedo for metals.
	struct crd_material_param reserved[8]; ///< Room to add more parameters.
	uint16_t index; ///< What material index on the mesh to change. Set to 0 to broadcast.
};

/** @brief Add a material to an entity.
 *
 * In order for this to do anything, the entity must also have a mesh
 * or something on it.
 *
 * The material is processed and cached by Caridina. Don't try to process
 * the materials before hand to make this easier.
 *
 * @param entity The entity to add the material to.
 * @param material The material to add to it.
 */
void crd_add_material(crd_entity_t entity, struct crd_material *material);

#pragma GCC visibility pop

#endif
