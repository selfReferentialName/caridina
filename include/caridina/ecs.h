/** @file caridina/ecs.h
 * @brief Functions to work with entities, components, and systems.
 */
#ifndef CARIDINA_ECS_H
#define CARIDINA_ECS_H

#include <caridina/base.h>

#pragma GCC visibility push(default)

/** @brief Allocate a new entity.
 *
 * Get a new, blank, and unique entity.
 *
 * @return A new entity.
 */
crd_entity_t crd_new_entity(const char *name);

#pragma GCC visibility pop

#endif
