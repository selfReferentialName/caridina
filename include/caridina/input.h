/** @file caridina/input.h
 * @brief Keyboard and controler inputs.
 *
 * Most stuff here are used in terms of actions (corresponding to keys and buttons)
 * and axes (corresponding to mouse axes, joystick axes, and analog triggers).
 */
#ifndef CARIDINA_INPUT_H
#define CARIDINA_INPUT_H

#include <caridina/base.h>

#pragma GCC visibility push(default)

/// A handle for an action.
typedef uint16_t crd_action_t;

/** @brief Register an action.
 *
 * The key is treated as a GLFW key code. It can be set
 * in the config file under the `[INPUT]` section with
 * they key as the name of the action.
 *
 * This should be called before `crd_init` so that the
 * action can be read from the config file.
 *
 * Don't call twice with the same name.
 *
 * @param name The name of the action for debugging and configuration.
 * @param deflt The default key to watch for.
 *
 * @return The handle for the action.
 */
crd_action_t crd_register_action(const char *name, int deflt);

/// What happened with the action this frame?
enum crd_action_state {
	CRD_ACTION_OFF, ///< The key isn't pressed this frame and it wasn't last frame.
	CRD_ACTION_PRESSED, ///< The key was just pressed this frame (but wasn't last frame).
	CRD_ACTION_HELD, ///< The key was pressed last frame and continues to be pressed this one.
	CRD_ACTION_RELEASED ///< The key was pressed last frame but is no longer pressed this frame.
};

/// Array of the states each action is in by action.
extern const enum crd_action_state *crd_action_states;

/** @brief Run a bit of code whenever an action is released.
 *
 * You can register any number of functions to call when this action
 * is released, but currently you can't *unregister* any, kinda like
 * `atexit`.
 *
 * @param action The action to check for releases of.
 * @param fun The function to call.
 */
void crd_on_action_released(crd_action_t action, void (*fun)(void));

#pragma GCC visibility pop

#endif
