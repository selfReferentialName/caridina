#include "util/xmalloc.h"
#include "util/log.h"

void *xmalloc(size_t size)
{
	void *malloc_result = malloc(size); // long name for error messages
	log_assert(malloc_result != NULL);
	return malloc_result;
}
