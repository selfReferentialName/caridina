/** @file util/fio.h
 * @brief Basic synchronous file I/O routines.
 * TODO: respect path
 */
#ifndef UTIL_FIO_H
#define UTIL_FIO_H

#include <stddef.h>

/// The text read from a file.
struct fio_file_data {
	char *data; ///< The text itself.
	size_t size; ///< The size of the file.
};

/** @brief Read all text in a file.
 *
 * Get the size of a file and read it all at once.
 * TODO: search the path
 * Always crashes on error.
 *
 * @param path The filename to read.
 *
 * @return The text in the file.
 */
struct fio_file_data fio_read_file(const char *path);

/** @brief Free the text of a file.
 *
 * Call after done reading a file to avoid leaking resources.
 *
 * @param data The file data to free up.
 */
void fio_free_file(struct fio_file_data data);

#endif
