#include "util/sync.h"
#include "util/log.h"

#include <stdatomic.h>
#include <pthread.h>

struct barrier {
	int interested;
	int waiting;
	int finishing;
	pthread_cond_t signal;
	pthread_mutex_t mutex;
};

static struct barrier frame_end = {
	.interested = 2, // system, server/gpu
	.signal = PTHREAD_COND_INITIALIZER,
	.mutex = PTHREAD_MUTEX_INITIALIZER
};

static struct barrier frame_late = {
	.interested = 1, // system
	.signal = PTHREAD_COND_INITIALIZER,
	.mutex = PTHREAD_MUTEX_INITIALIZER
};

static void kill_barrier(struct barrier *b)
{
	pthread_mutex_lock(&b->mutex);
	b->finishing = b->waiting;
	b->waiting = 0;
	pthread_cond_signal(&b->signal);
	pthread_mutex_unlock(&b->mutex);
}

void sync_kill(void)
{
	kill_barrier(&frame_end);
	kill_barrier(&frame_late);
}

static void sync_barrier(struct barrier *b)
{
	pthread_mutex_lock(&b->mutex);
//	log_f(CRD_LOG_VERBOSE, "Thread %x is waiting on %p", pthread_self(), b);
	b->waiting++;
	if (b->waiting >= b->interested) {
		b->finishing = b->waiting - 1;
		b->waiting = 0;
		pthread_cond_signal(&b->signal);
//		log_f(CRD_LOG_VERBOSE, "Thread %x signalled %p", pthread_self(), b);
	} else {
		pthread_cond_wait(&b->signal, &b->mutex);
		b->finishing--;
		if (b->finishing != 0) pthread_cond_signal(&b->signal);
//		log_f(CRD_LOG_VERBOSE, "Thread %x done with %p", pthread_self(), b);
	}
	pthread_mutex_unlock(&b->mutex);
}

static bool sync_now(struct barrier *b)
{
	pthread_mutex_lock(&b->mutex); // maybe unnecessary
	bool r = b->waiting >= b->interested - 1;
	pthread_mutex_unlock(&b->mutex);
	return r;
}

void sync_frame_end(void)
{
	sync_barrier(&frame_end);
}
bool sync_frame_end_now(void)
{
	return sync_now(&frame_end);
}

void sync_frame_late(void)
{
	sync_barrier(&frame_late);
}
bool sync_frame_late_now(void)
{
	return sync_now(&frame_late);
}
