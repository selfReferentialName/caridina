#include "util/queue.h"
#include "util/log.h"
#include "attributes.h"
#include "util/xmalloc.h"

#include <pthread.h>

#define THRD_COUNT 3
#define SEND_COUNT 1000

struct item {
	int sender;
	long item;
};

static pthread_t threads[THRD_COUNT];

static long recieved[THRD_COUNT];

static struct queue queue;

HIDDEN
void *reciever(void *in)
{
	for (int i = 0; i < THRD_COUNT; i++) {
		recieved[i] = -1;
	}
	int done = 0;
	while (done < THRD_COUNT) {
		struct item item;
		queue_shift(&queue, &item);
//		log_f(CRD_LOG_ALWAYS, "Recieved %ld from %d", item.item, item.sender);
		if (item.item != recieved[item.sender] + 1) {
			log_f(CRD_LOG_ALWAYS, "Uh oh, %ld != %ld + 1", item.item, recieved[item.sender]);
		}
		log_assert(item.item == recieved[item.sender] + 1);
		recieved[item.sender] = item.item;
		if (item.item >= SEND_COUNT - 1) {
			done++;
		}
	}
	return in;
}

HIDDEN
void *sender(void *in)
{
	int self = *(int*)in;
	free(in);
	for (long i = 0; i < SEND_COUNT; i++) {
		struct item item = {
			.sender = self,
			.item = i
		};
		queue_push(&queue, &item);
//		log_f(CRD_LOG_ALWAYS, "Sent %ld from %d", i, self);
	}
	return in;
}

int main(void)
{
	queue.size = sizeof(struct item);
	queue_init(&queue);
	for (int i = 0; i < THRD_COUNT; i++) {
		int *in = xmalloc(sizeof(int));
		*in = i;
		log_assert(pthread_create(&threads[i], NULL, &sender, in) == 0);
	}
	pthread_t reciever_thread;
	log_assert(pthread_create(&reciever_thread, NULL, &reciever, NULL) == 0);
	for (int i = 0; i < THRD_COUNT; i++) {
		log_assert(pthread_join(threads[i], NULL) == 0);
	}
	log_assert(pthread_join(reciever_thread, NULL) == 0);
	return 0;
}
