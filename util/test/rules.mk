util/test/hash: util/test/hash.o $(OBJECTS)
	$(LD) $(OBJECTS) util/test/hash.o $(LFLAGS) -o util/test/hash

util/test/queue: util/test/queue.o $(OBJECTS)
	$(LD) $(OBJECTS) util/test/queue.o $(LFLAGS) -o util/test/queue

check-util: util/test/hash util/test/queue
	util/test/hash
	util/test/queue

TESTS+=check-util
