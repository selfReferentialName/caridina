#include "util/hash.h"
#include "util/log.h"

const int N = 100;

bool gpu_demo_mesh;

static void check_flk(void)
{
	struct hash_table t = {
		.keysize = sizeof(int),
		.valsize = sizeof(int),
		.count = 1
	};

	for (int i = 0; i <= N; i++) {
		log_assert(!hash_table_get(t, &i, NULL));
	}

	for (int i = 0; i < N; i++) {
		int v = (i * 7537) % 4096;
		hash_table_set(&t, &i, &v);
	}
	log_assert(!hash_table_get(t, &N, NULL));

	for (int i = 0; i < N; i++) {
		int v;
		if (!hash_table_get(t, &i, &v)) {
			log_f(CRD_LOG_ALWAYS, "Couldn't get key %d", i);
			exit(1);
		}
		log_assert(v == (i * 7537) % 4096);
	}
	log_assert(!hash_table_get(t, &N, NULL));

	for (int i = 0; i < N; i++) {
		hash_table_remove(&t, &i);
	}

	for (int i = 0; i <= N; i++) {
		log_assert(!hash_table_get(t, &i, NULL));
	}

	hash_table_clean(&t);
}

static void check_vlk(void)
{
	struct hash_table_vlk t = {
		.valsize = sizeof(int)
	};

	for (int i = 0; i < N; i++) {
		int v = (i * 7537) % 4096;
		hash_table_vlk_set(&t, &i, sizeof(int), &v);
	}

	for (int i = 0; i < N; i++) {
		int v;
		log_assert(hash_table_vlk_get(&t, &i, sizeof(int), &v));
		log_assert(v == (i * 7537) % 4096);
	}

	hash_table_vlk_clean(&t);
}

int main(void)
{
	check_flk();
	check_vlk();
}
