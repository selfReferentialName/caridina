/** @file util/atomic-range.h
 * @brief A range which can be atomically expanded where 0 is invalid.
 */
#ifndef UTIL_ATOMIC_RANGE_H
#define UTIL_ATOMIC_RANGE_H

#include <stdatomic.h>

/// A range which can be atomically changed.
struct atomic_range {
	atomic_ulong min; ///< The minimum thing in the range.
	atomic_ulong max; ///< The maximum thing in the range.
};

/// Make sure something is in the range.
void atomic_range_insert(atomic_range *range, unsigned long val);

#endif
