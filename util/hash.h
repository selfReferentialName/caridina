/** @file util/hash.h
 * @brief Non-cryptographic hashing and hash tables and stuff.
 */
#ifndef UTIL_HASH_H
#define UTIL_HASH_H

#include "util/mem/arena.h"

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/** @brief Calculate a non-cryptographic hash.
 *
 * Guarenteed to return the same thing if fed the same data.
 * That is: `memcmy(d0, d1, n) == 0` implies `hash(n, d0) == hash(n, d1)`
 *
 * @param size The number of bytes of data to hash.
 * @param data The actual data to hash.
 *
 * @return The hash of the data.
 */
uint64_t hash(size_t size, const void *data);

/// A hash table. Intended to be passed by value if constant. Zero is initialisation aside from keysize and valsize.
struct hash_table {
	size_t keysize; ///< Size of a key. Set for initialisation and then don't change.
	size_t valsize; ///< Size of a value. Set for initialisation and then don't change.
	size_t count; ///< Backing of the key and value arrays. You can set on initialisation for initial size.
	char *keys; ///< Array of keys.
	char *values; ///< Array of values.
	bool *valid; ///< Array of where rows are valid.
};

/** @brief Set the value for a key in a hash table.
 *
 * Adds an element if the key isn't already there, and
 * overwrites if it is.
 *
 * @param table The table to set it in.
 * @param key The key to set.
 * @param val The value to set.
 */
void hash_table_set(struct hash_table *table, const void *key, const void *value);

/** @brief Get a value for a key in a hash table.
 *
 * If not found, return false and don't change value.
 *
 * @param table The table to look in.
 * @param key The key to look up.
 * @param value Where the value is written to.
 *
 * @return true if found.
 */
bool hash_table_get(struct hash_table table, const void *key, void *value);

/** @brief Remove a key from a hash_table.
 *
 * Does nothing if it isn't there.
 *
 * @param table The table to remove it from.
 * @param key The key to remove.
 */
void hash_table_remove(struct hash_table *table, const void *key); 

/** @brief free all memory in a hash table.
 *
 * `.count` is not reset, but `.keys`, `.values`, and `.valid` are.
 * This means it can be reused but will start with a bunch of memory after 3 allocations.
 *
 * @param table The table to clean.
 */
void hash_table_clean(struct hash_table *table);

/// A hash table with variable length keys. Zero is initialisation aside from valsize.
struct hash_table_vlk {
	size_t valsize; ///< Size of a value. Set for initialisation and then don't change.
	size_t count; ///< Backing of the key and value arrays. You can set on initialisation for initial size.
	void **keys; ///< Array of keys.
	size_t *keylens; ///< Array of key lengths.
	char *values; ///< Array of values.
	bool *valid; ///< Array of where rows are valid.
	struct mem_arena *key_arena; ///< Arena where keys are allocated on.
};

/** @brief Set the value for a key in a hash table with variable length keys.
 *
 * Adds an element if the key isn't already there, and
 * overwrites if it is.
 *
 * @param table The table to set it in.
 * @param key The key to set.
 * @param keylen The length of the key.
 * @param val The value to set.
 */
void hash_table_vlk_set(struct hash_table_vlk *table, const void *key, size_t keylen, const void *value);

/** @brief Get a value for a key in a hash table with variable length keys.
 *
 * If not found, return false and don't change value.
 *
 * @param table The table to look in.
 * @param key The key to look up.
 * @param keylen The length of the key.
 * @param value Where the value is written to.
 *
 * @return true if found.
 */
bool hash_table_vlk_get(const struct hash_table_vlk *table, const void *key, size_t keylen, void *value);

/** @brief free all memory in a hash table with variable length keys..
 *
 * `.count` is not reset, but everything else is freed and set to zero.
 * This means it can be reused but will start with a bunch of memory after 3 allocations.
 *
 * @param table The table to clean.
 */
void hash_table_vlk_clean(struct hash_table_vlk *table);

#endif
