#include "util/mem/arena.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#define N 18

char *str[N] = {
	"a",
	"cs",
	"bot",
	"late",
	"tines",
	"shorts",
	"chortle",
	"psyonics",
	"PsychOloG",
	"VeLoCiRaPt",
	"ntaeuhotahs",
	"123456789012",
	"snateuostnhc.",
	"lrcgbkjq;;lg'b",
	"sntaheuobbbqqbr",
	"fjcrq;wwmyqxirle",
	"abcdefghijklmnopqrstuvwxyz",
	"The quick, brown fox jumped over the lazy dog"
};

#define M (1024)

char *al[M];
int aj[M];

int main(void)
{
	struct mem_arena *arena = mem_arena_new(4096);

	for (int i = 0; i < M; i++) {
		int j = rand() % N;
		al[i] = mem_arena_malloc(arena, strlen(str[j]) + 1);
		aj[i] = j;
		strcpy(al[i], str[j]);
		assert(strcmp(al[i], str[aj[i]]) == 0);
		for (int k = 0; k < i; k++) {
			assert(al[i] != al[k]);
			assert(strcmp(al[k], str[aj[k]]) == 0);
		}
	}

	mem_arena_destroy(arena);

	puts("Success");
}
