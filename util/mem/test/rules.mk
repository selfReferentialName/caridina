TESTS+=check-mem-arena

check-mem-arena: util/mem/test/arena
	util/mem/test/arena

util/mem/test/arena: util/mem/test/arena.o util/mem/arena.o util/log.o config/general.o util/xmalloc.o
	$(CC) util/mem/test/arena.o util/mem/arena.o util/log.o config/general.o util/xmalloc.o $(LFLAGS) -o util/mem/test/arena
