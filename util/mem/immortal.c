#include "util/mem/immortal.h"
#include "util/log.h"
#include "util/xmalloc.h"

#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#define UNUSED __attribute__((unused))

struct free_list {
	struct free_list *next;
	size_t size;
	size_t remaining;
	char *free;
};

// TODO: consider spliting this to per alignment or making align* thread local
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

struct free_list *align1;
struct free_list *align2;
struct free_list *align4;
struct free_list *align8;
struct free_list *align16;
struct free_list *align32;

#define INITIAL_SIZE (2 * 1024 * 1024)

static void clean_list(struct free_list *list)
{
	if (list->next) clean_list(list->next);
	free(list);
}

__attribute__((visibility("hidden")))
void mem_imrtl_atexit(void)
{
	clean_list(align1);
	clean_list(align2);
	clean_list(align4);
	clean_list(align8);
	clean_list(align16);
}

void mem_imrtl_init(void)
{
	struct free_list template = {
		.next = NULL,
		.size = INITIAL_SIZE,
		.remaining = INITIAL_SIZE - sizeof(struct free_list)
	};

	align1 = xmalloc(INITIAL_SIZE);
	align2 = xmalloc(INITIAL_SIZE);
	align4 = xmalloc(INITIAL_SIZE);
	align8 = xmalloc(INITIAL_SIZE);
	align16 = xmalloc(INITIAL_SIZE);
	align32 = xmalloc(INITIAL_SIZE);

	memcpy(align1, &template, sizeof(struct free_list));
	memcpy(align2, &template, sizeof(struct free_list));
	memcpy(align4, &template, sizeof(struct free_list));
	memcpy(align8, &template, sizeof(struct free_list));
	memcpy(align16, &template, sizeof(struct free_list));
	memcpy(align32, &template, sizeof(struct free_list));

	align1->free = (char *) &align1[1];
	align2->free = (char *) &align2[1];
	align4->free = (char *) &align4[1];
	align8->free = (char *) &align8[1];
	align16->free = (char *) &align16[1];
	align32->free = (char *) &align32[1];
}

static void *alloc_in(struct free_list *list, size_t size)
{
	while (list->next && list->remaining < size) list = list->next;

	if (list->remaining < size) {
		size_t next_size = list->size * 2;
		if (next_size < size) next_size = (size / 4096 + 1) * 4096;
		list->next = xmalloc(next_size);
	}

	void *r = list->free;
	list->remaining -= size;
	return r;
}

void *mem_imrtl_malloc(size_t size)
{
	return xmalloc(size); // TODO: actually use code which currently has a bug

	log_assert(pthread_mutex_lock(&lock) == 0);
	void *r;
	if (size < 2) {
		r = alloc_in(align1, size);
	} else if (size < 4) {
		size += size & 1;
		r = alloc_in(align2, size);
	} else if (size < 8) {
		size = ((size - 1) / 4 + 1) * 4;
		r = alloc_in(align4, size);
	} else if (size < 16) {
		size = ((size - 1) / 8 + 1) * 8;
		r = alloc_in(align8, size);
	} else if (size < 32) {
		size = ((size - 1) / 16 + 1) * 16;
		r = alloc_in(align16, size);
	} else {
		size = ((size - 1) / 32 + 1) * 32;
		r = alloc_in(align32, size);
	}
	log_assert(pthread_mutex_unlock(&lock) == 0);
	return r;
}

static void *allocator_malloc(UNUSED void *unused, size_t size)
{
	return mem_imrtl_malloc(size);
}

malloc_t mem_imrtl_allocator = {
	.malloc = &allocator_malloc
};
