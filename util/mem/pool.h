/** @file util/mem/pool.h
 * @brief Fixed-size memory allocators
 */
#ifndef UTIL_MEM_POOL_H
#define UTIL_MEM_POOL_H

#include <stddef.h>

struct mem_pool *mem_pool_new(size_t elem_size); ///< Make a new pool for entries of a given size.

void *mem_pool_alloc(struct mem_pool *pool); ///< Allocate something from the pool.

void mem_pool_free(struct mem_pool *pool, void *what); ///< Free something to the pool.

void mem_pool_destroy(struct mem_pool *pool); ///< Destroy a memory pool.

#endif
