/** @file util/mem/fnptrs.h
 * @brief Ways to pass allocators into functions.
 */
#ifndef UTIL_MEM_FNPTRS_H
#define UTIL_MEM_FNPTRS_H

#include <stddef.h>

/// An allocator which can only allocate.
typedef struct {
	void *state; ///< Thing to pass to the functions.
	void *(*malloc)(void *state, size_t size); ///< Function like malloc.
} malloc_t;

/// An allocator which can also free.
typedef struct {
	malloc_t base;
	void *(*realloc)(void *state, void *ptr, size_t newsize); ///< Function like realloc.
	void (*free)(void *state, void *ptr); ///< Function like free.
} free_t;

/// Macro to call malloc of an allocator.
#define mem_malloc(allocator, size) (((malloc_t *) (allocator))->malloc(((malloc_t *) (allocator))->state, (size)))

/// Macro to allocate an array with an allocator.
#define mem_alloc(a, n, t) mem_malloc(a, (n) * sizeof(t))

/// Macro to call realloc of an allocator.
#define mem_realloc(allocator, ptr, size) ((allocator)->realloc(((malloc_t *) (allocator))->state, (ptr) (size)))

/// Macro to reallocate an array with an allocator.
#define mem_reallocv(a, p, n, t) mem_realloc(a, p, (n) * sizeof(t))

/// Macro to call free of an allocator.
#define mem_free(allocator, ptr) ((allocator)->free(((malloc_t *) (allocator))->state, (ptr)))

extern free_t mem_stdlib_allocator; ///< Standard library allocator (malloc, free, realloc)

#endif
