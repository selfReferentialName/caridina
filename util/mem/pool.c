#include "util/mem/pool.h"
#include "util/log.h"
#include "util/xmalloc.h"

#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>
#include <stdalign.h>
#include <stdbool.h>

#define INITIAL_SIZE (1024*128)

struct destroy_info {
	char *data; // pointer to free
	struct destroy_info *next;
};

struct mem_pool {
	size_t count; // number in previous creation
	size_t stride; // size of elements
	char **free;
	struct destroy_info *dstr;
};

static void make_block(struct mem_pool *r)
{
	r->free = xmalloc(r->count * r->stride);
	char **last = r->free;
	for (int i = 1; i < r->count; i++) {
		char *me = ((char *) last) + r->stride;
		*last = me;
		last = (char **) me;
	}
	*last = NULL;

	struct destroy_info *ld = r->dstr;
	r->dstr = xmalloc(sizeof(struct destroy_info));
	r->dstr->data = (void *) r->free;
	r->dstr->next = ld;
}

struct mem_pool *mem_pool_new(size_t size)
{
	if (size < sizeof(char *)) {
		size = sizeof(char *);
	}

/*	if (size < 2) { // I don't think this is neccessary
	} else if (size < 4) { // it makes sure sizes are aligned
		size = ((size - 1) / 2 + 1) * 2; // but I think C does that
	} else if (size < 8) {
		size = ((size - 1) / 4 + 1) * 4;
	} else if (size < 16) {
		size = ((size - 1) / 8 + 1) * 8;
	} else if (size < 32) {
		size = ((size - 1) * 16 + 1) * 16;
	} else {
		size = ((size - 1) * 32 + 1) * 32;
	} */

	struct mem_pool *r = xmalloc(sizeof(struct mem_pool));
	r->count = (INITIAL_SIZE - 1) / size + 1;
	r->stride = size;
	r->dstr = NULL;
	make_block(r);

	return r;
}

void *mem_pool_alloc(struct mem_pool *pool)
{
	while (true) {
		if (*pool->free != NULL) {
			void *r = (void *) pool->free;
			pool->free = (char **) *pool->free;
			return r;
		} else {
			pool->count *= 2;
			make_block(pool);
		}
	}
}

void mem_pool_free(struct mem_pool *pool, void *ptr)
{
	*(char **) ptr = (char *) pool->free;
	pool->free = (char **) ptr;
}

void mem_pool_destroy(struct mem_pool *pool)
{
	struct destroy_info *d = pool->dstr;
	while (d) {
		struct destroy_info *n = d->next;
		free(d->data);
		free(d);
		d = n;
	}
	free(pool);
}
