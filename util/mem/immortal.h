/** @file util/mem/immortal.h
 * @brief Allocate memory which lasts as long as the program.
 */
#ifndef UTIL_MEM_IMMORTAL_H
#define UTIL_MEM_IMMORTAL_H

#include "util/mem/fnptrs.h"

#include <stddef.h>

void mem_imrtl_init(void); ///< Call early before any other functions here.

void *mem_imrtl_malloc(size_t); ///< Immortal memory version of malloc.

/// Allocate an array of imortal values of a given type.
#define mem_imrtl_alloc(n, t) mem_imrtl_malloc(n * sizeof(t))

extern malloc_t mem_imrtl_allocator; ///< Allocator for immortal values.

#endif
