/** @file util/mem/arena.h
 * @brief Arenas. Many allocations to one malloc, quick and easy free at the end.
 */
#ifndef UTIL_MEM_ARENA_H
#define UTIL_MEM_ARENA_H

#include "util/mem/fnptrs.h"

#include <stddef.h>

struct mem_arena *mem_arena_new(size_t size); ///< Get a new arena to allocate in. Pass 0 for a default size.

void *mem_arena_malloc(struct mem_arena *arena, size_t size); ///< Like malloc in an arena.

/// Allocate an array with a given type in an arena.
#define mem_arena_alloc(a, n, t) mem_arena_malloc(a, n * sizeof(t))

void mem_arena_destroy(struct mem_arena *arena); ///< Free an arena all at once.

void mem_arena_reset(struct mem_arena *arena); ///< Free the data in an arena all at once while keeping the arena around.

malloc_t *mem_arena_allocator(struct mem_arena *arena); ///< Allocator for arena values.

#endif
