#include "util/mem/arena.h"
#include "util/log.h"
#include "util/xmalloc.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_ALIGN 32

// in this file, this gives both false positives and false negatives
#pragma GCC diagnostic ignored "-Wfree-nonheap-object"

struct mem_arena {
	size_t size; // across all chunks
	size_t remaining; // in active chunk
	size_t chunks;
	size_t chunk_back;
	char **ptrs;
	char *free;
};

#define DEFAULT_SIZE (64 * 1024)

struct mem_arena *mem_arena_new(size_t size)
{
	size = ((size - 1) / MAX_ALIGN + 1) * MAX_ALIGN;
	struct mem_arena *r = xmalloc(sizeof(struct mem_arena));
	r->size = size;
	r->remaining = size;
	r->chunks = 1;
	r->chunk_back = 128;
	r->ptrs = xmalloc(sizeof(char *) * 128);
	r->ptrs[0] = xmalloc(size);
	r->free = r->ptrs[0];
	return r;
}

void *mem_arena_malloc(struct mem_arena *arena, size_t size)
{
	size_t max_size = ((size - 1) / MAX_ALIGN + 1) * MAX_ALIGN;
	size_t align;
	if (size < 2) {
		align = 1;
	} else if (size < 4) {
		align = 2;
	} else if (size < 8) {
		align = 4;
	} else if (size < 16) {
		align = 8;
	} else if (size < 32) {
		align = 16;
	} else {
		align = 32;
	}
	while (true) {
		uintptr_t ru = (uintptr_t) arena->free;
		uintptr_t ri = ((ru - 1) / align + 1) * align;
		size += ri - ru;
		if (arena->remaining < size) {
			size_t next_size = arena->size * 2; // growth factor is one more
			if (next_size < max_size) next_size = max_size;
			arena->size += next_size;
			arena->remaining = next_size;
			arena->chunks++;
			log_assert(arena->chunks < arena->chunk_back); // TODO
			arena->ptrs[arena->chunks - 1] = xmalloc(next_size);
			arena->free = arena->ptrs[arena->chunks - 1];
			continue;
		} else {
			arena->remaining -= size;
			arena->free += size;
			return (void *) ri;
		}
	}
}

void mem_arena_destroy(struct mem_arena *arena)
{
	for (int i = 0; i < arena->chunks; i++) {
		free(arena->ptrs[i]);
	}
	free(arena->ptrs);
	free(arena);
}

void mem_arena_reset(struct mem_arena *arena)
{
	if (arena->chunks == 1) return; // already not fragmented
	for (int i = 0; i < arena->chunks; i++) {
		free(arena->ptrs[i]);
	}
	arena->ptrs[0] = xmalloc(arena->size);
	arena->free = arena->ptrs[0];
	arena->chunks = 1;
}

malloc_t *mem_arena_allocator(struct mem_arena *arena)
{
	malloc_t *r = mem_arena_alloc(arena, 1, malloc_t);
	*r = (malloc_t) {
		.state = (void *) arena,
		.malloc = (void *(*)(void *, size_t)) &mem_arena_malloc
	};
	return r;
}
