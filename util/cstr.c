#include "util/cstr.h"

#include <string.h>
#include <ctype.h>

const char *cstr_index(const char *s, char c)
{
	for (; *s; s++) {
		if (*s == c) return s;
	}
	return NULL;
}

void cstr_tolower_inplace(char *s)
{
	for (; *s; s++) {
		*s = tolower(*s);
	}
}
