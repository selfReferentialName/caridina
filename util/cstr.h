/** @file util/cstr.h
 * @brief Convinience functions to work with null terminated strings
 */
#ifndef UTIL_CSTR_H
#define UTIL_CSTR_H

/** @brief Find a character in a string.
 *
 * Return a pointer to the first instance of a character in a string
 * or NULL if the end of the string comes first.
 *
 * @param s The string to search in.
 * @param c The character to search for.
 *
 * @return A pointer in s to c.
 */
const char *cstr_index(const char *s, char c);

/** @brief Convert a string to lowercase in-place.
 *
 * Runs tolower on every character in a string in-place.
 *
 * @param s The string to convert to lowercase.
 */
void cstr_tolower_inplace(char *s);

#endif
