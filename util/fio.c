#include "util/fio.h"
#include "util/log.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

struct fio_file_data fio_read_file(const char *path)
{
	log_f(CRD_LOG_VERBOSE, "Reading file %s", path);
	struct stat stats;
	log_assert(stat(path, &stats) == 0);
	struct fio_file_data r = {
		.size = stats.st_size,
		.data = malloc(stats.st_size + 1)
	};
	log_assert(r.data);
	int fd = open(path, O_RDONLY);
	log_assert(fd != -1);
	log_assert(read(fd, r.data, r.size) == r.size);
	close(fd);
	r.data[r.size] = 0;
	return r;
}

void fio_free_file(struct fio_file_data data)
{
	free(data.data);
}
