/** @file util/log.h
 * @brief Log various events.
 */
#ifndef UTIL_LOG_H
#define UTIL_LOG_H

// Loging stuff below this level should be optimised out.
#ifndef NDEBUG
#define LOG_MAX_LEVEL CRD_LOG_VERY_VERBOSE
#else
#define LOG_MAX_LEVEL CRD_LOG_VERBOSE
#endif

// for enum crd_log_level
#include <caridina/_log.h>

#include <stdlib.h>
#include <errno.h>

/** @brief Log a string like puts.
 *
 * Log a string at a given log level. Automatically appends a newline.
 *
 * @param msg The message to log.
 * @param level The verbosity of the message.
 *
 * @see log_f
 */
#define logs(msg, level) \
	do { \
		if (level <= LOG_MAX_LEVEL) { \
			log_raw(msg, level, __FILE__, __LINE__); \
		} \
	} while (0)

/** @brief Log with formatting like printf.
 *
 * Log a formatted message at a given log level. The message format
 * is identical to that of printf. Appends a newline.
 *
 * @param level The verbosity of the message.
 * @param fmt The message format.
 *
 * @see logs
 */
#define log_f(level, fmt, ...) \
	do { \
		if (level <= LOG_MAX_LEVEL) { \
			logf_raw(level, __FILE__, __LINE__, fmt, __VA_ARGS__); \
		} \
	} while (0)

/** @brief Assert an expression.
 *
 * Acts like C's standard function assert, but calls log on failure.
 *
 * @param e The expression which must be nonzero.
 *
 * @see log_unreachable log_todo
 */
#define log_assert(e) \
	do { \
		if (!(e)) { \
			int log_errno = errno; \
			logs("Assertation failure: " #e, CRD_LOG_ALWAYS); \
			errno = log_errno; \
			log_errnos(); \
			exit(1); \
		} \
	} while (0)

/** @brief Complain that code is unreachable.
 *
 * Essentially the same thing as log_assert(false).
 *
 * @see log_assert log_todo
 */
#define log_unreachable() \
	do { \
		logs("Unreachable code reached.", CRD_LOG_ALWAYS); \
		exit(1); \
	} while(0)

/** @brief Complain that code needs implementing.
 *
 * Just a fancy log_unreachable
 *
 * @see log_assert log_unreachable
 */
#define log_todo(msg) \
	do { \
		logs("Code path is TODO: " msg, CRD_LOG_ALWAYS); \
		exit(1); \
	} while(0)

/** @brief Print a log message before running something.
 *
 * @see logs
 */
#define log_checkpoint(e, level) \
	do { \
		logs(#e, level); \
		e; \
	} while(0)

/// Put in a string for printing a mat4
#define PF_MAT4 "\\begin{bmatrix}%f&%f&%f&%f\\\\%f&%f&%f&%f\\\\%f&%f&%f&%f\\\\%f&%f&%f&%f\\end{bmatrix}"

/// Put in a format stuff for printing a mat4
#define PV_MAT4(m) m[0][0], m[1][0], m[2][0], m[3][0], m[0][1], m[1][1], m[2][1], m[3][1], \
	m[0][2], m[1][2], m[2][2], m[3][2], m[0][3], m[1][3], m[2][3], m[3][3]

// log is a macro which calls this function
void log_raw(const char *msg, enum crd_log_level level, const char *file, int line);

// logf is a macro which calls this function
void logf_raw(enum crd_log_level level, const char *file, int line, const char *fmt, ...);

/// Log all errno like variables if they're not success.
void log_errnos(void);

#endif
