#include "util/hash.h"
#include "util/log.h"
#include "util/xmalloc.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdalign.h>

uint64_t hash(size_t size, const void *data)
{
	const char *cd = (void *) data;
	uint64_t r = 0;
	while ((uintptr_t) cd % alignof(uint64_t) != 0 && size > 0) { // while not aligned
		r += *cd;
		cd++; size--;
	}
	const uint64_t *cu = (void *) cd;
	while (size > sizeof(uint64_t)) {
		r ^= *cu + ((r >> 23) | (r << 41)) + r*11400714819323198485ull;
		cu++; size -= sizeof(uint32_t);
	}
	while (size > 0) {
		r += *cd;
		cd++; size--;
	}
	return r;
}

static void expand(struct hash_table *t)
{
	// there's an off by one error somewhere in the set and get functions
	// adding 1 to the value count here fixes it
	if (t->keys == NULL || t->values == NULL) {
		assert(t->keys == NULL && t->values == NULL);
		t->count = t->count > 0 ? t->count : 1024;
		t->keys = xmalloc(t->keysize*(t->count));
		t->values = xmalloc(t->valsize*(t->count));
		t->valid = calloc(sizeof(bool), t->count);
	} else {
		struct hash_table new = *t;
		new.count *= 2;
		new.keys = xmalloc(new.keysize*(new.count));
		new.values = xmalloc(new.valsize*(new.count));
		new.valid = calloc(sizeof(bool), new.count);
		for (size_t i = 0; i < t->count; i++) {
			if (t->valid[i]) {
				hash_table_set(&new, t->keys + t->keysize*i, t->values + t->valsize*i);
			}
		}
		if (t->keys) free(t->keys);
		if (t->values) free(t->values);
		if (t->valid) free(t->valid);
		*t = new;
	}
}

static void vlk_set_copied(struct hash_table_vlk *t, void *k, size_t kl, const void *v);

static void expand_vlk(struct hash_table_vlk *t)
{
	if (t->key_arena == NULL) t->key_arena = mem_arena_new(4096);
	if (t->keys == NULL || t->values == NULL) {
		assert(t->keys == NULL && t->values == NULL);
		t->count = t->count ? t->count : 1024;
		t->keys = xmalloc(sizeof(void *)*(t->count));
		t->values = xmalloc(t->valsize*(t->count));
		t->valid = calloc(sizeof(bool), t->count);
		t->keylens= xmalloc(sizeof(size_t)*(t->count));
	} else {
		struct hash_table_vlk new;
		new = *t;
		new.count *= 2;
		new.keys = xmalloc(sizeof(void *)*(new.count));
		new.values = xmalloc(new.valsize*(new.count));
		new.valid = calloc(sizeof(bool), new.count);
		new.keylens= xmalloc(sizeof(size_t)*(new.count));
		for (size_t i = 0; i < t->count; i++) {
			if (t->valid[i]) {
				vlk_set_copied(&new, t->keys[i], t->keylens[i], t->values + t->valsize*i);
			}
		}
		if (t->keys) free(t->keys);
		if (t->values) free(t->values);
		if (t->valid) free(t->valid);
		if (t->keylens) free(t->keylens);
		*t = new;
	}
}

// expand if we linear probe this fraction of the table size
#define EXPAND_DENOM 2

void hash_table_set(struct hash_table *t, const void *k, const void *v)
{
	if (t->valid == NULL || t->keys == NULL || t->values == NULL) {
		expand(t);
	}

	uint64_t h = hash(t->keysize, k);

	size_t i = h % t->count;
	for (int n = 0; n < t->count / EXPAND_DENOM; n++) {
		if (!t->valid[i]) goto found;
		if (memcmp(t->keys + t->keysize*i, k, t->keysize) == 0) goto found;
		i++;
		if (i >= t->count) i = 0;
	}
	expand(t);
	hash_table_set(t, k, v);
	return;

found:
	t->valid[i] = true;
	memcpy(t->keys + t->keysize*i, k, t->keysize);
	memcpy(t->values + t->valsize*i, v, t->valsize);
}

static void vlk_set_copied(struct hash_table_vlk *t, void *k, size_t kl, const void *v)
{
	if (t->valid == NULL || t->keys == NULL || t->values == NULL) {
		expand_vlk(t);
	}

	uint64_t h = hash(kl, k);

	size_t i = h % t->count;
	for (int n = 0; n < t->count / EXPAND_DENOM; n++) {
		if (!t->valid[i]) goto found;
		if (t->keylens[i] == kl && memcmp(t->keys[i], k, kl) == 0) goto found;
		i = (i+1) % t->count;
	}
	expand_vlk(t);
	vlk_set_copied(t, k, kl, v);

found:
	t->valid[i] = true;
	t->keys[i] = k;
	t->keylens[i] = kl;
	memcpy(t->values + t->valsize*i, v, t->valsize);
}

void hash_table_vlk_set(struct hash_table_vlk *t, const void *k, size_t kl, const void *v)
{
	if (t->key_arena == NULL) {
		t->key_arena = mem_arena_new(t->count*kl);
	}
	void *kc = mem_arena_malloc(t->key_arena, kl);
	memcpy(kc, k, kl);
	vlk_set_copied(t, kc, kl, v);
}

bool hash_table_get(struct hash_table t, const void *k, void *v)
{
	if (t.valid == NULL || t.keys == NULL || t.values == NULL) return false;

	uint64_t h = hash(t.keysize, k);

	bool looped = false;
	bool found = false;
	size_t i;
	for (i = h % t.count; !looped || i != h % t.count; i = (i + 1) % t.count) {
		if (t.valid[i] && memcmp(t.keys + t.keysize*i, k, t.keysize) == 0) {
			found = true;
			break;
		}
		looped = true;
	}

	if (!found) {
		return false;
	} else {
		memcpy(v, t.values + t.valsize*i, t.valsize);
		return true;
	}
}

bool hash_table_vlk_get(const struct hash_table_vlk *t, const void *k, size_t kl, void *v)
{
	if (t->valid == NULL || t->keys == NULL) return false;

	uint64_t h = hash(kl, k);

	bool looped = false;
	bool found = false;
	size_t i;
	for (i = h % t->count; !looped || i != h % t->count; i = (i + 1) % t->count) {
		if (t->valid[i] && t->keylens[i] == kl && memcmp(t->keys[i], k, kl) == 0) {
			found = true;
			break;
		}
		looped = true;
	}

	if (!found) {
		return false;
	} else {
		memcpy(v, t->values + t->valsize*i, t->valsize);
		return true;
	}
}

void hash_table_remove(struct hash_table *t, const void *k)
{
	if (t->valid == NULL || t->keys == NULL || t->values == NULL) return;

	uint64_t h = hash(t->keysize, k);

	bool looped = false;
	size_t i;
	for (i = h % t->count; !looped || i != h % t->count; i = (i + 1) % t->count) {
		if (t->valid[i] && memcmp(t->keys + t->keysize*i, k, t->keysize) == 0) {
			t->valid[i] = false;
			return;
		}
		looped = true;
	}
}

void hash_table_clean(struct hash_table *t)
{
	free(t->keys);
	free(t->values);
	free(t->valid);
	t->keys = NULL;
	t->values = NULL;
	t->valid = NULL;
}

void hash_table_vlk_clean(struct hash_table_vlk *t)
{
	free(t->keys);
	free(t->values);
	free(t->valid);
	free(t->keylens);
	mem_arena_destroy(t->key_arena);
	t->keys = NULL;
	t->values = NULL;
	t->valid = NULL;
	t->keylens= NULL;
	t->key_arena = NULL;
}
