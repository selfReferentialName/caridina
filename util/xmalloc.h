/** @file util/xmalloc.h
 * @brief Error checking malloc.
 */
#ifndef UTIL_XMALLOC_H
#define UTIL_XMALLOC_H

#include <stddef.h>

/// Call malloc and assert that it doesn't return null.
void *xmalloc(size_t size);

#endif
