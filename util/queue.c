#include "util/queue.h"
#include "util/log.h"
#include "util/xmalloc.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

struct queue_entry {
	struct queue_entry *next;
	char data[1];
};

void queue_init(struct queue *queue)
{
	pthread_mutex_init(&queue->lock, NULL);
	log_assert(sem_init(&queue->sem, 0, 0) == 0);
	queue->pool = mem_pool_new(queue->size + sizeof(struct queue_entry) - 1);
	queue->shift = NULL;
	queue->push = NULL;
}

void queue_shift(struct queue *queue, void *out)
{
	log_assert(sem_wait(&queue->sem) == 0);
	pthread_mutex_lock(&queue->lock);
	log_assert(queue->shift);
	memcpy(out, queue->shift->data, queue->size);
	struct queue_entry *shifted = queue->shift;
	queue->shift = shifted->next;
	if (queue->shift == NULL) queue->push = NULL;
	mem_pool_free(queue->pool, shifted);
	pthread_mutex_unlock(&queue->lock);
}

void queue_push(struct queue *queue, const void *in)
{
	pthread_mutex_lock(&queue->lock);
	struct queue_entry *new = mem_pool_alloc(queue->pool);
	new->next = NULL;
	memcpy(new->data, in, queue->size);
	if (queue->push != NULL) {
		queue->push->next = new;
		queue->push = new;
	} else {
		queue->push = new;
		queue->shift = new;
	}
	pthread_mutex_unlock(&queue->lock);
	log_assert(sem_post(&queue->sem) == 0);
}
