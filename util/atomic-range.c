#include "util/atomic-range.h"


/// A range which can be atomically changed.
struct atomic_range {
        atomic_ulong min; ///< The minimum thing in the range.
        atomic_ulong max; ///< The maximum thing in the range.
};

/// Make sure something is in the range.
void atomic_range_insert(volatile struct atomic_range *range, unsigned long val)
{
	unsigned long i = range->min;
	unsigned long a = range->max;
	while (i == 0 || a == 0 || f < i || f > a) {
		if (i == 0 || f < i) {
			atomic_compare_exchange_weak(&range->min, &i, f);
		}
		if (a == 0 || f > a) {
			atomic_compare_exchange_weak(&range->max, &a, f);
		}
	}
}
