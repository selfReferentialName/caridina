/** @file util/sync.h
 * @brief Synchronisation points to fence across multiple servers.
 *
 * A sync point is called through a function which waits until all
 * interested threads have called the function.
 *
 * The number of interested threads is hardcoded in util/sync.c
 */
#ifndef UTIL_SYNC_H
#define UTIL_SYNC_H

#include <stdbool.h>

void sync_frame_end(void); ///< End frame transition.
bool sync_frame_end_now(void); ///< Ready for end frame transition?
void sync_frame_late(void); ///< ECS has stopped.
bool sync_frame_late_now(void); ///< ECS about to stop?

void sync_kill(void); ///< Stop everything from waiting on a sync point.

#endif
