/** @file util/queue.h
 * @brief General thread-safe queue datastructure for server commands.
 */
#ifndef UTIL_QUEUE_H
#define UTIL_QUEUE_H

#include "util/mem/pool.h"

#include <stdbool.h>
#include <stddef.h>
#include <pthread.h>
#include <stdint.h>
#include <semaphore.h>

/// A queue.
struct queue {
	pthread_mutex_t lock; ///< Locked when dealing with the queue.
	sem_t sem; ///< Number of available items.
	size_t size; ///< The size of an element.
	struct mem_pool *pool; ///< (internal) Cells are allocated here.
	struct queue_entry *shift; ///< (internal) The entry to shift off.
	struct queue_entry *push; ///< (internal) The entry to put a new entry after.
};

/// Initialise a queue
void queue_init(struct queue *queue);

/** @brief Get something off a queue.
 *
 * If the queue is empty then block until it isn't.
 *
 * @param queue The queue to shift from.
 * @param out Where to write the shifted off thing.
 */
void queue_shift(struct queue *queue, void *out);

/** @brief Put something on a queue.
 *
 * Signals to make anything blocking in queue_shift stop blocking
 * and use the given data.
 *
 * @param queue The queue to push on.
 * @param in What to push onto the queue.
 */
void queue_push(struct queue *queue, const void *in);

#endif
