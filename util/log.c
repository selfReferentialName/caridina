#include "util/log.h"
#include "config/general.h"

#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

// + 1 for useless null terminator
static const char level_char[LOG_MAX_LEVEL + 1] = "!EW+-Vv";

void log_raw(const char *msg, enum crd_log_level level, const char *file, int line)
{
	if (level <= cfg_log_level) {
		printf("[%c] %s at %s:%d\n", level_char[level], msg, file, line);
	}
}

void logf_raw(enum crd_log_level level, const char *file, int line, const char *fmt, ...)
{
	if (level <= cfg_log_level) {
		printf("[%c] ", level_char[level]);
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		printf(" at %s:%d\n", file, line);
	}
}

void log_errnos(void)
{
	if (errno > 0) {
		log_f(CRD_LOG_ALWAYS, "errno: %s", strerror(errno));
	}
}
