#version 450

layout(location=0) in vec3 iPos;
layout(location=1) in vec3 iNml;
layout(location=2) in vec2 iUv;

layout(location=0) out vec4 oNml;

void main()
{
	oNml.xyz = normalize(iNml);
	oNml.w = 1.0;
}
