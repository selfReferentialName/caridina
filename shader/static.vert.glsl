#version 450

layout(push_constant) uniform constants {
	mat4 persp;
} pc;

// instance
layout(location=3) in mat4 iMv;

// vertex
layout(location=0) in vec3 iPos;
layout(location=1) in vec3 iNml;
layout(location=2) in vec2 iUv;

// outputs
layout(location=0) out vec3 oPos;
layout(location=1) out vec3 oNml;
layout(location=2) out vec2 oUv;

void main()
{
	vec4 hPos = vec4(iPos, 1);
	gl_Position = pc.persp*iMv*hPos;
	oPos = gl_Position.xyz/gl_Position.w;
	oNml = (transpose(inverse(iMv))*vec4(iNml, 0)).xyz;
	oUv = iUv;
}
