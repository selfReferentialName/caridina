SHADER_FRAGS!=ls shader/*.frag.glsl
SHADER_VERTS!=ls shader/*.vert.glsl

.for source in $(SHADER_FRAGS)
crd/${source:S/.glsl//}: $(source)
	glslangValidator -S frag -V -o crd/${source:S/.glsl//} $(source)
RESOURCES+=crd/${source:S/.glsl//}
.endfor

.for source in $(SHADER_VERTS)
crd/${source:S/.glsl//}: $(source)
	glslangValidator -S vert -V -o crd/${source:S/.glsl//} $(source)
RESOURCES+=crd/${source:S/.glsl//}
.endfor
