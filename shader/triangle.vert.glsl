#version 450

layout(location = 0) out vec3 oCol;

void main()
{
	const vec3 positions[6] = vec3[6](
		vec3(-1, 1, 0),
		vec3(1, 1, 0),
		vec3(0, 0, 0),
		vec3(-1, -1, 0),
		vec3(1, -1, 0),
		vec3(0, 0, 0)
	);
	gl_Position = vec4(positions[gl_VertexIndex], 1);

	const vec3 cols[6] = vec3[6](
		vec3(1, 0, 0),
		vec3(0, 1, 0),
		vec3(0, 0, 1),
		vec3(1, 0, 0),
		vec3(0, 1, 0),
		vec3(0, 0, 1)
	);
	oCol = cols[gl_VertexIndex];
}
