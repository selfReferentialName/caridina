#version 450

layout(location=0) in vec3 iCol;

layout(location=0) out vec4 oCol;

void main()
{
	oCol = vec4(iCol, 1);
}
