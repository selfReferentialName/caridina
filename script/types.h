/** @file script/types.h
 * @brief All the types available for scripting, and ways to get out the values.
 */
#ifndef SCRIPT_TYPES_H
#define SCRIPT_TYPES_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#if INTPTR_MAX <= INT64_MAX
/// A boxed value.
typedef int64_t lsp_t;
#else
/// A boxed value.
typedef intptr_t lsp_t;
#endif

/// The different type IDs for lsp_t values.
enum lsp_base_type {
	LSP_BASIC = 0, ///< boolean, char, void, null, log-port
	LSP_INT = 1, ///< 60 bit signed integer
	LSP_FLOAT = 2, ///< Floating point number with at least single precision
	LSP_SYMBOL = 3, ///< A symbol
	LSP_PAIR = 4, ///< A cons cell
	LSP_CONT = 5, ///< A continuation frame
	LSP_ARRAY = 6, ///< A multidimensional unpadded array
	LSP_RECORD = 7 ///< Some other type or a polymorphic boxed value
};

/// Base type IDs >= this number are tagged pointers
#define LSP_PTR_MIN LSP_PAIR

/// An arbitrary type ID beyond just what's in lsp_base_type.
typedef unsigned long lsp_type_t;

/// Returns true if this type fits in one lsp_t.
inline bool lsp_is_base_type(lsp_type_t t) { return (t & 7) == 0 || t < 7; }

/// An array to lookup the size of a non-base type and LSP_PAIR.
extern size_t *lsp_sizeof_non_base;

/// Get the size of any type.
inline size_t lsp_sizeof(lsp_type_t t) { return t != LSP_PAIR && lsp_is_base_type(t) ? sizeof(lsp_t) : lsp_sizeof_non_base[t]; }

/// The maximum registered type plus one.
extern lsp_type_t lsp_type_count;

/// The backing size of type info tables.
extern lsp_type_t lsp_type_back;

/// Unbox a tagged pointer
#define LSP_UBOX_PTR(x) ((void *) ((x) & ~(lsp_t)7))
/// Box a tagged pointer
#define LSP_BOX_PTR(x, t) (((lsp_t) (x)) | (t))

/// Extract the type tag from a boxed value
#define LSP_TYPEOF(x) ((x) & 7)

/// All kinds of LSP_BASIC aside from chars
enum lsp_basic {
	LSP_FALSE = 0x0, ///< #f
	LSP_TRUE = 0x8, ///< #t
	LSP_VOID = 0x10, ///< An unusable value used for undefined returns.
	LSP_NULL = 0x18, ///< The empty list.
	LSP_LOG_PORT = 0x20, ///< The output port hooked up to logging.
};

/// Return true if the value is a char
#define LSP_CHAR(x) (((x) & 0xff) == 0x80)
/// Unbox the value of a char
#define LSP_UBOX_CHAR(x) ((x) >> 8)
/// Box a char
#define LSP_BOX_CHAR(x) (((x) << 8) | 0x80)

/// Unbox a 60 bit integer.
#define LSP_UBOX_INT(x) ((x) >> 3)
/// Box a 60 bit integer.
#define LSP_BOX_INT(x) (((x) << 3) | LSP_INT)

/// Unbox a float
#define LSP_UBOX_FLOAT(x) (((union {float f; uint32_t i;}) {.i = (x) >> 3}).f)
/// Box a float
#define LSP_BOX_FLOAT(x) ((((union {float f; uint64_t i;}) {.f = (x)}).i << 3) | LSP_FLOAT)

/// Unbox a symbol
#define LSP_UBOX_SYMBOL(x) LSP_UBOX_INT(x)
/// Box a symbol
#define LSP_BOX_SYMBOL(x) (((x) << 8) | LSP_SYMBOL)

/// A cons cell.
struct lsp_pair {
	lsp_t car; ///< The first bit.
	lsp_t cdr; ///< The second bit.
};

/// A continuation frame.
struct lsp_cont {
	struct lsp_cont *base; ///< The next frame up on the call stack or NULL.
	struct lsp_thunk (*run)(lsp_cont *self, lsp_t arg); ///< Run the continuation and return the next continuation and its argument.
	size_t size; ///< The number of local variables.
	lsp_t locals[]; ///< The local variables of this frame for garbage collection.
};

/// A continuation and argument ready to be called.
struct lsp_thunk {
	lsp_t arg; ///< What to pass in to run.
	struct lsp_cont cont; ///< The continuation itself.
};

/// An upper and lower bound.
struct lsp_bounds {
	size_t lower; ///< Lower bound.
	size_t upper; ///< Upper bound.
};

/// A multidimensional unpadded array.
struct lsp_array {
	char *data; ///< The data in unpadded column major order.
	lsp_type_t type; ///< The type of the elements.
	size_t dims; ///< The number of dimensions.
	struct lsp_bounds shape[]; ///< The sizes in each dimension.
};

/// Get the length of an array on the nth dimension.
inline size_t lsp_array_stride(struct lsp_array *a, size_t n) { return a->shape[n].upper - a->shape[n].lower; }

// Credit to stackoverflow user netcoder for this hack
// https://stackoverflow.com/questions/11761703/overloading-macro-on-number-of-arguments
#define LSP_ARRAY_LOOKUP_GET_MACRO(_1, _2, _3, _4, NAME, ...) NAME
inline void *LSP_ARRAY_LOOKUP1(struct lsp_array *a, size_t i) { return a->data + lsp_sizeof(a->type)*i; }
inline void *LSP_ARRAY_LOOKUP2(struct lsp_array *a, size_t i, size_t j) { return a->data + lsp_sizeof(a->type)*(j+lsp_array_stride(a,0)*i); }
inline void *LSP_ARRAY_LOOKUP2(struct lsp_array *a, size_t i, size_t j, size_t k) { return a->data + lsp_sizeof(a->type)*(k+lsp_array_stride(a,1)*(j+lsp_array_stride(a,0)*i)); }

/// Get a pointer to an element in a multidimensional array.
/// This uses a nasty hack for varadicity which can't do arbitrary dimensions.
/// If you need more than 3, duplicate the code implementing this.
/// Use like LSP_ARRAY_LOOKUP(array, row, column) for a 2d array.
#define LSP_ARRAY_LOOKUP(...) LSP_ARRAY_LOOKUP_GET_MACRO(__VA_ARGS__, LSP_ARRAY_LOOKUP3, LSP_ARRAY_LOOKUP2, LSP_ARRAY_LOOKUP1, )(__VA_ARGS__)

/// How a record is layed out in memory for GC and reflection purposes.
struct lsp_record_layout {
	size_t len; ///< The number of members.
	lsp_type_t members[]; ///< The member types.
};

/// The layout of all non-base types.
extern struct lsp_record_layout **lsp_record_layouts;

#endif
