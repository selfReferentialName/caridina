(define globals-table
  (make-hash-table
    keys: (module-variable-names (lisp))
    values: (module-variable-values (lisp))))

(define (eval-begin expr locals)
  (cond
    ((null? expr)
     (void))
    ((null? (cdr expr))
     (eval (car expr) locals))
    (else
     (eval (car expr) locals)
     (eval-begin (cdr expr) locals))))

(define (eval-lambda expr locals)
  (cond
    ((symbol? (car expr))
     (lambda args (eval-begin (cdr expr) (cons (cons (car expr) args) locals)))) ; an little optimisation to not do all the below
    (else
     (lambda argv
       (let loop
           ((argv* argv)
            (args (car expr))
            (locals* (append (list-ec (:list a (car expr)) (if (pair? a)) (begin (assert (= (length a) 3)) (cadr a))) locals))
            (keyword-only? #f))
         (cond
           ((not (pair? args))
            (eval-begin (cdr expr) (cons (cons args argv*) locals*)))
           ((null? argv*)
            (assert (or (null? args) (pair? (car args))))
            (eval-begin (cdr expr) locals*))
           ((not (keyword? (car argv*)))
            (assert (not keyword-only?))
            (loop (cdr argv*) (cdr args) (cons (cons (if (pair? (car args) (caar args) (car args))) (car argv*)) locals*) keyword-only?))
           ((not (or (memv (keyword->symbol (car argv*)) args) (assv (keyword->symbol (car argv*)) args)))
            (assert (not keyword-only))
            (loop (cdr argv*) (cdr args) (cons (cons (if (pair? (car args) (caar args) (car args))) (car argv*)) locals*) keyword-only?))
           (else
            (let inner ((prev #f) (cur args) (kw (keyword->symbol (car argv*))))
              (assert (pair? cur))
              (if (or (eqv? kw (car cur)) (and (pair? (car cur)) (eqv? kw (caar cur))))
                (if prev
                  (begin
                    (set-cdr! prev (cdr args))
                    (loop (cddr argv*) args (cons (cons kw (cadr argv*)) locals*) #t))
                  (loop (cddr argv*) (cdr cur) (cons (cons kw (cadr argv*)) locals*) #t))
                (inner cur (cdr cur) kw))))))))))

(define (eval-lambda* expr locals)
  (if (symbol? (car expr))
     (lambda args (eval-begin (cdr expr) (cons (cons (car expr) args) locals)))
     (lambda args
       (let loop ((argn* (car expr)) (argv* args) (locals* locals))
         (cond
           ((or (null? argn*) (null? argv*))
            (assert (and (null? argn*) (null? argv*)))
            (eval-begin (cdr expr) locals*))
           ((symbol? argn*)
            (eval-begin (cdr expr) (cons (cons argn* argv*) locals*)))
           (else
            (loop (cdr argn*) (cdr argv*) (cons (cons (car argn*) (car argv*)) locals*))))))))

(define (eval expr (locals '()))
  (cond
    ((symbol? expr) (or (map-true cdr (assv expr locals)) (hash-table-ref globals-table expr)))
    ((pair? expr)
     (case (car expr)
       ((define)
        (hash-table-set! globals-table (cadr expr) (eval (caddr expr) locals)))
       ((set!)
        (if (assv (cadr expr) locals)
          (set-cdr! (assv (cadr expr) locals) (eval (caddr expr) locals))))
       ((lambda)
        (eval-lambda (cdr expr) locals))
       ((lambda*)
        (eval-lambda* (cdr expr) locals))
       ((if)
        (assert (<= 2 (length expr) 3))
        (if (eval (cadr expr) locals)
          (eval (caddr expr) locals)
          (if (pair? (cdddr expr)) (eval (cadddr expr) locals))))
       ((begin)
        (eval-begin (cdr expr) locals))
       ((quote) (cadr expr))
       ((quasiquote)
        (eval-quasiquote (cadr expr) locals))
       ((unquote unquote-splicing)
        (unreachable))
       (else
        (apply (eval (car expr) locals) (map (lambda (x) (eval x locals)))))))
    (else expr)))
