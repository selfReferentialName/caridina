# User changeable configuration.
CC=cc
CXX=c++
LD=c++
CFLAGS=-O2 -Wall -Werror -Wno-error=missing-braces -Wno-error=unused-variable -Wno-error=unused-const-variable -g -O0 -rdynamic

# Update configuration to include dependencies and stuff
# If the build system can't find a dependency, consider changing these
CFLAGS_CGLM!=pkg-config --cflags cglm
CFLAGS_FLECS=
CFLAGS_GLFW!=pkg-config --cflags glfw3
LFLAGS:=$(CFLAGS) -lglfw -lcglm -lm -ldl -lpthread -lX11 -lXrandr -lXi -lvulkan -lVulkanMemoryAllocator
CFLAGS+=$(CFLAGS_CGLM) $(CFLAGS_FLECS) $(CFLAGS_GLFW) -I. -Iinclude

.MAIN: caridina.so caridina.respack

OBJECTS=
RESOURCES=
INTERMEDIATES=
TESTS=

.SUFFIXES: .o

.include "glue/rules.mk"
.include "server/rules.mk"
.include "util/rules.mk"
.include "config/rules.mk"
.include "system/rules.mk"
#.include "script/rules.mk"
.include "shader/rules.mk"
.include "tool/rules.mk"
.include "meshes/rules.mk"
.include "ecs/rules.mk"
.include "examples/rules.mk"

.include "util/test/rules.mk"

caridina.so: $(OBJECTS)
	$(LD) $(OBJECTS) $(LFLAGS) -shared -o caridina.so

caridina: $(OBJECTS) caridina.o
	$(LD) $(OBJECTS) caridina.o $(LFLAGS) -o caridina

server/gpu/demo: $(OBJECTS) server/gpu/demo.o
	$(LD) $(OBJECTS) server/gpu/demo.o $(LFLAGS) -o server/gpu/demo

caridina.respack: $(RESOURCES) tool/respack
	tool/respack caridina.respack $(RESOURCES)

.c.o:
	$(CC) $(CFLAGS) -fPIC -fvisibility=internal -c -o $@ $<

.cpp.o:
	$(CXX) $(CFLAGS) -fPIC -fvisibility=hidden -c -o $@ $<

.PHONY: cloc docs clean check $(TESTS)

cloc:
	cloc --exclude-dir=docs .

docs:
	doxygen doxygen.config
	make -C docs/latex

clean:
	rm -f $(OBJECTS)
	rm -f $(RESOURCES)
	rm -f $(INTERMEDIATES)

check: $(TESTS)
