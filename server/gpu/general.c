#include "server/gpu/api.h"
#include "server/gpu/i_vlk.h"
#include "server/gpu/i.h"
#include "attributes.h"
#include "util/sync.h"
#include "server/gpu/mesh_vlk.h"

#include <pthread.h>

HIDDEN
void *gpu_worker_fun(void *in)
{
	struct gpu_cmd cmd;
	queue_shift(&gpu_queue, &cmd);
	while (cmd.kind != GPU_CMD_EXIT) {
		switch (cmd.kind) {
			case GPU_CMD_UPLOAD_MESH_RES:
//				logs("Running upload mesh", CRD_LOG_ALWAYS);
				gpu_run_upload_mesh_res(cmd.upload_mesh_res.path, cmd.upload_mesh_res.id);
				break;
			case GPU_CMD_DRAW:
//				logs("Running draw", CRD_LOG_ALWAYS);
				gpu_run_draw(cmd.draw.mesh, cmd.draw.xform);
				break;
			case GPU_CMD_SET_CAMERA:
				glm_mat4_copy(cmd.set_camera.view, vlk_view);
				glm_mat4_copy(cmd.set_camera.proj, vlk_proj);
				break;
			case GPU_CMD_FINISH:
//				logs("Running finish", CRD_LOG_ALWAYS);
				vlk_draw();
				sync_frame_end();
				break;
			case GPU_CMD_EXIT:
		}
		queue_shift(&gpu_queue, &cmd);
	}
	return in;
}

void gpu_init(void)
{
	vlk_init_context();
	vlk_init_cmd();
	vlk_make_swapchain();
	log_assert(0 == pthread_create(&gpu_thread, NULL, &gpu_worker_fun, NULL));
}

void gpu_exit(void)
{
	struct gpu_cmd cmd;
	cmd.kind = GPU_CMD_EXIT;
	queue_push(&gpu_queue, &cmd);
	sync_kill();
	log_assert(0 == pthread_join(gpu_thread, NULL));

	vkDeviceWaitIdle(vlk_device);
	vlk_clean_swapchain();
	vlk_clean_cmd();
	vlk_clean_context();
}

void gpu_draw(gpu_mesh_t id, mat4 xform)
{
	struct gpu_cmd cmd;
	cmd.kind = GPU_CMD_DRAW;
	cmd.draw.mesh = id;
	glm_mat4_copy(xform, cmd.draw.xform);
	queue_push(&gpu_queue, &cmd);
}

void gpu_finish(void)
{
	struct gpu_cmd cmd;
	cmd.kind = GPU_CMD_FINISH;
	queue_push(&gpu_queue, &cmd);
}

void gpu_set_camera(mat4 view, mat4 proj)
{
	struct gpu_cmd cmd;
	cmd.kind = GPU_CMD_SET_CAMERA;
	glm_mat4_copy(view, cmd.set_camera.view);
	glm_mat4_copy(proj, cmd.set_camera.proj);
	queue_push(&gpu_queue, &cmd);
}
