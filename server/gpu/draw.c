#include <caridina/control.h>
#include "server/gpu/i.h"
#include "server/gpu/i_vlk.h"
#include "server/gpu/mesh_vlk.h"

#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <GLFW/glfw3.h>

static uint32_t image;
// static const VkDeviceSize zero_ds = 0; // a zero you can point to

static VkBuffer inst_buf[FRAMES];
static VmaAllocation inst_vma[FRAMES];
static size_t inst_back[FRAMES];

void vlk_draw_clean(void)
{
	VLK_OR_DIE(vkWaitForFences(vlk_device, 1, &vlk_render_fence[vlk_frame%FRAMES], true, 1000000000));
	if (inst_buf[vlk_frame%FRAMES] != VK_NULL_HANDLE) {
		vmaDestroyBuffer(vlk_vma, inst_buf[vlk_frame%FRAMES], inst_vma[vlk_frame%FRAMES]);
	}
}

static void alloc_inst(void)
{
        VkBufferCreateInfo vb_info = {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size = sizeof(mat4)*inst_back[vlk_frame%FRAMES],
                .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT
        };
        VmaAllocationCreateInfo va_info = {
                .usage = VMA_MEMORY_USAGE_CPU_TO_GPU
        };
        VLK_OR_DIE(vmaCreateBuffer(vlk_vma, &vb_info, &va_info, &inst_buf[vlk_frame%FRAMES], &inst_vma[vlk_frame%FRAMES], NULL));
}

// must be idempotent
static void send_inst(void)
{
	if (gpu_draw_queue_len > inst_back[vlk_frame%FRAMES]) {
		inst_back[vlk_frame%FRAMES] += gpu_draw_queue_len;
		vlk_draw_clean();
		alloc_inst();
	} else if (inst_vma[vlk_frame%FRAMES] == NULL) {
		inst_back[vlk_frame%FRAMES] = 16;
		alloc_inst();
	}

	mat4 *data;
	vmaMapMemory(vlk_vma, inst_vma[vlk_frame%FRAMES], (void **) &data);
	for (size_t i = 0; i < gpu_draw_queue_len; i++) {
		glm_mat4_mul(vlk_view, gpu_draw_queue_xforms[i], data[i]);
//		glm_mat4_mul(data[i], vlk_proj, data[i]);
//		log_f(CRD_LOG_ALWAYS, "Instance xform " PF_MAT4, PV_MAT4(data[i]));
	}
	vmaUnmapMemory(vlk_vma, inst_vma[vlk_frame%FRAMES]);
}

static void record_call(VkCommandBuffer cb, size_t i)
{
	gpu_mesh_t mesh = gpu_draw_queue_meshes[i];
	struct static_mesh sm = gpu_meshes[mesh].statique;
//	log_f(CRD_LOG_ALWAYS, "Recording call %u with %u indices", (unsigned) i, (unsigned) sm.idx_count);
	VkBuffer bufs[2] = {
		sm.vert_buf,
		inst_buf[vlk_frame%FRAMES]
	};
	VkDeviceSize offs[2] = { 0, 0 };
	vkCmdBindVertexBuffers(cb, 0, 2, bufs, offs);
	vkCmdBindIndexBuffer(cb, sm.idx_buf, 0, sm.vert_count <= UINT16_MAX ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32);
	vkCmdDrawIndexed(cb, sm.idx_count, 1, 0, 0, i);
}

static void record(VkCommandBuffer cb)
{
	VLK_OR_DIE(vkResetCommandBuffer(cb, 0));

	VkCommandBufferBeginInfo buf_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
	};
	VLK_OR_DIE(vkBeginCommandBuffer(cb, &buf_info));

	VkClearValue clear[2] = {
		{
			.color = { { 0, 0, 0, 0 } }
		},
		{
			.depthStencil = { .depth = 1 }
		}
	};
	VkRenderPassBeginInfo pass_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
		.renderPass = vlk_pass[PASS_FINAL],
		.renderArea = {
			.offset = {
				.x = 0,
				.y = 0
			},
			.extent = vlk_window_size
		},
		.framebuffer = vlk_fbs[PASS_FINAL][image],
		.clearValueCount = 2,
		.pClearValues = clear
	};
	vkCmdBeginRenderPass(cb, &pass_info, VK_SUBPASS_CONTENTS_INLINE);

//	vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, vlk_pplns[vlk_ppln_mode][PPLN_TRIANGLE]);
//	vkCmdDraw(cb, 6, 1, 0, 0);

	vkCmdBindPipeline(cb, VK_PIPELINE_BIND_POINT_GRAPHICS, vlk_pplns[vlk_ppln_mode][PPLN_STATIC]);
	vkCmdPushConstants(cb, vlk_layout_push_persp, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(mat4), vlk_proj);
//	log_f(CRD_LOG_ALWAYS, "Pushing perspective " PF_MAT4, PV_MAT4(vlk_proj));
//	log_f(CRD_LOG_ALWAYS, "Recording %u draw calls", (unsigned) gpu_draw_queue_len);
	for (size_t i = 0; i < gpu_draw_queue_len; i++) {
		record_call(cb, i);
	}

	vkCmdEndRenderPass(cb);

	VLK_OR_DIE(vkEndCommandBuffer(cb));
}

static void resize(void)
{
	vkDeviceWaitIdle(vlk_device);
	vlk_clean_swapchain();
	vlk_make_swapchain();
	gpu_need_resize = false;
}

// must be idempotent
static void upload_camera(void)
{
	void *data;
	mat4 cam;
	glm_mat4_mul(vlk_proj, vlk_view, cam);
	vmaMapMemory(vlk_vma, vlk_view_vma, &data);
	memcpy(data, cam, sizeof(mat4));
	vmaUnmapMemory(vlk_vma, vlk_view_vma);
}

void vlk_draw(void)
{
	upload_camera();
	send_inst();

	// NB: at this point, we may or may not have waited for this fence
	VLK_OR_DIE(vkWaitForFences(vlk_device, 1, &vlk_render_fence[vlk_frame%FRAMES], true, 1000000000));
	VLK_OR_DIE(vkResetFences(vlk_device, 1, &vlk_render_fence[vlk_frame%FRAMES]));

	VkResult res = VK_ERROR_OUT_OF_DATE_KHR;
	while (res == VK_ERROR_OUT_OF_DATE_KHR) {
		if (gpu_need_resize) resize();
		res = vkAcquireNextImageKHR(vlk_device, vlk_swapchain, 1000000000, vlk_present_sema[vlk_frame%FRAMES], NULL, &image);
		if (res != VK_SUCCESS) {
			log_assert(res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR);
			gpu_need_resize = true;
		}
	}

	record(vlk_cmd_buf[vlk_frame%FRAMES]);

	VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	VkSubmitInfo submit = {
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		.pWaitDstStageMask = &wait_stage,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &vlk_present_sema[vlk_frame%FRAMES],
		.signalSemaphoreCount = 1,
		.pSignalSemaphores = &vlk_render_sema[vlk_frame%FRAMES],
		.commandBufferCount = 1,
		.pCommandBuffers = &vlk_cmd_buf[vlk_frame%FRAMES]
	};
	VLK_OR_DIE(vkQueueSubmit(vlk_queue_graphics, 1, &submit, vlk_render_fence[vlk_frame%FRAMES]));

	VkPresentInfoKHR present = {
		.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		.swapchainCount = 1,
		.pSwapchains = &vlk_swapchain,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = &vlk_render_sema[vlk_frame%FRAMES],
		.pImageIndices = &image
	};
	VLK_OR_DIE(vkQueuePresentKHR(vlk_queue_present, &present));

	vlk_frame++;
	gpu_draw_queue_len = 0;

	if (glfwWindowShouldClose(gpu_window)) crd_exit = true;
}
