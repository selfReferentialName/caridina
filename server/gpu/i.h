/** @file server/gpu/i.h
 * @brief Basic shared declarations for the gpu server.
 */
#ifndef SERVER_GPU_I_H
#define SERVER_GPU_I_H

#include "util/queue.h"
#include "server/gpu/api.h"

#include <stdbool.h>
#include <cglm/cglm.h>

extern bool gpu_need_resize; ///< Don't draw a frame because we need to resize.

/// What kind of command there is for the GPU.
enum gpu_cmd_kind {
	GPU_CMD_UPLOAD_MESH_RES, ///< Sent by gpu_upload_mesh_res.
	GPU_CMD_DRAW, ///< Sent by gpu_draw.
	GPU_CMD_SET_CAMERA, ///< Sent by gpu_set_camera.
	GPU_CMD_EXIT, ///< Kill the gpu server.
	GPU_CMD_FINISH ///< Sent by gpu_finish.
};

/// A command for the GPU
struct gpu_cmd {
	union {
		struct {
			const char *path; ///< Path to the resource.
			gpu_mesh_t id; ///< The mesh handle.
		} upload_mesh_res; ///< GPU_CMD_DRAW
		struct {
			mat4 xform; ///< The transform of the mesh.
			gpu_mesh_t mesh; ///< The mesh handle.
		} draw; ///< GPU_CMD_DRAW
		struct {
			mat4 view; ///< World space to view space.
			mat4 proj; ///< View space to unit cube.
		} set_camera; ///< GPU_SET_CAMERA
	};
	enum gpu_cmd_kind kind; ///< What kind of command is it?
};

extern struct queue gpu_queue; ///< Queue of commands.

extern pthread_t gpu_thread; ///< Thread ID of the gpu worker thread.

extern gpu_mesh_t *gpu_draw_queue_meshes; ///< Queue of meshes to draw.
extern mat4 *gpu_draw_queue_xforms; ///< Queue of transforms of meshes to draw.
extern size_t gpu_draw_queue_len; ///< Number of meshes to draw this frame.
extern size_t gpu_draw_queue_back; ///< Backing size of gpu_draw_queue_*

// command runners

void gpu_run_upload_mesh_res(const char *path, gpu_mesh_t id); ///< Runs GPU_CMD_UPLOAD_MESH_RES
void gpu_run_draw(gpu_mesh_t id, mat4 xform); ///< Runs GPU_CMD_DRAW

#endif
