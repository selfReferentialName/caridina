/** @file server/gpu/api.h
 * @brief GPU server public API.
 */
#ifndef SERVER_GPU_API_H
#define SERVER_GPU_API_H

#include <cglm/cglm.h>
#include <stdint.h>

#include <GLFW/glfw3.h>

/// Tracked to `crd_mesh_t`
typedef uint16_t gpu_mesh_t;

/// A vertex in a static mesh.
struct gpu_static_vert {
	vec3 pos; ///< Position.
	vec3 normal; ///< Normal vector (need not be unit).
	vec2 uv; ///< Texture coordinate in [0,1]x[0,1]
};

/// A static mesh
struct gpu_static_mesh {
	struct gpu_static_vert *verts; ///< The vertices themselves.
	void *idxs; ///< Vertex indices. uint16_t if vert_count <= UINT16_MAX and uint32_t otherwise
	const char *name; ///< Name for debugging purposes.
	uint32_t vert_count; ///< Size of the verts array.
	uint32_t idx_count; ///< Size of the idxs array.
};

/// Initialise the GPU server.
void gpu_init(void);

/// Exit the GPU server and wait for it to finish.
void gpu_exit(void);

/// Tell the GPU to finish the frame.
void gpu_finish(void);

/** @brief Upload a mesh in a resource to the GPU.
 *
 * Gets a handle back immediately, but only sends the command to actually
 * process and upload the mesh in a queue.
 * 
 * The path must live for the entire frame.
 *
 * @param path The path to the resource.
 */
gpu_mesh_t gpu_upload_mesh_res(const char *path);

/// Draw a mesh with a given model transform. Enqueues a command.
void gpu_draw(gpu_mesh_t mesh, mat4 xform);

/// Send the camera matrices.
void gpu_set_camera(mat4 view, mat4 proj);

extern GLFWwindow *gpu_window; ///< The window of the game.

#endif
