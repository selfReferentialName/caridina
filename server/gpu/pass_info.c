#include "server/gpu/i_vlk.h"

static VkAttachmentDescription attach[PASS_NUM_SUPER][MAX_FB_ATTACH] = {
	[PASS_FINAL] = {
		{ // color
			// need to fill out .format
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
		},
		{ // depth
			.format = VK_FORMAT_D32_SFLOAT,
			.samples = VK_SAMPLE_COUNT_1_BIT,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
			.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		}
	}
};

VkFormat vlk_fb_fmts[PASS_NUM_SUPER][MAX_FB_ATTACH] = {
	[PASS_FINAL] = {
		[1] = VK_FORMAT_D32_SFLOAT // depth TODO: don't assume this is best
	}
};

VkImageUsageFlags vlk_fb_usages[PASS_NUM_SUPER][MAX_FB_ATTACH] = {
	[PASS_FINAL] = {
		[1] = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
	}
};

VkImageAspectFlags vlk_fb_aspects[PASS_NUM_SUPER][MAX_FB_ATTACH] = {
	[PASS_FINAL] = {
		[0] = VK_IMAGE_ASPECT_COLOR_BIT,
		[1] = VK_IMAGE_ASPECT_DEPTH_BIT
	}
};

static VkAttachmentReference attach_ref[PASS_NUM_SUPER][MAX_FB_ATTACH] = {
	[PASS_FINAL] = {
		{ // color
			.attachment = 0,
			.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		},
		{ // depth
			.attachment = 1,
			.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		}
	}
};

static VkSubpassDescription subs[PASS_NUM] = {
	[PASS_SUB_FINAL] = {
		.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
		.colorAttachmentCount = 1,
		.pColorAttachments = attach_ref[PASS_FINAL],
		.pDepthStencilAttachment = &attach_ref[PASS_FINAL][1]
	}
};

#define MAX_DEPS 16

static VkSubpassDependency deps[PASS_NUM_SUPER][MAX_DEPS] = {
	[PASS_FINAL] = {
		[0] = {
			.srcSubpass = VK_SUBPASS_EXTERNAL,
			.dstSubpass = 0,
			.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
			.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
			.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
		}
	}
};

VkRenderPassCreateInfo vlk_pass_info[PASS_NUM_SUPER] = {
	[PASS_FINAL] = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.attachmentCount = 2,
		.pAttachments = attach[PASS_FINAL],
		.subpassCount = 1,
		.pSubpasses = &subs[PASS_SUB_FINAL],
		.dependencyCount = 1,
		.pDependencies = deps[PASS_FINAL]
	}
};

void vlk_pass_reformat(void)
{
	attach[PASS_FINAL][0].format = vlk_window_format.format;
	vlk_fb_fmts[PASS_FINAL][0] = vlk_window_format.format;
}
