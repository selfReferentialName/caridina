#include "server/gpu/i_vlk.h"
#include "server/resource/api.h"
#include "util/log.h"
#include "util/xmalloc.h"

#include <alloca.h>

void vlk_load_shaders(int count, const char *const *path, VkShaderModule *out)
{
	struct res_promise **res;
	if (count <= 256) {
		res = alloca(sizeof(struct res_promise *) * count);
	} else {
		res = xmalloc(sizeof(struct res_promise *) * count);
	}
	for (int i = 0; i < count; i++) {
		res[i] = res_open(path[i], 0);
	}

	for (int i = 0; i < count; i++) {
		res_await(res[i]);
		VkShaderModuleCreateInfo create = {
			.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
			.codeSize = res[i]->size,
			.pCode = (void *) res[i]->data
		};
		VLK_OR_DIE(vkCreateShaderModule(vlk_device, &create, NULL, &out[i]));
		log_f(CRD_LOG_VERY_VERBOSE, "Shader %s module %p", path[i], (void *) out[i]);
	}

	if (count > 256) {
		free(res);
	}
}
