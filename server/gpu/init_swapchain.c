#include "server/gpu/i_vlk.h"
#include "server/gpu/i.h"
#include "server/gpu/mesh_vlk.h"

#include <stdlib.h>

static VkSurfaceFormatKHR choose_format(void)
{
	uint32_t c = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(vlk_physical, vlk_surface, &c, NULL);
	VkSurfaceFormatKHR *v = alloca(sizeof(VkSurfaceFormatKHR) * c);
	vkGetPhysicalDeviceSurfaceFormatsKHR(vlk_physical, vlk_surface, &c, v);

	for (uint32_t i = 0; i < c; i++) {
		log_f(CRD_LOG_VERY_VERBOSE, "Surface format %d and %d", (int) v[i].format, (int) v[i].colorSpace);
		if (v[i].format == VK_FORMAT_B8G8R8A8_SRGB && v[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return v[i];
		}
	}

	logs("Didn't find desired format for the swapchain.", CRD_LOG_WARN);
	return v[0];
}

static VkPresentModeKHR choose_mode(void)
{
	uint32_t mode_c = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(vlk_physical, vlk_surface, &mode_c, NULL);
	VkPresentModeKHR *v = alloca(sizeof(VkPresentModeKHR) * mode_c);
	vkGetPhysicalDeviceSurfacePresentModesKHR(vlk_physical, vlk_surface, &mode_c, v);

	for (uint32_t i = 0; i < mode_c; i++) {
		log_f(CRD_LOG_VERY_VERBOSE, "Present mode %d", (int) v[i]);
	}

	return VK_PRESENT_MODE_FIFO_KHR; // TODO: consider switching to VK_PRESENT_MODE_MAILBOX_KHR or something
}

#define clamp(x, i, a) ((x) < (i) ? (i) : (x) > (a) ? (a) : (x))

static VkExtent2D choose_extent(void)
{
	VkExtent2D r;
	if (vlk_surface_capabilities.currentExtent.width != UINT32_MAX) {
		logs("Have pre-selected surface extent.", CRD_LOG_VERBOSE);
		r = vlk_surface_capabilities.currentExtent;
	} else {
		logs("Figuring out surface extent manually.", CRD_LOG_VERBOSE);
		int width, height;
		glfwGetFramebufferSize(gpu_window, &width, &height);
		VkExtent2D i = vlk_surface_capabilities.minImageExtent;
		VkExtent2D a = vlk_surface_capabilities.maxImageExtent;
		r.width = clamp((uint32_t) width, i.width, a.width);
		r.height = clamp((uint32_t) height, i.height, a.height);
	}
	log_f(CRD_LOG_VERBOSE, "Surface size %dx%d.", (int) r.width, (int) r.height);
	return r;
}

static void make_swapchain(void)
{
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vlk_physical, vlk_surface, &vlk_surface_capabilities);

	vlk_window_format = choose_format();
	vlk_window_mode = choose_mode();
	vlk_window_size = choose_extent();

	vlk_swapchain_size = vlk_surface_capabilities.minImageCount + 1;
	if (vlk_surface_capabilities.maxImageCount && vlk_swapchain_size > vlk_surface_capabilities.maxImageCount) {
		vlk_swapchain_size = vlk_surface_capabilities.maxImageCount;
	}
	log_f(CRD_LOG_VERBOSE, "Swapchain size %d with min size %d and max size %d", vlk_swapchain_size, vlk_surface_capabilities.minImageCount, vlk_surface_capabilities.maxImageCount);

	VkSwapchainCreateInfoKHR create = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = vlk_surface,
		.minImageCount = vlk_swapchain_size,
		.imageFormat = vlk_window_format.format,
		.imageColorSpace = vlk_window_format.colorSpace,
		.imageExtent = vlk_window_size,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.preTransform = vlk_surface_capabilities.currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = vlk_window_mode,
		.clipped = VK_TRUE
	};
	if (vlk_queue_graphics_family == vlk_queue_present_family) {
		create.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	} else {
		uint32_t family_indices[] = {
			vlk_queue_graphics_family,
			vlk_queue_present_family
		};
		create.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		create.queueFamilyIndexCount = 2;
		create.pQueueFamilyIndices = family_indices;
	}

	VLK_OR_DIE(vkCreateSwapchainKHR(vlk_device, &create, NULL, &vlk_swapchain));

	vkGetSwapchainImagesKHR(vlk_device, vlk_swapchain, &vlk_swapchain_size, NULL);
}

static void clean_swapchain(void)
{
	vkDestroySwapchainKHR(vlk_device, vlk_swapchain, NULL);
}

static void alloc_images(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		vlk_fb_images[i] = mem_arena_alloc(vlk_swapchain_arena, vlk_swapchain_size, VkImage *);
		vlk_fb_image_vmas[i] = mem_arena_alloc(vlk_swapchain_arena, vlk_swapchain_size, VmaAllocation *);
		vlk_fb_views[i] = mem_arena_alloc(vlk_swapchain_arena, vlk_swapchain_size, VkImageView *);
		for (uint32_t j = 0; j < vlk_swapchain_size; j++) {
			vlk_fb_images[i][j] = mem_arena_alloc(vlk_swapchain_arena, vlk_fb_counts[i], VkImage);
			vlk_fb_image_vmas[i][j] = mem_arena_alloc(vlk_swapchain_arena, vlk_fb_counts[i], VmaAllocation);
			vlk_fb_views[i][j] = mem_arena_alloc(vlk_swapchain_arena, vlk_fb_counts[i], VkImageView);
		}
	}
}

static void make_images(void)
{
	VkImage swapchain_images[vlk_swapchain_size];
	vkGetSwapchainImagesKHR(vlk_device, vlk_swapchain, &vlk_swapchain_size, swapchain_images);
	for (uint32_t i = 0; i < vlk_swapchain_size; i++) {
		vlk_fb_images[PASS_FINAL][i][0] = swapchain_images[i];
		for (int h = 0; h < PASS_NUM_SUPER; h++) {
			for (int j = 0; j < vlk_fb_counts[h]; j++) {
				if (h == PASS_FINAL && j == 0) continue;
				VkImageCreateInfo img_info = {
					.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
					.imageType = VK_IMAGE_TYPE_2D,
					.format = vlk_fb_fmts[h][j],
					.extent = {
						.width = vlk_window_size.width,
						.height = vlk_window_size.height,
						.depth = 1
					},
					.mipLevels = 1,
					.arrayLayers = 1,
					.samples = VK_SAMPLE_COUNT_1_BIT,
					.tiling = VK_IMAGE_TILING_OPTIMAL,
					.usage = vlk_fb_usages[h][j]
				};
				VmaAllocationCreateInfo vma_info = {
					.usage = VMA_MEMORY_USAGE_GPU_ONLY
				};
				// TODO: consider making into an arena
				VLK_OR_DIE(vmaCreateImage(vlk_vma, &img_info, &vma_info, &vlk_fb_images[h][i][j], &vlk_fb_image_vmas[h][i][j], NULL));
			}
		}
	}
}

static void clean_images(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		for (uint32_t j = 0; j < vlk_swapchain_size; j++) {
			for (int k = 0; k < vlk_fb_counts[i]; k++) {
				if (i == PASS_FINAL && k == 0) continue;
				vmaDestroyImage(vlk_vma, vlk_fb_images[i][j][k], vlk_fb_image_vmas[i][j][k]);
			}
		}
	}
}

static void make_views(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		for (uint32_t k = 0; k < vlk_swapchain_size; k++) {
			for (int j = 0; j < vlk_fb_counts[i]; j++) {
				VkImageViewCreateInfo create = {
					.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
					.image = vlk_fb_images[i][k][j],
					.viewType = VK_IMAGE_VIEW_TYPE_2D,
					.format = vlk_fb_fmts[i][j],
					.components = {
						.r = VK_COMPONENT_SWIZZLE_IDENTITY,
						.g = VK_COMPONENT_SWIZZLE_IDENTITY,
						.b = VK_COMPONENT_SWIZZLE_IDENTITY,
						.a = VK_COMPONENT_SWIZZLE_IDENTITY
					},
					.subresourceRange = {
						.aspectMask = vlk_fb_aspects[i][j],
						.baseMipLevel = 0,
						.levelCount = 1,
						.baseArrayLayer = 0,
						.layerCount = 1
					}
				};
				VLK_OR_DIE(vkCreateImageView(vlk_device, &create, NULL, &vlk_fb_views[i][k][j]));
			}
		}
	}
}

static void clean_views(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		for (uint32_t k = 0; k < vlk_swapchain_size; k++) {
			for (int j = 0; j < vlk_fb_counts[i]; j++) {
				vkDestroyImageView(vlk_device, vlk_fb_views[i][k][j], NULL);
			}
		}
	}
}

static void make_pass(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		VLK_OR_DIE(vkCreateRenderPass(vlk_device, &vlk_pass_info[i], NULL, &vlk_pass[i]));
	}
}

static void clean_pass(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		vkDestroyRenderPass(vlk_device, vlk_pass[i], NULL);
	}
}

static void make_fbs(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		vlk_fbs[i] = mem_arena_alloc(vlk_swapchain_arena, vlk_swapchain_size, VkFramebuffer);
		for (uint32_t j = 0; j < vlk_swapchain_size; j++) {
			VkFramebufferCreateInfo create = {
				.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
				.renderPass = vlk_pass[i],
				.attachmentCount = vlk_fb_counts[i],
				.width = vlk_window_size.width,
				.height = vlk_window_size.height,
				.layers = 1,
				.pAttachments = vlk_fb_views[i][j]
			};
			VLK_OR_DIE(vkCreateFramebuffer(vlk_device, &create, NULL, &vlk_fbs[i][j]));
		}
	}
}

static void clean_fbs(void)
{
	for (int i = 0; i < PASS_NUM_SUPER; i++) {
		for (uint32_t j = 0; j < vlk_swapchain_size; j++) {
			vkDestroyFramebuffer(vlk_device, vlk_fbs[i][j], NULL);
		}
	}
}

static void make_pplns(void)
{
	struct VkPipelineVertexInputStateCreateInfo vi_empty = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		.vertexBindingDescriptionCount = 0,
		.vertexAttributeDescriptionCount = 0
	};
	for (int m = PLMD_NORMAL; m < PLMD_NUM; m++) {
		VkPolygonMode pm = m == PLMD_WIREFRAME ? VK_POLYGON_MODE_LINE : VK_POLYGON_MODE_FILL;
		log_f(CRD_LOG_VERY_VERBOSE, "Creating vlk_pplns[%d][PPLN_TRIANGLE]", m);
		vlk_pplns[m][PPLN_TRIANGLE] = vlk_make_pipeline(
				vlk_vert_sm, vlk_frag_sm, &vi_empty,
				VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
				pm, VK_CULL_MODE_NONE,
				VK_COMPARE_OP_ALWAYS,
				PASS_SUB_FINAL, vlk_layout_empty);
		log_f(CRD_LOG_VERY_VERBOSE, "Creating vlk_pplns[%d][PPLN_STATIC]", m);
		vlk_pplns[m][PPLN_STATIC] = vlk_make_pipeline(
				vlk_static_vert_sm, vlk_nml_frag_sm, &gpu_static_vert_info,
				VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
				pm, VK_CULL_MODE_NONE,
				VK_COMPARE_OP_LESS_OR_EQUAL,
				PASS_SUB_FINAL, vlk_layout_push_persp);
	}
}

static void clean_pplns(void)
{
	for (int i = 0; i < PLMD_NUM; i++) {
		for (int j = 0; j < PPLN_NUM; j++) {
			vkDestroyPipeline(vlk_device, vlk_pplns[i][j], NULL);
		}
	}
}

void vlk_make_swapchain(void)
{
	if (vlk_swapchain_arena) {
		mem_arena_reset(vlk_swapchain_arena);
	} else {
		vlk_swapchain_arena = mem_arena_new(64);
	}
	make_swapchain();
	vlk_pass_reformat();
	alloc_images();
	make_images();
	make_views();
	make_pass();
	make_fbs();
	make_pplns();
}

void vlk_clean_swapchain(void)
{
	clean_pplns();
	clean_fbs();
	clean_pass();
	clean_views();
	clean_images();
	clean_swapchain();
}
