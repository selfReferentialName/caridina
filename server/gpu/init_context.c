#include "server/gpu/i_vlk.h"
#include "server/gpu/i.h"
#include "config/gpu.h"
#include "attributes.h"
#include "server/gpu/mesh_vlk.h"
#include "util/xmalloc.h"

#include <string.h>
#include <execinfo.h>

#define NDEBUG

static GLFWmonitor *monitor;

HIDDEN void vlk_glfw_resize_callback(UNUSED GLFWwindow *window, UNUSED int width, UNUSED int height)
{
	logs("Window was resized.", CRD_LOG_VERBOSE);
	gpu_need_resize = true;
}

static void init_window(void)
{
	logs("Creating window.", CRD_LOG_EVENTS);
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	monitor = NULL;
	if (cfg_window_fullscreen) {
		monitor = glfwGetPrimaryMonitor();
		int width, height;
		glfwGetMonitorWorkarea(monitor, NULL, NULL, &width, &height);
		cfg_window_width = width;
		cfg_window_height = height;
	}
	gpu_window = glfwCreateWindow(cfg_window_width, cfg_window_height, cfg_window_name, monitor, NULL);
	log_assert(gpu_window);
	glfwSetFramebufferSizeCallback(gpu_window, &vlk_glfw_resize_callback);
}

static void clean_window(void)
{
	logs("Destroying window.", CRD_LOG_EVENTS);
	glfwDestroyWindow(gpu_window);
	glfwTerminate();
}

static void init_extensions(void)
{
	logs("Figuring out needed Vulkan extensions.", CRD_LOG_EVENTS);
	uint32_t glfw_c = 0;
	const char **glfw_v = glfwGetRequiredInstanceExtensions(&glfw_c);

	size_t extra_c = 1;
	const char *extra_v[] = {
		VK_EXT_DEBUG_UTILS_EXTENSION_NAME
	};

	vlk_extension_count = glfw_c + extra_c;
	vlk_extensions = xmalloc(sizeof(const char *) * vlk_extension_count);
	memcpy(vlk_extensions, glfw_v, sizeof(const char *) * glfw_c);
	memcpy(vlk_extensions + glfw_c, extra_v, sizeof(const char *) * extra_c);
}

static void clean_extensions(void)
{
	logs("Freeing needed Vulkan extension array.", CRD_LOG_EVENTS);
	free(vlk_extensions);
}

static uint32_t validation_count = 1;
static const char *const validation_layers[] = {
	"VK_LAYER_KHRONOS_validation"
};

#define BACKTRACE_LEN 256

__attribute__((visibility("hidden")))
VKAPI_ATTR VkBool32 VKAPI_CALL log_vulkan(
		UNUSED VkDebugUtilsMessageSeverityFlagBitsEXT severity, // TODO: map to log_level
		UNUSED VkDebugUtilsMessageTypeFlagsEXT type,
		const VkDebugUtilsMessengerCallbackDataEXT *data,
		UNUSED void *user_data)
{
	logs(data->pMessage, CRD_LOG_DEFAULT);
	void *buffer[BACKTRACE_LEN];
	int len = backtrace(buffer, BACKTRACE_LEN);
	backtrace_symbols_fd(buffer, len, 0);
	return VK_FALSE;
}

static VkDebugUtilsMessengerCreateInfoEXT debug_info = {
	.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
	// TODO: probably less ifdef NDEBUG
	.messageSeverity = /* VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | */ VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
	.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
	.pfnUserCallback = &log_vulkan
};

static void init_instance(void)
{
	logs("Creating Vulkan instance", CRD_LOG_EVENTS);
	VkApplicationInfo app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = cfg_window_name,
		.applicationVersion = VK_MAKE_VERSION(1, 0, 0),
		.pEngineName = "Caridina",
		.engineVersion = VK_MAKE_VERSION(0, 1, 0),
		.apiVersion = VK_API_VERSION_1_0
	};
	VkInstanceCreateInfo create = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &app_info,
		.enabledExtensionCount = vlk_extension_count,
		.ppEnabledExtensionNames = vlk_extensions,
#ifndef NDEBUG
		.enabledLayerCount = validation_count,
		.ppEnabledLayerNames = validation_layers,
#else
		.enabledLayerCount = 0,
#endif
		.pNext = &debug_info
	};
	VLK_OR_DIE(vkCreateInstance(&create, NULL, &vlk_instance));
#ifndef NDEBUG
	logs("Using vulkan validation layers", CRD_LOG_DEFAULT);
#endif
}

static void clean_instance(void)
{
	logs("Destroying Vulkan instance", CRD_LOG_EVENTS);
	vkDestroyInstance(vlk_instance, NULL);
}

static void get_extension_functions(void)
{
	logs("Getting Vulkan extension functions", CRD_LOG_EVENTS);
	vkCreateDebugUtilsMessenger = (void *) vkGetInstanceProcAddr(vlk_instance, "vkCreateDebugUtilsMessengerEXT");
	vkDestroyDebugUtilsMessenger = (void *) vkGetInstanceProcAddr(vlk_instance, "vkDestroyDebugUtilsMessengerEXT");
}

static void init_debug(void)
{
	logs("Creating Vulkan debug messenger", CRD_LOG_EVENTS);
	VLK_OR_DIE(vkCreateDebugUtilsMessenger(vlk_instance, &debug_info, NULL, &vlk_debug_messenger));
}

static void clean_debug(void)
{
	logs("Destroying Vulkan debug messenger", CRD_LOG_EVENTS);
	vkDestroyDebugUtilsMessenger(vlk_instance, vlk_debug_messenger, NULL);
}

static void init_surface(void)
{
	logs("Creating Vulkan surface for main window", CRD_LOG_EVENTS);
	if (glfwCreateWindowSurface(vlk_instance, gpu_window, NULL, &vlk_surface)) {
		const char *desc;
		int code = glfwGetError(&desc);
		log_f(CRD_LOG_ALWAYS, "Can't make window surface (code %d): %s", code, desc);
		exit(1);
	}
}

static void clean_surface(void)
{
	logs("Destroying Vulkan surface for main window", CRD_LOG_EVENTS);
	vkDestroySurfaceKHR(vlk_instance, vlk_surface, NULL);
}

static uint32_t need_device_extension_count = 1;
static const char *need_device_extensions[1] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

// negative score is unsuitable
static int score_device(VkPhysicalDevice device)
{
	int score = 100000;

	VkPhysicalDeviceProperties prop;
	vkGetPhysicalDeviceProperties(device, &prop);

	// Device suitability

	int64_t graphics_family = -1, present_family = -1; // negative for not found
	uint32_t family_c = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &family_c, NULL);
	VkQueueFamilyProperties *family_v = alloca(sizeof(VkQueueFamilyProperties) * family_c);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &family_c, family_v);
	for (uint32_t i = 0; i < family_c; i++) {
		if (family_v[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			vlk_queue_graphics_family = graphics_family = i;
		}
		VkBool32 can_present = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(device, i, vlk_surface, &can_present);
		if (can_present) {
			vlk_queue_present_family = present_family = i;
		}
		log_f(CRD_LOG_VERY_VERBOSE, "Flags %d need %d present %d", (int) family_v[i].queueFlags, (int) VK_QUEUE_GRAPHICS_BIT, (int) can_present);
	}
	if (graphics_family < 0 || present_family < 0) {
		log_f(CRD_LOG_VERBOSE, "Device %s has no queues of %d", prop.deviceName, (int) family_c);
		return -1;
	}

	uint32_t ext_c = 0;
	vkEnumerateDeviceExtensionProperties(device, NULL, &ext_c, NULL);
	VkExtensionProperties *ext_v = alloca(sizeof(VkExtensionProperties) * ext_c);
	vkEnumerateDeviceExtensionProperties(device, NULL, &ext_c, ext_v);
	for (uint32_t i = 0; i < need_device_extension_count; i++) { // yes this is O(mn) when it can be O(max(m, n))
		bool found = false; // m is most likely just 1
		for (uint32_t j = 0; j < ext_c; j++) { // so I don't care
			if (strcmp(need_device_extensions[i], ext_v[j].extensionName) == 0) {
				found = true;
				break;
			}
		}
		if (!found) {
			log_f(CRD_LOG_VERY_VERBOSE, "Device %s lacks extension %s", prop.deviceName, need_device_extensions[i]);
			return -1;
		}
	}

	uint32_t format_c = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, vlk_surface, &format_c, NULL);
	if (format_c == 0) {
		log_f(CRD_LOG_VERBOSE, "Device %s has no surface formats", prop.deviceName);
		return -1;
	}

	uint32_t mode_c = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, vlk_surface, &mode_c, NULL);
	if (format_c == 0) {
		log_f(CRD_LOG_VERBOSE, "Device %s has no present modes", prop.deviceName);
		return -1;
	}

	VkPhysicalDeviceFeatures ftrs;
	vkGetPhysicalDeviceFeatures(device, &ftrs);

	log_assert(ftrs.fillModeNonSolid == VK_TRUE);

	// Device scoring

	int score_discrete = -100;
	if (cfg_gpu_prefer_discrete) score_discrete = 100;
	if (prop.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) score += score_discrete;

	const int score_favorite = 1000;
	if (strcmp(prop.deviceName, cfg_gpu_favorite) == 0) score += score_favorite;

	const int score_cpu = -500;
	if (prop.deviceType == VK_PHYSICAL_DEVICE_TYPE_CPU) score += score_cpu;

	log_f(CRD_LOG_EVENTS, "Device %s has score %d", prop.deviceName, score);
	return score;
}

static void init_physical(void)
{
	logs("Choosing GPU for Vulkan", CRD_LOG_EVENTS);
	uint32_t device_c = 0;
	vkEnumeratePhysicalDevices(vlk_instance, &device_c, NULL);
	log_assert(device_c != 0);

	VkPhysicalDevice *device_v = alloca(sizeof(VkPhysicalDevice) * device_c);
	vkEnumeratePhysicalDevices(vlk_instance, &device_c, device_v);

	uint32_t best_graphics = 0, best_present = 0;
	int best_score = -1;
	bool have_gpu = false;
	for (int i = 0; i < device_c; i++) {
		int score = score_device(device_v[i]);
		if (score > best_score) {
			vlk_physical = device_v[i];
			best_graphics = vlk_queue_graphics_family;
			best_present = vlk_queue_present_family;
			have_gpu = true;
			best_score = score;
		}
	}

	log_assert(have_gpu);

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vlk_physical, vlk_surface, &vlk_surface_capabilities);
	vlk_queue_graphics_family = best_graphics;
	vlk_queue_present_family = best_present;

	VkPhysicalDeviceProperties prop;
	vkGetPhysicalDeviceProperties(vlk_physical, &prop);
	log_f(CRD_LOG_DEFAULT, "Using GPU named %s", prop.deviceName);
}

static void init_logical(void)
{
	logs("Connecting to GPU with Vulkan", CRD_LOG_EVENTS);
	int queue_c;
	uint32_t queue_fv[2] = {vlk_queue_graphics_family, vlk_queue_present_family};
	if (vlk_queue_graphics_family == vlk_queue_present_family) {
		queue_c = 1; // queue_v past first index ignored, so can use same queue_fv
	} else {
		queue_c = 2;
	}
	float queue_priority = 1.0;
	VkDeviceQueueCreateInfo *queue_infos = alloca(sizeof(VkDeviceQueueCreateInfo) * queue_c);
	for (int i = 0; i < queue_c; i++) {
		queue_infos[i] = (VkDeviceQueueCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
			.queueFamilyIndex = queue_fv[i],
			.queueCount = 1,
			.pQueuePriorities = &queue_priority
		};
	}

	VkPhysicalDeviceFeatures features = {
		.fillModeNonSolid = VK_TRUE
	};

	VkDeviceCreateInfo create = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.queueCreateInfoCount = queue_c,
		.pQueueCreateInfos = queue_infos,
		.pEnabledFeatures = &features,
		.enabledExtensionCount = need_device_extension_count,
		.ppEnabledExtensionNames = need_device_extensions,
#ifndef NDEBUG
		.enabledLayerCount = validation_count,
		.ppEnabledLayerNames = validation_layers
#endif
	};

	VLK_OR_DIE(vkCreateDevice(vlk_physical, &create, NULL, &vlk_device));

	vkGetDeviceQueue(vlk_device, vlk_queue_graphics_family, 0, &vlk_queue_graphics);
	vkGetDeviceQueue(vlk_device, vlk_queue_present_family, 0, &vlk_queue_present);
	vkGetDeviceQueue(vlk_device, vlk_queue_graphics_family, 0, &vlk_queue_transfer);

	VmaAllocatorCreateInfo vma_info = {
		.physicalDevice = vlk_physical,
		.device = vlk_device,
		.instance = vlk_instance
	};
	vmaCreateAllocator(&vma_info, &vlk_vma);
}

static void clean_logical(void)
{
	logs("Destroying Vulkan connection to GPU", CRD_LOG_EVENTS);
	vmaDestroyAllocator(vlk_vma);
	vkDestroyDevice(vlk_device, NULL);
}

static void init_sync(void)
{
	logs("Creating GPU synchronisation objects", CRD_LOG_EVENTS);

	for (int i = 0; i < FRAMES; i++) {
		VkFenceCreateInfo fence_info = {
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
			.flags = VK_FENCE_CREATE_SIGNALED_BIT
		};
		VLK_OR_DIE(vkCreateFence(vlk_device, &fence_info, NULL, &vlk_render_fence[i]));

		VkSemaphoreCreateInfo sema_info = {
			.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
		};
		VLK_OR_DIE(vkCreateSemaphore(vlk_device, &sema_info, NULL, &vlk_present_sema[i]));
		VLK_OR_DIE(vkCreateSemaphore(vlk_device, &sema_info, NULL, &vlk_render_sema[i]));
	}
}

static void clean_sync(void)
{
	logs("Destroying GPU synchronisation objects", CRD_LOG_EVENTS);
	for (int i = 0; i < FRAMES; i++) {
		vkDestroySemaphore(vlk_device, vlk_render_sema[i], NULL);
		vkDestroySemaphore(vlk_device, vlk_present_sema[i], NULL);
		vkDestroyFence(vlk_device, vlk_render_fence[i], NULL);
	}
}

static void init_shaders(void)
{
	logs("Creating shader modules", CRD_LOG_EVENTS);
	VkShaderModule mods[4];
	const char *const paths[4] = {
		"crd/shader/triangle.frag",
		"crd/shader/triangle.vert",
		"crd/shader/static.vert",
		"crd/shader/normal.frag"
	};
	vlk_load_shaders(4, paths, mods);
	vlk_frag_sm = mods[0];
	vlk_vert_sm = mods[1];
	vlk_static_vert_sm = mods[2];
	vlk_nml_frag_sm = mods[3];
}

static void clean_shaders(void)
{
	logs("Destroying shader modules", CRD_LOG_EVENTS);
	vkDestroyShaderModule(vlk_device, vlk_frag_sm, NULL);
	vkDestroyShaderModule(vlk_device, vlk_vert_sm, NULL);
	vkDestroyShaderModule(vlk_device, vlk_static_vert_sm, NULL);
	vkDestroyShaderModule(vlk_device, vlk_nml_frag_sm, NULL);
}

static void init_layouts(void)
{
	logs("Creating graphics pipeline layouts", CRD_LOG_EVENTS);

	VkPipelineLayoutCreateInfo create_empty = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.setLayoutCount = 0,
		.pushConstantRangeCount = 0,
	};
	VLK_OR_DIE(vkCreatePipelineLayout(vlk_device, &create_empty, NULL, &vlk_layout_empty));

	VkPushConstantRange push_persp_range = {
		.offset = 0,
		.size = sizeof(mat4),
		.stageFlags = VK_SHADER_STAGE_VERTEX_BIT
	};
	VkPipelineLayoutCreateInfo create_push_persp = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.setLayoutCount = 0,
		.pushConstantRangeCount = 1,
		.pPushConstantRanges = &push_persp_range
	};
	VLK_OR_DIE(vkCreatePipelineLayout(vlk_device, &create_push_persp, NULL, &vlk_layout_push_persp));
}

static void clean_layouts(void)
{
	logs("Destroying graphics pipeline layouts", CRD_LOG_EVENTS);
	vkDestroyPipelineLayout(vlk_device, vlk_layout_empty, NULL);
	vkDestroyPipelineLayout(vlk_device, vlk_layout_push_persp, NULL);
}

static void init_camera(void)
{
	glm_mat4_identity(vlk_view);
	glm_mat4_identity(vlk_proj);
	vlk_proj[1][1] *= -1;

	VkBufferCreateInfo vb_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = sizeof(mat4),
		.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT
	};
	VmaAllocationCreateInfo va_info = {
		.usage = VMA_MEMORY_USAGE_CPU_TO_GPU
	};
	VLK_OR_DIE(vmaCreateBuffer(vlk_vma, &vb_info, &va_info, &vlk_view_buf, &vlk_view_vma, NULL));
}

static void clean_camera(void)
{
	vmaDestroyBuffer(vlk_vma, vlk_view_buf, vlk_view_vma);
}

static void clean_meshes(void)
{
	for (gpu_mesh_t i = 0; i < sizeof(gpu_meshes)/sizeof(union mesh); i++) {
		if (gpu_meshes[i].statique.vert_buf != VK_NULL_HANDLE) {
			vmaDestroyBuffer(vlk_vma, gpu_meshes[i].statique.vert_buf, gpu_meshes[i].statique.vert_vma);
			vmaDestroyBuffer(vlk_vma, gpu_meshes[i].statique.idx_buf, gpu_meshes[i].statique.idx_vma);
		}
	}
}

void vlk_init_context(void)
{
	gpu_queue.size = sizeof(struct gpu_cmd);
	queue_init(&gpu_queue);
	init_window();
	init_extensions();
	init_instance();
	get_extension_functions();
	init_debug();
	init_surface();
	init_physical();
	init_logical();
	init_sync();
	init_shaders();
	init_layouts();
	init_camera();
}

void vlk_clean_context(void)
{
	clean_camera();
	vlk_draw_clean();
	vlk_frame++;
	vlk_draw_clean();
	clean_meshes();
	clean_layouts();
	clean_shaders();
	clean_sync();
	clean_logical();
	clean_surface();
	clean_debug();
	clean_instance();
	clean_extensions();
	clean_window();
}
