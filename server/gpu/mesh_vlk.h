/** @file server/gpu/mesh_vlk.h
 * @brief Internal Vulkan related functions related to meshes.
 */
#ifndef SERVER_GPU_MESH_VLK_H
#define SERVER_GPU_MESH_VLK_H

#include "server/gpu/api.h"
#include "server/gpu/i_vlk.h"

/*
/// A vertex in a static mesh.
struct gpu_static_vert {
        vec3 pos; ///< Position.
        vec3 normal; ///< Normal vector (need not be unit).
        vec2 uv; ///< Texture coordinate in [0,1]x[0,1]
};

/// A static mesh
struct gpu_static_mesh {
        gpu_static_vert *verts; ///< The vertices themselves.
        void *idxs; ///< Vertex indices. uint16_t if vert_count <= UINT16_MAX and uint32_t otherwise
	const char *name;
        uint32_t vert_count; ///< Size of the verts array.
        uint32_t idx_count; ///< Size of the idxs array.
};
*/

/// A static mesh uploaded to the gpu.
struct static_mesh {
	VkBuffer vert_buf; ///< The vertex buffer.
	VkBuffer idx_buf; ///< The index buffer.
	VmaAllocation vert_vma; ///< The allocation of vert_buf.
	VmaAllocation idx_vma; ///< The allocation of idx_buf.
	uint32_t vert_count; ///< Copied from the gpu_static_mesh.
	uint32_t idx_count; ///< Copied from the gpu_static_mesh.
};

/** @brief Send a static mesh to the gpu.
 *
 * @param mesh The mesh to upload.
 * 
 * @return mesh but on the GPU.
 */
struct static_mesh gpu_static_mesh_to_gpu(struct gpu_static_mesh mesh);

/// TODO: move to public API somehow
struct static_mesh gpu_static_res_mesh_to_gpu(const char *path);

extern VkPipelineVertexInputStateCreateInfo gpu_static_vert_info; ///< Binding and attribute description fo static meshes.

/// A mesh on the GPU.
union mesh {
	struct static_mesh statique; ///< A static mesh.
};

extern union mesh gpu_meshes[(1 << 8) << sizeof(gpu_mesh_t)]; ///< The mesh data uploaded to the gpu.

#endif
