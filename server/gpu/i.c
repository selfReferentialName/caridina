#include "server/gpu/i.h"
#include "server/gpu/i_vlk.h"

bool gpu_need_resize;

GLFWwindow *gpu_window;
VkInstance vlk_instance;
VkSurfaceKHR vlk_surface;
VkPhysicalDevice vlk_physical;
VkDevice vlk_device;

VkQueue vlk_queue_graphics;
VkQueue vlk_queue_present;
VkQueue vlk_queue_transfer;
uint32_t vlk_queue_graphics_family;
uint32_t vlk_queue_present_family;
uint32_t vlk_queue_transfer_family;

size_t vlk_extension_count;
const char **vlk_extensions;
VkSurfaceCapabilitiesKHR vlk_surface_capabilities;

PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessenger;
PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessenger;
VkDebugUtilsMessengerEXT vlk_debug_messenger;

VkExtent2D vlk_window_size;
VkSurfaceFormatKHR vlk_window_format;
VkPresentModeKHR vlk_window_mode;

VkSwapchainKHR vlk_swapchain;
uint32_t vlk_swapchain_size;
struct mem_arena *vlk_swapchain_arena = NULL;

const int vlk_fb_counts[PASS_NUM_SUPER] = {2};
VkImage **vlk_fb_images[PASS_NUM_SUPER];
VmaAllocation **vlk_fb_image_vmas[PASS_NUM_SUPER];
VkImageView **vlk_fb_views[PASS_NUM_SUPER];
VkFramebuffer *vlk_fbs[PASS_NUM];

VkCommandPool vlk_cmd_pool[FRAMES];
VkCommandBuffer vlk_cmd_buf[FRAMES];

VkRenderPass vlk_pass[PASS_NUM_SUPER];

VkSemaphore vlk_present_sema[FRAMES];
VkSemaphore vlk_render_sema[FRAMES];
VkFence vlk_render_fence[FRAMES];

uint64_t vlk_frame = 0;

const enum vlk_pass vlk_major[PASS_NUM] = {
	[PASS_SUB_FINAL] = PASS_FINAL
};

const uint32_t vlk_subpass[PASS_NUM] = {
	[PASS_SUB_FINAL] = 0
};

VkShaderModule vlk_frag_sm;
VkShaderModule vlk_vert_sm;
VkShaderModule vlk_nml_frag_sm;
VkShaderModule vlk_static_vert_sm;

VkPipelineLayout vlk_layout_empty;
VkPipelineLayout vlk_layout_push_persp;

enum vlk_ppln_mode vlk_ppln_mode = PLMD_NORMAL;

VkPipeline vlk_pplns[PLMD_NUM][PPLN_NUM];

VmaAllocator vlk_vma;

mat4 vlk_view, vlk_proj;

VkBuffer vlk_view_buf;
VmaAllocation vlk_view_vma;

struct queue gpu_queue;

pthread_t gpu_thread;

gpu_mesh_t *gpu_draw_queue_meshes;
mat4 *gpu_draw_queue_xforms;
size_t gpu_draw_queue_len;
size_t gpu_draw_queue_back;
