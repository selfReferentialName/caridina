/** @file server/gpu/i_vlk.h
 * @brief Vulkan specific but general internal GPU server stuff
 */
#ifndef SERVER_GPU_I_VLK_H
#define SERVER_GPU_I_VLK_H

#include <vk_mem_alloc.h>

#include "util/log.h"
#include "util/mem/arena.h"

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include <stdlib.h>
#include <cglm/cglm.h>

/// The number of frames in flight.
#define FRAMES 2

/// Macro to assert a vulkan call was successful.
#define VLK_OR_DIE(x) \
	do { \
		VkResult err = x; \
		if (err != VK_SUCCESS && err != VK_SUBOPTIMAL_KHR) { \
			logs("Vulkan call at  failed: " #x, CRD_LOG_ALWAYS); \
			log_f(CRD_LOG_ALWAYS, "Offending VkResult: %lld", (long long) err); \
			exit(1); \
		} \
	} while (0)

extern VkInstance vlk_instance; ///< The vulkan instance.
extern VkSurfaceKHR vlk_surface; ///< The surface for the window.
extern VkPhysicalDevice vlk_physical; ///< The physical device.
extern VkDevice vlk_device; ///< The logical device.

extern VkQueue vlk_queue_graphics; ///< The graphics queue.
extern VkQueue vlk_queue_present; ///< The present queue.
extern VkQueue vlk_queue_transfer; ///< The transfer queue.
extern uint32_t vlk_queue_graphics_family; ///< The graphics queue index.
extern uint32_t vlk_queue_present_family; ///< The present queue index.
extern uint32_t vlk_queue_transfer_family; ///< The transfer queue index.

extern size_t vlk_extension_count; ///< The number of loaded extensions.
extern const char **vlk_extensions; ///< The names of loaded extensions.
extern VkSurfaceCapabilitiesKHR vlk_surface_capabilities; ///< Window surface capabilities.

extern PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessenger;
extern PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessenger;
extern VkDebugUtilsMessengerEXT vlk_debug_messenger; ///< The debug messenger.

extern VkSemaphore vlk_present_sema[FRAMES]; ///< Signalled when the swapchain image is ready to be presented to.
extern VkSemaphore vlk_render_sema[FRAMES]; ///< Signalled when the frame is rendered.
extern VkFence vlk_render_fence[FRAMES]; ///< Signalled when the frame is rendered.

extern VkShaderModule vlk_frag_sm; ///< Triangle fragment shader module.
extern VkShaderModule vlk_vert_sm; ///< Triangle vertex shader module.
extern VkShaderModule vlk_nml_frag_sm; ///< Fragment shader to only write the normal G-Buffer.
extern VkShaderModule vlk_static_vert_sm; ///< Vertex shader for a static mesh.

extern VmaAllocator vlk_vma; ///< Connection to VMA

void vlk_init_context(void); ///< Initialise the window, instance, surface, and device (everything before the swapchain).
void vlk_clean_context(void); ///< Undo vlk_init_context.

extern VkExtent2D vlk_window_size; ///< The extent of the main window.
extern VkSurfaceFormatKHR vlk_window_format; ///< The image format of the main window.
extern VkPresentModeKHR vlk_window_mode; ///< The mode of the window's swap chain.

extern VkSwapchainKHR vlk_swapchain; ///< The swapchain.
extern uint32_t vlk_swapchain_size; ///< The length of the swapchain.
extern struct mem_arena *vlk_swapchain_arena; ///< A memory arena reset on swapchain recreation.

/// The various renderpasses and subpasses available. All renderpasses before subpasses.
enum vlk_pass {
	PASS_FINAL, ///< The renderpass from which we output stuff.
	PASS_SUB_FINAL, ///< The subpass from which we output stuff.
	PASS_NUM ///< The number of passes.
};

/// The number of renderpasses (not subpasses)
#define PASS_NUM_SUPER PASS_SUB_FINAL

/// The maximum number of attachments to a renderpass
#define MAX_FB_ATTACH 16

extern const int vlk_fb_counts[PASS_NUM_SUPER]; ///< The number of attachments for each pass. [pass]
extern VkFormat vlk_fb_fmts[PASS_NUM_SUPER][MAX_FB_ATTACH]; ///< The formats of the attachments. [pass][attachment]
extern VkImageUsageFlags vlk_fb_usages[PASS_NUM_SUPER][MAX_FB_ATTACH]; ///< The usages of the attachments. [pass][attachment]
extern VkImageAspectFlags vlk_fb_aspects[PASS_NUM_SUPER][MAX_FB_ATTACH]; ///< The aspects of the attachments. [pass][attachment]
extern VkImage **vlk_fb_images[PASS_NUM_SUPER]; ///< The targeted rendering images. [pass][swapchain image][attachment]
extern VmaAllocation **vlk_fb_image_vmas[PASS_NUM_SUPER]; ///< Allocation corresponding to vlk_fb_images.
extern VkImageView **vlk_fb_views[PASS_NUM_SUPER]; ///< The views for each image. [pass][swapchain image][attachment]
extern VkFramebuffer *vlk_fbs[PASS_NUM]; ///< The framebuffers. [pass][swapchain image]

extern VkRenderPassCreateInfo vlk_pass_info[PASS_NUM_SUPER]; ///< The info for each renderpass.
void vlk_pass_reformat(void); ///< Called by vlk_make_swapchain to put information about the swapchain into vlk_pass_info.
extern VkRenderPass vlk_pass[PASS_NUM_SUPER]; ///< The renderpasess.
extern const enum vlk_pass vlk_major[PASS_NUM]; ///< Lookup table to get the major pass of a subpass.
extern const uint32_t vlk_subpass[PASS_NUM]; ///< Lookup table for subpass indices.

extern VkPipelineLayout vlk_layout_empty; ///< A basic pipeline layout with nothing in it.
extern VkPipelineLayout vlk_layout_push_persp; ///< A pipeline layout with a single mat4 push constant.

/** @brief Make a pipeline
 *
 * A pipeline is a big fancy object, so this is a big function with
 * a bunch of inputs.
 *
 * @param vert The vertex shader.
 * @param frag The fragment shader.
 * @param vert_info The inputs to the vertex shader.
 * @param topology How the vertices are layed out.
 * @param poly_mode How the triangles are displayed (e.g. to allow for wireframe)
 * @param cull_mode How culling is done (always clockwise is front)
 * @param depth_mode What comparison for the depth test (VK_COMPARE_OP_ALWAYS to disable)
 * @param pass The subpass to use.
 * @param layout The uniform input information (AKA a VkPipelineLayout)
 */
VkPipeline vlk_make_pipeline(VkShaderModule vert, VkShaderModule frag,
		VkPipelineVertexInputStateCreateInfo *vert_info,
		VkPrimitiveTopology topology,
		VkPolygonMode poly_mode, VkCullModeFlagBits cull_mode,
		VkCompareOp depth_mode,
		enum vlk_pass pass,
		VkPipelineLayout layout);

/// All pipelines at given settings named and everything.
enum vlk_ppln {
	PPLN_TRIANGLE, ///< Test triangle
	PPLN_STATIC, ///< Static mesh
	PPLN_NUM ///< How many pipelines there are
};

/// Settings for the pipelines.
enum vlk_ppln_mode {
	PLMD_NORMAL, ///< Render everything normally.
	PLMD_WIREFRAME, ///< Don't fill in the polygons.
	PLMD_NUM ///< How many pipeline modes there are
};
extern enum vlk_ppln_mode vlk_ppln_mode; ///< What current mode are we rendering in.

extern VkPipeline vlk_pplns[PLMD_NUM][PPLN_NUM]; ///< The pipelines.

void vlk_make_swapchain(void); ///< Initialise/recreate the swapchain and all of its images and views.
void vlk_clean_swapchain(void); ///< Destroy the swapchain and all of its images.

extern VkCommandPool vlk_cmd_pool[FRAMES]; ///< The command pool. TODO frames in flight.
extern VkCommandBuffer vlk_cmd_buf[FRAMES]; ///< The command buffer. TODO frames in flight.

void vlk_init_cmd(void); ///< Initialise command buffers and such.
void vlk_clean_cmd(void); ///< Clean command buffers and such.

void vlk_draw_clean(void); ///< Clean buffers for drawing

extern uint64_t vlk_frame; ///< Incremented each frame.

void vlk_draw(void); ///< Do the draw part of the main loop.

/** @brief Load a bunch of shaders.
 *
 * @param count The number of shaders to load.
 * @param paths The resource paths of the shaders.
 * @param out Where to write the shaders.
 */
void vlk_load_shaders(int count, const char *const *path, VkShaderModule *out); ///< Load a shader from a resource.

/// How to blend stuff in a pipeline
enum vlk_blend {
	VLK_BLEND_NONE, ///< blendEnable = VK_FALSE
};

extern mat4 vlk_view, vlk_proj; ///< View and projection matrices.

extern VkBuffer vlk_view_buf; ///< Vertex buffer with vlk_view.
extern VmaAllocation vlk_view_vma; ///< Allocation of vlk_view_buf.

#endif
