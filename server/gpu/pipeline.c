#include "server/gpu/i_vlk.h"
#include "server/gpu/mesh_vlk.h"

static VkVertexInputBindingDescription static_binds[2] = {
	{ // vertex info
		.binding = 0,
		.stride = sizeof(struct gpu_static_vert),
		.inputRate = VK_VERTEX_INPUT_RATE_VERTEX
	},
	{ // instance info
		.binding = 1,
		.stride = sizeof(mat4),
		.inputRate = VK_VERTEX_INPUT_RATE_INSTANCE
	}
};
static VkVertexInputAttributeDescription static_attrs[7] = {
	{ // iPos
		.location = 0,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32_SFLOAT,
		.offset = offsetof(struct gpu_static_vert, pos)
	},
	{ // iNml
		.location = 1,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32_SFLOAT,
		.offset = offsetof(struct gpu_static_vert, normal)
	},
	{ // iUv
		.location = 2,
		.binding = 0,
		.format = VK_FORMAT_R32G32_SFLOAT,
		.offset = offsetof(struct gpu_static_vert, uv)
	},
	{ // iMv
		.location = 3,
		.binding = 1,
		.format = VK_FORMAT_R32G32B32A32_SFLOAT,
		.offset = 0
	}, {
		.location= 4,
		.binding = 1,
		.format = VK_FORMAT_R32G32B32A32_SFLOAT,
		.offset = sizeof(vec4)
	}, {
		.location= 5,
		.binding = 1,
		.format = VK_FORMAT_R32G32B32A32_SFLOAT,
		.offset = 2*sizeof(vec4)
	}, {
		.location= 6,
		.binding = 1,
		.format = VK_FORMAT_R32G32B32A32_SFLOAT,
		.offset = 3*sizeof(vec4)
	}
};
VkPipelineVertexInputStateCreateInfo gpu_static_vert_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
	.vertexBindingDescriptionCount = 2,
	.pVertexBindingDescriptions = static_binds,
	.vertexAttributeDescriptionCount = 7,
	.pVertexAttributeDescriptions = static_attrs
};

VkPipeline vlk_make_pipeline(VkShaderModule vert, VkShaderModule frag,
                VkPipelineVertexInputStateCreateInfo *vert_info,
                VkPrimitiveTopology topology,
                VkPolygonMode poly_mode, VkCullModeFlagBits cull_mode,
                VkCompareOp depth_mode,
                enum vlk_pass pass,
		VkPipelineLayout layout)
{
	VkPipelineShaderStageCreateInfo stages[2] = {
		{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.stage = VK_SHADER_STAGE_VERTEX_BIT,
			.module = vert,
			.pName = "main",
		},
		{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
			.module = frag,
			.pName = "main",
		},
	};
	VkPipelineInputAssemblyStateCreateInfo assembly = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		.topology = topology,
		.primitiveRestartEnable = (topology <= VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST) ? VK_FALSE : VK_TRUE
	};
	VkViewport view = {
		.width = vlk_window_size.width,
		.height = vlk_window_size.height,
		.minDepth = 0,
		.maxDepth = 1
	};
	VkRect2D scissor = {
		.offset = {0, 0},
		.extent = vlk_window_size
	};
	VkPipelineViewportStateCreateInfo view_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.viewportCount = 1,
		.pViewports = &view,
		.scissorCount = 1,
		.pScissors = &scissor
	};
	VkPipelineRasterizationStateCreateInfo raster = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		.depthClampEnable = VK_FALSE,
		.rasterizerDiscardEnable = VK_FALSE,
		.polygonMode = poly_mode,
		.cullMode = cull_mode,
		.frontFace = VK_FRONT_FACE_CLOCKWISE,
		.depthBiasEnable = VK_FALSE,
		.lineWidth = 1
	};
	VkPipelineMultisampleStateCreateInfo msaa = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
		.sampleShadingEnable = VK_FALSE,
		.minSampleShading = 1
	};
	VkPipelineDepthStencilStateCreateInfo depth = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		.depthTestEnable = depth_mode == VK_COMPARE_OP_ALWAYS ? VK_FALSE : VK_TRUE,
		.depthWriteEnable = depth_mode == VK_COMPARE_OP_ALWAYS ? VK_FALSE : VK_TRUE,
		.depthCompareOp = depth_mode
	};
	VkPipelineColorBlendAttachmentState win_blend = {
		.blendEnable = VK_FALSE,
		.colorWriteMask = 0xf
	};
	VkPipelineColorBlendStateCreateInfo blend = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.attachmentCount = 1,
		.pAttachments = &win_blend,
		.logicOpEnable = VK_FALSE
	};

	VkGraphicsPipelineCreateInfo create = {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.stageCount = 2,
		.pStages = stages,
		.pVertexInputState = vert_info,
		.pInputAssemblyState = &assembly,
		.pViewportState = &view_info,
		.pRasterizationState = &raster,
		.pMultisampleState = &msaa,
		.pDepthStencilState = &depth,
		.pColorBlendState = &blend,
		.layout = layout,
		.renderPass = vlk_pass[vlk_major[pass]],
		.subpass = vlk_subpass[pass]
	};
	VkPipeline r;
	VLK_OR_DIE(vkCreateGraphicsPipelines(vlk_device, VK_NULL_HANDLE, 1, &create, NULL, &r));
	return r;
}
