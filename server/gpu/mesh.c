#include "server/gpu/mesh_vlk.h"
#include "server/resource/api.h"
#include "file/mesh.h"
#include "server/gpu/i.h"
#include "util/xmalloc.h"

#include <string.h>
#include <pthread.h>

static pthread_mutex_t new_mesh_lock = PTHREAD_MUTEX_INITIALIZER;
static gpu_mesh_t num_meshes;
static gpu_mesh_t free_meshes[(1 << 8) << sizeof(gpu_mesh_t)];
static gpu_mesh_t num_free_meshes;

union mesh gpu_meshes[(1 << 8) << sizeof(gpu_mesh_t)];

static gpu_mesh_t new_handle(void)
{
	pthread_mutex_lock(&new_mesh_lock);
	gpu_mesh_t r;
	if (num_free_meshes != 0) {
		r = free_meshes[num_free_meshes - 1];
		num_free_meshes++;
	} else {
		r = num_meshes;
		num_meshes++;
	}
	pthread_mutex_unlock(&new_mesh_lock);
	return r;
}

/*
static void drop_handle(gpu_mesh_t mesh)
{
	pthread_mutex_lock(&new_mesh_lock);
	free_meshes[num_free_meshes] = mesh;
	num_free_meshes++;
	pthread_mutex_unlock(&new_mesh_lock);
}
*/

// ironically, this actually converts a gpu_static_mesh to a static_mesh
// naming really is the hardest thing in programming
struct static_mesh gpu_static_mesh_to_gpu(struct gpu_static_mesh mesh)
{
	log_f(CRD_LOG_VERY_VERBOSE, "Mesh has %u verts and %u idxs", mesh.vert_count, mesh.idx_count);

	VkBufferCreateInfo vb_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = sizeof(struct gpu_static_vert)*mesh.vert_count,
		.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT
	};
	VmaAllocationCreateInfo va_info = {
//		.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT |
//			VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
//		.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE // better but might not be working right now
		.usage = VMA_MEMORY_USAGE_CPU_TO_GPU
	};
	struct static_mesh r = {
		.vert_count = mesh.vert_count,
		.idx_count = mesh.idx_count
	};
	VLK_OR_DIE(vmaCreateBuffer(vlk_vma, &vb_info, &va_info, &r.vert_buf, &r.vert_vma, NULL));

	VkBufferCreateInfo ib_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = (mesh.vert_count <= UINT16_MAX ? sizeof(uint16_t) : sizeof(uint32_t)) * mesh.idx_count,
		.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT
	};
	VmaAllocationCreateInfo ia_info = {
//		.flags = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT |
//			VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
//		.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE
		.usage = VMA_MEMORY_USAGE_CPU_TO_GPU
	};
	VLK_OR_DIE(vmaCreateBuffer(vlk_vma, &ib_info, &ia_info, &r.idx_buf, &r.idx_vma, NULL));

	void *data;
	vmaMapMemory(vlk_vma, r.vert_vma, &data);
	memcpy(data, mesh.verts, vb_info.size);
	vmaUnmapMemory(vlk_vma, r.vert_vma);

	vmaMapMemory(vlk_vma, r.idx_vma, &data);
	memcpy(data, mesh.idxs, ib_info.size);
	vmaUnmapMemory(vlk_vma, r.idx_vma);

	return r;
}

struct static_mesh gpu_static_res_mesh_to_gpu(const char *path)
{
	struct res_promise *res = res_open(path, 0);
	res_await(res);
	struct file_mesh_header *hdr = (struct file_mesh_header *) res->data;
	void *vi = (void *) &hdr[1];
	size_t vi_size = (hdr->vert_count <= UINT16_MAX ? sizeof(uint16_t) : sizeof(uint32_t)) * hdr->vi_count;
	void *verts = (void *) &((char *) &hdr[1])[vi_size];

	struct gpu_static_mesh mesh = {
		.verts = xmalloc(sizeof(struct gpu_static_vert) * hdr->vert_count),
		.idxs = xmalloc(vi_size),
		.name = path,
		.vert_count = hdr->vert_count,
		.idx_count = hdr->vi_count
	};
	log_assert(sizeof(struct gpu_static_vert) == sizeof(struct file_mesh_vert));
	memcpy(mesh.verts, verts, sizeof(struct gpu_static_vert) * hdr->vert_count); // TODO: change if struct file_mesh_vert changes
	memcpy(mesh.idxs, vi, vi_size);
	res_close(res);

	struct static_mesh r = gpu_static_mesh_to_gpu(mesh);
	free(mesh.verts);
	free(mesh.idxs);
	return r;
}

void gpu_run_upload_mesh_res(const char *path, gpu_mesh_t id)
{
	log_f(CRD_LOG_VERBOSE, "Uploading mesh %u in resource path %s to GPU", (unsigned) id, path);
	gpu_meshes[id].statique = gpu_static_res_mesh_to_gpu(path);
}

gpu_mesh_t gpu_upload_mesh_res(const char *path)
{
	log_f(CRD_LOG_ALWAYS, "Requesting mesh in %s be uploaded to the GPU", path);
	gpu_mesh_t id = new_handle();
	struct gpu_cmd cmd = {
		.kind = GPU_CMD_UPLOAD_MESH_RES,
		.upload_mesh_res = {
			.path = path,
			.id = id
		}
	};
	queue_push(&gpu_queue, &cmd);
	return id;
}

void gpu_run_draw(gpu_mesh_t id, mat4 xform)
{
	if (gpu_draw_queue_len >= gpu_draw_queue_back) {
		gpu_draw_queue_back *= 2;
		if (gpu_draw_queue_len >= gpu_draw_queue_back) gpu_draw_queue_back = gpu_draw_queue_len + 1;
		gpu_draw_queue_meshes = realloc(gpu_draw_queue_meshes, gpu_draw_queue_back*sizeof(gpu_mesh_t));
		gpu_draw_queue_xforms = realloc(gpu_draw_queue_xforms, gpu_draw_queue_back*sizeof(mat4));
	}
	gpu_draw_queue_meshes[gpu_draw_queue_len] = id;
	glm_mat4_copy(xform, gpu_draw_queue_xforms[gpu_draw_queue_len]);
	gpu_draw_queue_len++;
}
