#include "i_vlk.h"

void vlk_init_cmd(void)
{
	logs("Creating command pool", CRD_LOG_EVENTS);
	VkCommandPoolCreateInfo pool_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.queueFamilyIndex = vlk_queue_graphics_family,
		.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
	};
	for (int i = 0; i < FRAMES; i++) {
		vlk_cmd_pool[i] = VK_NULL_HANDLE;
		VLK_OR_DIE(vkCreateCommandPool(vlk_device, &pool_info, NULL, &vlk_cmd_pool[i]));
		VkCommandBufferAllocateInfo buf_info = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			.commandPool = vlk_cmd_pool[i],
			.commandBufferCount = 1,
			.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY
		};
		VLK_OR_DIE(vkAllocateCommandBuffers(vlk_device, &buf_info, &vlk_cmd_buf[i]));
	}
}

void vlk_clean_cmd(void)
{
	logs("Destroying command pool", CRD_LOG_EVENTS);
	for (int i = 0; i < FRAMES; i++) {
		vkDestroyCommandPool(vlk_device, vlk_cmd_pool[i], NULL);
	}
}
