#include "server/gpu/i_vlk.h"
#include "config/file.h"
#include "server/resource/api.h"
#include "attributes.h"
#include "server/gpu/mesh_vlk.h"
#include "util/sync.h"

#include <unistd.h>
#include <time.h>

static mat4 rot_left;
static mat4 rot_right;

HIDDEN
void got_key(GLFWwindow *win, int k, UNUSED int sc, int a, UNUSED int m)
{
	if (a == GLFW_RELEASE) return;
	if (k == GLFW_KEY_SPACE) {
		vlk_ppln_mode++;
		vlk_ppln_mode = vlk_ppln_mode % PLMD_NUM;
		log_f(CRD_LOG_DEFAULT, "You've switched to mode %d", vlk_ppln_mode);
	} else if (k == GLFW_KEY_PAGE_UP) {
		vlk_view[3][2] += 0.1;
	} else if (k == GLFW_KEY_PAGE_DOWN) {
		vlk_view[3][2] -= 0.1;
	} else if (k == GLFW_KEY_DOWN) {
		vlk_view[3][1] += 0.1;
	} else if (k == GLFW_KEY_UP) {
		vlk_view[3][1] -= 0.1;
	} else if (k == GLFW_KEY_RIGHT) {
		vlk_view[3][0] += 0.1;
	} else if (k == GLFW_KEY_LEFT) {
		vlk_view[3][0] -= 0.1;
	} else if (k == GLFW_KEY_ESCAPE) {
		glfwSetWindowShouldClose(win, true);
	} else if (k == GLFW_KEY_Q) {
		glm_mat4_mul(vlk_view, rot_left, vlk_view);
	} else if (k == GLFW_KEY_E) {
		glm_mat4_mul(vlk_view, rot_right, vlk_view);
	} else if (k == GLFW_KEY_S) {
		glm_mat4_scale(vlk_view, 0.9);
		vlk_view[3][3] = 1;
	} else if (k == GLFW_KEY_W) {
		glm_mat4_scale(vlk_view, 1.1);
		vlk_view[3][3] = 1;
	} else if (k == GLFW_KEY_ENTER) {
		glm_perspective(vlk_window_size.height / (float) vlk_window_size.width, 0.8, 0.1, 10, vlk_proj);
		vlk_proj[1][1] *= -1;
	} else if (k == GLFW_KEY_BACKSPACE) {
		glm_mat4_identity(vlk_proj);
		vlk_proj[1][1] *= -1;
	}
}

int main(int argc, char **argv)
{
	config_init(argc, argv);
	res_init();
	gpu_init();
	gpu_mesh_t mesh = gpu_upload_mesh_res("crd/meshes/uv-teapot.mesh");
	glfwSetKeyCallback(vlk_window, got_key);

	vec3 left = {0, 0.1, 0};
	glm_euler(left, rot_left);
	glm_mat4_inv(rot_left, rot_right);

	time_t start = time(NULL);
	while (!glfwWindowShouldClose(vlk_window)) {
		mat4 xform;
		glm_mat4_mul(vlk_view, vlk_proj, xform);
		gpu_draw(mesh, xform);
		gpu_finish();
		glfwPollEvents();
		sync_frame_end();
	}
	gpu_exit();

	time_t end = time(NULL);
	double time = difftime(end, start);
	log_f(CRD_LOG_DEFAULT, "Drew %llu frames in %f seconds at %f FPS", (long long unsigned) vlk_frame, time, vlk_frame / time);
}
