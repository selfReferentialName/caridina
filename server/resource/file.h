/** @file server/resource/file.h
 * @brief File format for resource packs.
 */
#ifndef SERVER_RESOURCE_FILE_H
#define SERVER_RESOURCE_FILE_H

#include <stdint.h>

#define RES_PACK_NAME_SIZE (128 - 2*sizeof(uint32_t))

/// The header of a resource pack.
struct res_pack_header {
	union {
		char s[4];
		uint32_t i;
	} magic; ///< Magic number "crd0"
	uint32_t file_count; ///< Number of contained files.
	uint32_t chunk_count; ///< Number of contained chunks.
	uint32_t checksum; ///< Sum of magic.i + file_count + chunk_count
};

/// The information for a file.
struct res_pack_file {
	struct res_pack_file_base {
		uint32_t size; ///< Total number of bytes in the file.
		uint32_t chunk_count; ///< Total number of chunks in the file.
		char name[RES_PACK_NAME_SIZE]; ///< The null terminated name of the file.
	} base;
	uint32_t chunk_id[]; ///< Chunk ID array (size is chunk_count)
};

/// A chunk.
struct res_pack_chunk {
	uint32_t size; ///< The number of bytes in the chunk (excluding the size field).
	char data[]; ///< The actual data of the chunk.
};

#endif
