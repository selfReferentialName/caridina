/** @file server/resource/api.h
 * @brief Resource server API.
 */
#ifndef SERVER_RESOURCE_API_H
#define SERVER_RESOURCE_API_H

#include <stdint.h>
#include <stdatomic.h>
#include <stddef.h>

/// The data for a resource which may not be fully loaded.
struct res_promise {
	char *data; ///< A pointer to the data for the resource.
	size_t size; ///< The total size of the resource when fully loaded.
	atomic_size_t backing; ///< How much of the resource is actually loaded.
};

void res_init(void); ///< Call before using any resource functions.
void res_clean(void); ///< Call before exiting.

/** @brief Close a resource and free its promise.
 *
 * Frees up all data for a resource.
 * Make sure to call when done with a resource to avoid leaking both
 * data memory and res_promises.
 *
 * @param res The resource to close.
 */
void res_close(struct res_promise *res);

/** @brief Open a resource and start reading.
 *
 * Searches for the resource path, opens the resource into a res_promise,
 * and starts reading the data for the resource.
 *
 * Returns NULL on an errror.
 *
 * @param path The resource path.
 * @param stride The size of chunks to read this resource incrementally or 0 for full file.
 *
 * @return A promise for the resource or NULL on errror.
 */
struct res_promise *res_open(const char *path, size_t stride);

/** @brief Wait until the resource is done reading.
 *
 * Blocks until a resource promise's backing equals its size.
 *
 * @param res The resource to wait for.
 *
 * @see res_back_to
 */
void res_await(struct res_promise *res);

/** @brief Wait until the file is backed to a certain point.
 *
 * Blocks until a resource promise's backing equals the desired
 * backing.
 *
 * @param res The resource to wait for.
 * @param backing The desired backing to wait for.
 *
 * @see res_await
 */
void res_back_to(struct res_promise *res, size_t backing);

#endif
