#include "server/resource/file.h"

#include "server/resource/api.h"
#include "config/general.h"
#include "util/mem/immortal.h"
#include "util/mem/arena.h"
#include "util/mem/pool.h"
#include "util/log.h"
#include "util/cstr.h"
#include "util/xmalloc.h"
#include "attributes.h"
#include "util/hash.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <aio.h>
#include <unistd.h>
#include <dirent.h>
#include <alloca.h>
#include <sys/stat.h>
#include <fcntl.h>

static struct mem_pool *promise_pool;

static int *pack_fds; // File descriptors for packs so we can close them later.
static int pack_count; // Size of pack_fds
static off_t **pack_chunks;

// A "file" which is actually a resource
struct res_file {
	struct res_file *next; // next valid "file" which can back this resource
	union {
		char *path; // absolute path if is_file
		struct {
			int id;
			off_t hdr;
		} pack_part; // resource pack and resource within if !is_file
	};
	bool is_file;
};

static struct hash_table_vlk res_table = {
	.valsize = sizeof(struct res_file)
};

static void xread(int fd, void *data, size_t size)
{
	log_assert(read(fd, data, size) == size);
}

static void add_resource(struct mem_arena *arena, const char *name, struct res_file res)
{
	hash_table_vlk_set(&res_table, name, strlen(name), &res);
}

// First pass through the pack, make a directory tree in an arena.
static void index_pack(struct mem_arena *arena, int fd)
{
	struct res_pack_header hdr;
	log_assert(read(fd, &hdr, sizeof(struct res_pack_header)) == sizeof(struct res_pack_header));
	log_assert(hdr.magic.i == 0x30647263); // "crd0" in little endean
	log_assert(hdr.checksum == hdr.magic.i + hdr.file_count + hdr.chunk_count);

	for (uint32_t i = 0; i < hdr.file_count; i++) {
		off_t pos = lseek(fd, 0, SEEK_CUR);
		log_assert(pos != -1);
		struct res_pack_file_base file;
		log_assert(read(fd, &file, sizeof(struct res_pack_file_base)) == sizeof(struct res_pack_file_base));
		add_resource(arena, file.name, (struct res_file) {
			.pack_part = {
				.id = pack_count,
				.hdr = pos
			},
			.is_file = false
		});
		log_assert(lseek(fd, sizeof(uint32_t) * file.chunk_count, SEEK_CUR) != -1);
	}

	pack_chunks[pack_count] = mem_imrtl_alloc(hdr.chunk_count, off_t);
	for (uint32_t i = 0; i < hdr.chunk_count; i++) {
		pack_chunks[pack_count][i] = lseek(fd, 0, SEEK_CUR);
		log_assert(pack_chunks[pack_count][i] != -1);
		uint32_t size; 
		log_assert(read(fd, &size, sizeof(uint32_t)) == sizeof(uint32_t));
		log_assert(lseek(fd, size, SEEK_CUR) != -1);
	}
}

static void index_dir(struct mem_arena *arena, const char *base, const char *path)
{
	DIR *dir = opendir(path);
	log_assert(dir);

	size_t path_len = strlen(path);
	size_t npath_back = ((path_len + 63) / 64 + 1) * 64;
	char *npath = alloca(npath_back);
	strcpy(npath, path);
	npath[path_len] = '/';

	size_t base_len = strlen(base);
	size_t nbase_back = ((base_len + 63) / 64 + 1) * 64;
	char *nbase = alloca(nbase_back);
	strcpy(nbase, base);
	if (base_len) nbase[base_len] = '/'; // initially, base is empty so we can't put a /

	for (struct dirent *entry = readdir(dir); entry; entry = readdir(dir)) {
		size_t dname_len = strlen(entry->d_name);
		if (path_len + dname_len + 2 > npath_back) {
			npath_back += dname_len;
			npath = alloca(npath_back);
			strcpy(npath, path);
			npath[path_len] = '/';
		}

		if (base_len + dname_len + 2 > nbase_back) {
			nbase_back += dname_len;
			nbase = alloca(nbase_back);
			strcpy(nbase, base);
			if (base_len) nbase[base_len] = '/';
		}
		if (base_len) {
			strcpy(nbase + base_len + 1, entry->d_name);
		} else {
			strcpy(nbase + base_len, entry->d_name);
		}

		strcpy(npath + path_len + 1, entry->d_name);
		struct stat stats;
		log_assert(lstat(npath, &stats) == 0);

		switch (stats.st_mode & S_IFMT) {
			case S_IFREG:
				{
					struct res_file res = {
						.is_file = true,
						.path = mem_imrtl_malloc(strlen(npath) + 1)
					};
					strcpy(res.path, npath);
					add_resource(arena, nbase, res);
				}
				break;

			case S_IFDIR:
				index_dir(arena, nbase, npath);
				break;

			default:
				log_f(CRD_LOG_WARN, "Ignoring oddly typed file %s when indexing directory %s",
						npath,
						path);
		}
	}

	closedir(dir);
}

void res_init(void)
{
	pack_fds = mem_imrtl_alloc(cfg_res_mods_len + cfg_res_game_len + cfg_res_engine_len, int);
	pack_chunks = mem_imrtl_alloc(cfg_res_mods_len + cfg_res_game_len + cfg_res_engine_len, off_t *);
	pack_count = 0;

	struct mem_arena *arena = mem_arena_new(2 * 1024 * 1024); // I'll give the arena 2MiB. As a treat.

	// resources are checked in LIFO precedence,
	// but stuff is ordered from first to last
	// hence, we index stuff backwards

	for (ssize_t i = cfg_res_engine_len - 1; i >= 0; i--) {
		int fd = open(cfg_res_engine[i], O_RDONLY);
		log_assert(fd != -1);
		pack_fds[pack_count] = fd;
		index_pack(arena, fd);
		pack_count++;
	}

	for (ssize_t i = cfg_res_game_len - 1; i >= 0; i--) {
		int fd = open(cfg_res_game[i], O_RDONLY);
		log_assert(fd != -1);
		pack_fds[pack_count] = fd;
		index_pack(arena, fd);
		pack_count++;
	}

	for (ssize_t i = cfg_res_mods_len - 1; i >= 0; i--) {
		struct stat stats;
		log_assert(stat(cfg_res_mods[i], &stats) == 0);
		if (S_ISDIR(stats.st_mode)) {
			index_dir(arena, "", cfg_res_mods[i]);
		} else {
			int fd = open(cfg_res_mods[i], O_RDONLY);
			log_assert(fd != -1);
			pack_fds[pack_count] = fd;
			index_pack(arena, fd);
			pack_count++;
		}
	}

	mem_arena_destroy(arena);

	promise_pool = mem_pool_new(sizeof(struct res_promise));
}

void res_clean(void)
{
	mem_pool_destroy(promise_pool);

	for (int i = 0; i < pack_count; i++) {
		close(pack_fds[i]);
		free(pack_chunks[i]);
	}

	free(pack_fds);
	free(pack_chunks);
}

static struct res_file get_res(const char *path)
{
	struct res_file r;
	hash_table_vlk_get(&res_table, path, strlen(path), &r);
	return r;
}

// TODO: asynchronous I/O
struct res_promise *res_open(const char *path, UNUSED size_t stride)
{
	struct res_promise *r = mem_pool_alloc(promise_pool);
	struct res_file f0 = get_res(path);
	struct res_file *f = &f0;

	// from indexed
	while (f) {
		if (f->is_file) {

			log_f(CRD_LOG_VERBOSE, "Reading resource %s from file %s", path, f->path);
			int fd = open(f->path, O_RDONLY);
			if (fd == -1) {
				f = f->next;
				continue;
			}
			struct stat stats;
			log_assert(fstat(fd, &stats) == 0);
			r->size = stats.st_size;
			r->data = xmalloc(r->size);
			log_assert(read(fd, r->data, r->size) == r->size);
			r->backing = r->size;
			close(fd);
			return r;

		} else {

			log_f(CRD_LOG_VERBOSE, "Reading resource %s from pack %d at offset %d", path, (int) f->pack_part.id, (int) f->pack_part.hdr);
			int fd = pack_fds[f->pack_part.id];
			log_assert(lseek(fd, f->pack_part.hdr, SEEK_SET) != -1);
			struct res_pack_file_base hdr;
			xread(fd, &hdr, sizeof(struct res_pack_file_base));
			r->size = hdr.size;
			r->backing = 0;
			r->data = xmalloc(r->size);
			uint32_t chunk_ids[hdr.chunk_count];
			xread(fd, chunk_ids, sizeof(uint32_t) * hdr.chunk_count);
			char *data = r->data;
			for (uint32_t i = 0; i < hdr.chunk_count; i++) {
				log_assert(lseek(fd, pack_chunks[f->pack_part.id][chunk_ids[i]], SEEK_SET) != -1);
//				log_f(CRD_LOG_ALWAYS, "Chunk %d at %d", (int) chunk_ids[i], (int) lseek(fd, 0, SEEK_CUR));
				uint32_t size;
				log_assert(read(fd, &size, sizeof(uint32_t)) == sizeof(uint32_t));
				log_assert(read(fd, data, size) == size);
				r->backing += size;
				data += size;
			}
//			log_f(CRD_LOG_ALWAYS, "backing %d == size %d", (int) r->backing, (int) r->size);
			log_assert(r->backing == r->size);
			return r;

		}
	}

	size_t fn_back = strlen(path) + 128 - strlen(path) % 64;
	char *filename = alloca(fn_back);
	for (size_t i = 0; i < cfg_res_path_len; i++) {
		size_t dir_len = strlen(cfg_res_path[i]);
		if (dir_len + strlen(path) + 2 > fn_back) {
			fn_back += dir_len;
			alloca(fn_back);
		}
		strcpy(filename, cfg_res_path[i]);
		filename[dir_len] = '/';
		strcpy(filename + dir_len + 1, path);
		int fd = open(filename, O_RDONLY);
		if (fd == -1) continue;
		log_f(CRD_LOG_VERBOSE, "Reading resource %s from path directory %s", path, cfg_res_path[i]);
		struct stat stats;
		log_assert(fstat(fd, &stats) == 0);
		r->size = stats.st_size;
		r->data = xmalloc(r->size);
		log_assert(read(fd, r->data, r->size) == r->size);
		r->backing = r->size;
		close(fd);
		return r;
	}

	mem_pool_free(promise_pool, r);
	log_f(CRD_LOG_ERROR, "Couldn't find resource %s", path);
	return NULL;
}

void res_close(struct res_promise *res)
{
	free(res->data);
	mem_pool_free(promise_pool, res);
}

// trivial with synchronous I/O
void res_await(UNUSED struct res_promise *res) {}
void res_back_to(UNUSED struct res_promise *res, UNUSED size_t backing) {}
