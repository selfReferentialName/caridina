#include <caridina/caridina.h>

#include "config/file.h"
#include "server/resource/api.h"
#include "server/gpu/api.h"
#include "glue/i.h"
#include "ecs/api.h"
#include "system/api.h"

struct mem_arena *glue_frame_arena;
pthread_mutex_t glue_frame_arena_lock = PTHREAD_MUTEX_INITIALIZER;

void crd_init(int argc, char **argv)
{
	config_init(argc, argv);
	res_init();
	gpu_init();
	ecs_init();
	sys_init();
	glue_frame_arena = mem_arena_new(0);
	glue_init_input();
}
