#include <caridina/caridina.h>

#include "glue/i.h"
#include "config/file.h"
// TODO: add stuff to configuration files
#include "util/log.h"
#include "attributes.h"
#include "server/gpu/api.h"

#include <GLFW/glfw3.h>
#include <stdlib.h>

struct action_callbacks {
	void (*fun)(void);
	struct action_callbacks *next;
};

static int *action_to_key; // indexed by one
static struct action_callbacks **action_callbacks; // also indexed by one
static crd_action_t key_to_action[GLFW_KEY_LAST + 1];
static crd_action_t action_count;

crd_action_t crd_register_action(const char *name, int deflt)
{
	crd_action_t r = action_count + 1;
	action_count++;
	action_to_key = realloc(action_to_key, (action_count+1) * sizeof(int)); // O(n) allocation because I don't care
	action_callbacks = realloc(action_callbacks, (action_count+1) * sizeof(struct action_callbacks *)); // O(n) allocation because I don't care
	action_to_key[r] = deflt;
	action_callbacks[r] = NULL;
	key_to_action[deflt] = r;
	log_f(CRD_LOG_DEFAULT, "Action %u has name %s and default key %d", (unsigned) r, name, deflt); // don't get log level set at this point
	return r;
}

void crd_on_action_released(crd_action_t action, void (*fun)(void))
{
	struct action_callbacks *r = malloc(sizeof(struct action_callbacks));
	r->fun = fun;
	r->next = action_callbacks[action];
	action_callbacks[action] = r;
}

HIDDEN
void glue_key_callback(GLFWwindow *window, int key, UNUSED int scancode, int action, UNUSED int mods)
{
//	log_f(CRD_LOG_ALWAYS, "Got key %d as %d action %u", key, action, (unsigned) key_to_action[key]);
	if ((action == GLFW_RELEASE || action == GLFW_REPEAT) && key_to_action[key]) {
		crd_action_t a = key_to_action[key];
		for (struct action_callbacks *cur = action_callbacks[a]; cur; cur = cur->next) {
			cur->fun();
		}
	}
}

void glue_init_input(void)
{
	glfwSetKeyCallback(gpu_window, &glue_key_callback);
}
