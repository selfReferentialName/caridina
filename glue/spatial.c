#include <caridina/caridina.h>

#include "system/api.h"
#include "ecs/api.h"
#include "server/gpu/api.h"

#include <cglm/cglm.h>
#include <math.h>
#include <GLFW/glfw3.h>

void crd_add_pos(crd_entity_t entity, vec3 pos)
{
	if (ecs_has_comp(entity, ct_xform)) {
		if (ecs_has_comp(entity, ct_pos)) { // TODO: nasty hack, both branches should be equivalent if this is true
			ecs_set_data(entity, ct_pos, pos);
		} else {
			ecs_add_comps(entity, 1, &ct_pos, (const void *const *) &pos);
		}
	} else {
		mat4 xform;
		glm_mat4_identity(xform);
		glm_vec3_copy(pos, (float *) xform[3]);
		const void *data[2] = { pos, xform };
		ecs_field_t comps[2] = { ct_pos, ct_xform };
		ecs_add_comps(entity, 2, comps, data);
	}
}

void crd_add_xform(crd_entity_t entity, mat3 lform)
{
	mat4 xform;
	glm_mat4_identity(xform);
	glm_mat4_ins3(lform, xform);
	if (ecs_has_comp(entity, ct_xform)) {
		mat4 xform0;
		ecs_get_data(entity, ct_xform, xform0);
		for (int i = 0; i < 3; i++) {
			xform[3][i] = xform0[3][i];
		}
		ecs_set_data(entity, ct_xform, xform);
	} else {
		const void *data[1] = { xform };
		ecs_field_t comps[1] = { ct_xform };
		ecs_add_comps(entity, 1, comps, data);
	}
}

void crd_add_frustum(crd_entity_t entity, double near, double far, double fov, double aspect)
{
	struct sys_frustum_info info = {
		.near = near,
		.far = far,
		.fov = fov,
		.aspect = aspect
	};
	ecs_field_t comps[2] = { ct_frustum, ct_frustum_info };
	const void *data[2] = { NULL, &info };
	ecs_add_comps(entity, 2, comps, data);
}

void crd_get_pos(crd_entity_t entity, vec3 pos)
{
	ecs_get_data(entity, ct_pos, pos);
}

void crd_get_xform(crd_entity_t entity, mat3 lform)
{
	mat4 xform;
	ecs_get_data(entity, ct_xform, xform);
	glm_mat4_pick3(xform, lform);
}
