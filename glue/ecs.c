#include <caridina/caridina.h>
#include "ecs/api.h"
#include "glue/i.h"

#include <string.h>

crd_entity_t crd_new_entity(const char *name)
{
	pthread_mutex_lock(&glue_frame_arena_lock);
	char *local_name = mem_arena_malloc(glue_frame_arena, strlen(name) + 1);
	pthread_mutex_unlock(&glue_frame_arena_lock);

	strcpy(local_name, name);
	return ecs_new_entity(local_name);
}
