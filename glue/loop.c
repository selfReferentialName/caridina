#include <caridina/caridina.h>

#include "glue/i.h"
#include "system/api.h"
#include "ecs/api.h"
#include "server/gpu/api.h"
#include "util/sync.h"

#include <GLFW/glfw3.h>

atomic_bool crd_exit;

int crd_game_loop(void)
{
	while (!crd_exit) {
		ecs_run_pipeline(sys_predraw);
		glue_update_camera();
		ecs_run_pipeline(sys_draw);
		gpu_finish();
		glfwPollEvents();
//		ecs_run_pipeline(sys_update);
		sync_frame_end();
	}
	return 0;
}
