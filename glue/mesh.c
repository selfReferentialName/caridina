#include <caridina/caridina.h>

#include "ecs/api.h"
#include "system/api.h"
#include "server/gpu/api.h"
#include "glue/i.h"
#include "util/mem/arena.h"

#include <string.h>
#include <pthread.h>

crd_mesh_t crd_mesh_resource(const char *path)
{
	pthread_mutex_lock(&glue_frame_arena_lock);
	char *name = mem_arena_malloc(glue_frame_arena, strlen(path) + 1);
	pthread_mutex_unlock(&glue_frame_arena_lock);

	strcpy(name, path);
	return gpu_upload_mesh_res(name);
}

void crd_add_mesh(crd_entity_t entity, crd_mesh_t mesh)
{
	gpu_mesh_t m = mesh;
	const void *mp = &m;
	ecs_add_comps(entity, 1, &ct_mesh, &mp);
}
