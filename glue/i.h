/** @file glue/i.h
 * @brief Declarations for common use in glue code.
 */
#ifndef GLUE_I_H
#define GLUE_I_H

#include "util/mem/arena.h"

#include <pthread.h>

extern struct mem_arena *glue_frame_arena; ///< An arena reset every frame for temporary API allocations.
extern pthread_mutex_t glue_frame_arena_lock; ///< Lock out glue_frame_arena.

void glue_update_camera(void); ///< Call to send the camera to the gpu server.

void glue_init_input(void); ///< Initialise the input stuff.

#endif
