#include <caridina/caridina.h>
#include "system/api.h"
#include "glue/i.h"
#include "server/gpu/api.h"
#include "util/log.h"

#include <cglm/cglm.h>

ecs_entity_t sys_camera;

void glue_update_camera(void)
{
	mat4 view, proj;
	ecs_get_data(sys_camera, ct_xform, view);
	glm_mat4_inv(view, view);
//	log_f(CRD_LOG_ALWAYS, "{{%f,%f,%f,%f},{%f,%f,%f,%f},{%f,%f,%f,%f},{%f,%f,%f,%f}}",
//			view[0][0], view[0][1], view[0][2], view[0][3],
//			view[1][0], view[1][1], view[1][2], view[1][3],
//			view[2][0], view[2][1], view[2][2], view[2][3],
//			view[3][0], view[3][1], view[3][2], view[3][3]);
	ecs_get_data(sys_camera, ct_frustum, proj);
	gpu_set_camera(view, proj);
}

void crd_set_camera(crd_entity_t entity)
{
	sys_camera = entity;
}
